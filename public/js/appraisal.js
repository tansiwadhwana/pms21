$( document ).ready(function() {
    $("#add").click(function(){
        $('.error').hide();
        var lastname_id = $('.user input[type=text]:nth-child(1)').last().attr('name');
        var split_id = lastname_id.split('_');
        var index = Number(split_id[1]) + 1;
        var newel = $('.user:last').clone(true);
        $(newel).find('select[custom=form]:nth-child(2)').first().attr("name","name_"+index);
        $(newel).find('select[custom=form]:nth-child(2)').last().attr("name","designation_"+index);
        $(newel).find('input[type=text]:nth-child(1)').first().attr("name","amount_"+index);
        $(newel).find('input[type=text]:nth-child(1)').last().attr("name","newCtc_"+index);
        $(newel).find('label[custom=label-newCtc]:nth-child(3)').last().attr("data-val-newCtc","newCtc_"+index);
        $(newel).find('label[custom=label-amount]:nth-child(3)').last().attr("data-val-amount","amount_"+index);
        $(newel).find('label[custom=label-designation]:nth-child(3)').last().attr("data-val-designation","designation_"+index);
        $(newel).find('input[type=text]:nth-child(1)').val(0);
        $(newel).find('input[type=text]:nth-child(1)').val(0);
        $(newel).insertAfter(".user:last");
    });
});
manageForm();

function manageForm(){
    $("#appraisal_form").validate({
    
    submitHandler: function (form) {
        var isError = 0;
        // var to_date   = $('#to_date').val();
        // var from_date= $('#from_date').val();
        $('.name').each(function() {
            var nameField = $(this).attr('name');
            var emp_name = $('[name='+nameField+']').val();
            if(emp_name != '' && emp_name != 0)
            {
                var index = nameField.split("_")[1];
                var newCtcNameField = "newCtc_"+index;
                var newAmtNameField = "amount_"+index;
                var designationNameField = "designation_"+index;
                var newCtc = $('[name='+newCtcNameField+']').val();
                var amount = $('[name='+newAmtNameField+']').val();
                var designation = $('[name='+designationNameField+']').val();
                if(newCtc == '' || newCtc <= 0)
                {
                    isError++;
                    $('[data-val-newCtc='+newCtcNameField+']').css({'display':'block'});
                    $('[data-val-newCtc='+newCtcNameField+']').text('Please enter new ctc amount');
                }
                if(amount == '' || amount <= 0)
                {
                    isError++;
                    $('[data-val-amount='+newAmtNameField+']').css({'display':'block'});
                    $('[data-val-amount='+newAmtNameField+']').text('Please enter appraisal amount');
                }
                if(designation == '' || designation <= 0)
                {
                    isError++;
                    $('[data-val-designation='+designationNameField+']').css({'display':'block'});
                    $('[data-val-designation='+designationNameField+']').text('Please select designation');
                }
            }
        });     
        // if(to_date == ''){
        //     isError++;
        //     $('.to_date-error').css({'display':'block'});
        //     $('.to_date-error').text('Please select date');
        // }
        // if(from_date == ''){
        //     isError++;
        //     $('.from_date-error').css({'display':'block'});
        //     $('.from_date-error').text('Please select date');
        // }
        if(isError == 0)
        {
            form.submit();
        }
    }
});
}

$('#from_date').datepicker({
    todayBtn: 1,
    autoclose: true,
    format: 'dd-mm-yyyy',
}).on('changeDate', function (selected) {
    var endDate = new Date(((selected.date).getFullYear()), ((selected.date).getMonth()+1), 0);
    var minDate = new Date(selected.date.valueOf());
    $('#to_date').datepicker('setStartDate', minDate);
});
$('#to_date').datepicker({
    autoclose: true,
    todayBtn: 1,
    format: 'dd-mm-yyyy',
}).on('changeDate', function (selected) {
    var maxDate = new Date(selected.date.valueOf());
    $('#from_date').datepicker('setEndDate', maxDate);
});