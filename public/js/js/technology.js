///////// Add Technology
$( document ).ready(function() {
    $('[data-toggle="tooltip"]').popover({html:true});
    $('.select2').select2();
addTechForm();
editTechForm();
$(document).on("click", ".techModal", function(){
    $('#name').val('');
    $('.name_error').text('');
    $('#addTechModal').modal('show');
});

$(document).on("click", ".editTechModal", function(){
    var id = $(this).attr('data-user-id');
    if(id != ''){
        jQuery.ajax({
            url  : getsiteurl() + '/technology/edit/'+ id,
            type : "get",
            success: function(data) {
                $('#edit_name').val(data.name);
                $('#id').val(data.id);
                $('.edit_name-error').text('');
                $('#editTechModal').modal('show');
            }
        });
    }else{
        swal(response.message,"", "success");
    }
});

$('.deleteTech').on('click',function () {
    var techId = $(this).attr('data-user-id');

    if(techId != ''){
        swal({
            title: "Are you sure you want to delete technology?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                deleteTech(techId);
            }
        });
    }
});

function deleteTech(techId){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    jQuery.ajax({
    url  : getsiteurl() + '/technology/delete',
    type : "DELETE",
    data: {_token: CSRF_TOKEN,techId:techId},
    success: function(response) {
            if(response.status == 'success'){
                swal(response.message,"", "success");
                location.reload();
            }else if(response.status == 'error'){
                swal(response.message,"", "error");
            }
        }
    });
}

$('#addTech').submit(function(evt) {
    evt.preventDefault();
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    if( $('#addTech').valid() ) {
        var name = $('#name').val();
        jQuery.ajax({
            url  : getsiteurl() + '/technology/add',
            type : "POST",
            data: {_token: CSRF_TOKEN,name:name},
            success: function(response) {
                if(response.status == "success"){
                    swal(response.message,"", "success");
                    $('#addTechModal').modal('toggle');
                    getTechnology();
                }
                else{
                    $('.name_error').html(response.message).show();
                }
            }
        });
    }
});

$('#editTech').submit(function(evt) {
    evt.preventDefault();
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    if( $('#editTech').valid() ) {
        var edit_name = $('#edit_name').val();
        var id = $('#id').val();
        jQuery.ajax({
            url  : getsiteurl() + '/technology/update',
            type : "POST",
            data: {_token: CSRF_TOKEN,id:id,edit_name:edit_name},
            success: function(response) {
                if(response.status == "success"){
                    swal(response.message,"", "success");
                    $('#updateTechModal').modal('toggle');
                    getTechnology();
                }
                else{
                    $('.edit_name-error').html(response.message).show();
                }
            }
        });
    }
});

function addTechForm() {
    $("#addTech").validate({
        rules: {
            name: "required",
        },
        messages: { 
            name: "The name field is required",
        },
    });
}

function editTechForm() {
    $("#editTech").validate({
        rules: {
            edit_name: "required",
        },
        messages: {
            edit_name: "The edit name field is required",
        },
    });
}

function getTechnology(){
    jQuery.ajax({
        type: "GET",
        url: getsiteurl() + '/getTechnology',
        success: function (data) {
            location.reload();
        }
    });
}
$('#page_rang_dropdown').change(function(){
    var limit = $(this).val();
    var link  = window.location.pathname;
    var query_string = window.location.search;

    var search_str = (query_string.indexOf('?') == 0 ? query_string.substring(1) : query_string);
    if(limit != '' && query_string.indexOf('search_submit') != -1){
        if(query_string.indexOf('page_range')==1){
            var page_range = query_string.slice(0,query_string.indexOf('='));
            var srch_str = query_string.slice(query_string.indexOf('&'));
            var redirectLink = link +page_range+'='+limit+srch_str;
        }else{
            var redirectLink = link +'?page_range='+limit + '&'+search_str;
        }
    }else{
        var redirectLink = link +'?page_range='+limit;
    }
   window.location = redirectLink;
    
});	
});