 $(document).ready(function () {

 $.validator.addMethod('file', function (value, elem) {
  var hiddenid=$("#imgcount").val();

   var file=$("#file").val();
   if(hiddenid == 0){
   
  return false;
 
 }else{
  
  return true;
 }
}, 'This product images is required');
 $('input[type="file"]').change(function() {
                $("#imgcount").val(1);
            });
 const titleValidators = {
        validators: {
            notEmpty: {
                message: 'The task is required'
            }
        }
    };





 $('#productform').validate({

                rules: {
                   
                   category : {
                      required: true,
                        
                    },
                   

                    p_name: {
                     required: true,
                                        
                   },
                  description:{
                       required: true,
                  },
                   p_quantity:{
                       required: true,
                  },
                   p_price:{
                      required: true,
                  },
                    pv_price_0:{
                      required: true,
                  },
                   pv_quantity_0:{
                      required: true,
                  },
                   "file[]":{
                      file: true,
                  }
          },
          messages:{
                   
                category: {
                  required: " Category is required",
                   


                    },
                     p_name: {
                 
              required: " Product name is required",

                  },
                   description: {
                 
                   required: " Description is required",

                  },
                   p_quantity: {
                 
                  required: " Product quantity is required",

                  },
                  p_price: {
                 
               required: " Product price is required",

                  },
                   pv_price_0: {
                 
               required: " Product price  is required",

                  },
                    pv_quantity_0: {
                 
               required: " Product price 2 is required",

                  },
               //      "file[]": {
                 
               // required: " product images is required",

               //    }
              }
                       
            });

  });

 $(document).ready(function(){
    $('.customSwitchesproduct').change(function () {
        var status = $(this).prop('checked') === true ? 1 : 0;


        var productId = $(this).attr('data-id');
          // alert(productId);
       
        $.ajax({
            type: "GET",
            dataType: "json",
            url: site_url+'/productstatus' ,
            data: {'status': status, 'product_id':productId },
            success: function (data) {
             if (data.success === true) {
                            swal(data.message);
                        } 
              
               
            }
        });
    });

    $('.delete-product').on('click', function (event) {
    event.preventDefault();
    const url = $(this).attr('href');
   

    swal({
        title: 'Are you sure?',
        text: 'This record and it`s details will be permanantly deleted!',
        icon: 'warning',
        buttons: ["No", "Yes!"],
    }).then(function(value) {
        if (value) {
           
            window.location.href = url;
        }
    });
});

     $('.deleteimage').on('click', function (event) {
    event.preventDefault();
    var id = $(this).attr('data-attr');
   
   

    $.ajax({
            type: "GET",
            dataType: "json",
            data: { 'image_id':id },
            success: function (data) {
              // alert(data);
             if (data.success === true) {
                    swal(data.message);
                 } 
                 $('.hide_'+id).html('');
                 var count = $('#imgcount').val();
                 var finalcount = count-1;
                 $('#imgcount').val(finalcount);
                
                 
              
               
            }
        });
}); 
           $(".add_propertyvalues").hide();
           var j =  $('#property_count').val();


               /* if(j<1){*/
  var j =  $('#property_count').val();  
  var e =  $('#editproperty_count').val();  

                if(j==0){

                   $(".add_propertyvalues").hide();
                }  
                else{
                   $(".add_propertyvalues").show();
                }
                
          $('.add_property').on('click', function (event) {
               event.preventDefault();
               alert(j);
            
              var j =  $('#property_count').val();  
              j++;
              $('#property_count').val(j);
                // alert(j);
              $(".add_propertyvalues").toggle();
    
});

   $('#add').click(function(){ 
   var j =  $('#property_count').val();  
  
    var html = '';
        html += '<div class="row dynamic_field_'+j+' dynamic_field " style="margin-right:20px;">';
         html += '<input type="hidden"  value="" name="pv_id_'+j+'" id="">';
        html += '<div class="col-3 dynamic_row"><div class="form-group "> <label for="exampleInputEmail3" class="col-md-4 col-form-label text-md-left"></label><div class="col-md-12"><select name="color_'+j+'" class="form-control" required><option value="">select color</option> <option value="white">white</option><option value="black">Black</option> <option value="gray">Gray</option></select><span class="invalid-feedback" role="alert"><strong></strong></span></div></div></div>';
        html += '<div class="col-3 dynamic_row"><div class="form-group "><label for="exampleInputEmail3" class="col-md-4 col-form-label text-md-left"></label><div class="col-md-12"><input type="number" class="form-control" id="" placeholder="product price" min="0" oninput="validity.valid||(value="");" step=".01" name="pv_price_'+j+'" value="{{}}" required> <span class="text-danger">"{{ $errors->first("pv_price_'+j+'") }}"</span></div> </div> </div>';
        html += '<div class="col-3 dynamic_row"> <div class="form-group    ">  <label for="exampleInputEmail3" class="col-md-4 col-form-label text-md-left"></label> <div class="col-md-12">   <input type="number" class="form-control" id="" placeholder="product quantity" min="1" oninput="validity.valid||(value="");" name="pv_quantity_'+j+'"  value="{{}}" required>   <span class="text-danger"> ';
        html +=  {{$errors->first("pv_quantity_'+j+'")}} ;
        html +=  '</span></div></div></div>';
        html += '<div class="col-3 dynamic_row"><div class="form-group " ><label for="password" class="col-md-5 col-form-label text-md-left"></label><div class="row"> <div class="col-md-10 " ><input type="file"  name="pv_file_'+j+'[]" class="form-control" multiple/></div><div class="col-md-2" ><button type="button" name="remove"  id="'+j+'" class="btn btn-danger btn_remove removerow">X</button></div></div></div></div>';
    $('#newRow').append(html);
    j++;
      $('#property_count').val(j);
   
});  

   

   
 $(document).on('click', '.removerow', function () {
   var j =  $('#property_count').val(); 
    var button_id = $(this).attr("id");   
        $(this).closest('.dynamic_field_'+button_id).remove();
        j--;
         $('#property_count').val(j); 
    });

});
 