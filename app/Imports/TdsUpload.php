<?php

namespace App\Imports;
use App\Models\User;
use App\Models\Salary;
use App\Models\Salarysheet;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Illuminate\Support\Facades\Session;

class TdsUpload implements ToCollection, WithStartRow
{
    private $request = "";

    public function  __construct($request)
    {
        $this->request = $request;
    }
    public function startRow(): int {return 1; }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function collection(Collection $rows)
    {
        $success = $inValidRecord = 0;
        $data = array();
        
        foreach ($rows  as $key => $row) {
            if($key > 0 && $row[0] != ''){
                
                $userDetails = User::where('code',$row[0])->first();
                if(!empty($userDetails)){
                    $salarysheet = Salarysheet::where('employee_id',$userDetails->employee_id)->where('month',$this->request->ReportMonth)->where('year',$this->request->ReportYear)->first();
                    if(!empty($salarysheet)){
                        $salarysheet->tds = $row[2];
                        $salarysheet->save();
                        $success++;
                    }else{
                        $inValidRecord++;
                    }
                }else{
                    $inValidRecord++;
                }
                
            }
        }
        $data['success']=$success;
        $data['skipped']= $inValidRecord;
        Session::push('UserSalaryImportError',$data);
    }
}
