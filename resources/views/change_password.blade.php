@extends('layouts.master')
@section('content')
@section('moduleName')
    Change Password
@endsection
@include('sweet::alert')
<section class="content-header">
    <div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
        <div class="page-breadcrumb" >
            <div class="row">
                <div class="col-12 no-block align-items-center">
                    <div class="text-left">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb float-right">
                                <li class="breadcrumb-item"><a href="{{url('home')}}">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Change Password</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>    
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
<section class="content">    
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row change_pass">
            <div class="col-md-12">
                @if (Session::has('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                    <strong>{!!session('success')!!}</strong>
                </div>
                @endif    
                @if (Session::has('error'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                    <strong>{!!session('error')!!}</strong>
                </div>
                @endif 
                <div class="card card-primary">
                    <div class="card-header bg-dark">
                        <h3 class="card-title">Change Password</h3>
                    </div>
                    <form role="form" method="post" action="{{url('/changepassword')}}" name="user_create" id="user_create">
                    @csrf
                        <div class="card-body">
                            <div class="form-group">
                                <label for="password">
                                    Current Password
                                    <span class="error">*</span>
                                </label>
                                <input type="password" name="password" id="password" value="{{isset($request->password)?$request->password:old('password')}}" autocomplete="new-password" class="form-control" />
                                @if ($errors->has('password'))
                                    <div class="error">{{ $errors->first('password') }}</div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="new_password" class="">
                                    New Password
                                    <span class="error">*</span>
                                </label>
                                <input type="password" name="new_password" id="new_password" class="form-control" value="{{isset($request->new_password)?$request->new_password:old('new_password')}}" />
                                @if ($errors->has('new_password'))
                                    <div class="error">{{ $errors->first('new_password') }}</div>
                                @endif
                            </div>  
                            <div class="form-group">
                                <label for="confirm_password" class="">
                                    Confirm password
                                    <span class="error">*</span>
                                </label>
                                <input type="password" name="confirm_password" id="confirm_password" class="form-control" value="{{isset($request->confirm_password)?$request->confirm_password:old('confirm_password')}}" required />
                                @if ($errors->has('confirm_password'))
                                    <div class="error">{{ $errors->first('confirm_password') }}</div>
                                @endif
                            </div>
                            <div class="form-group">
                                <input id="generatePswdButton" type="button" value="Generate Password" class="btn btn-primary btn-dark">
                                <a id="showPswd" href="JavaScript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Show/Hide Password"/>
                                    <i class="fa fa-eye" style ="color:#343a40"></i>
                                </a>
                            </div>                    
                        </div>
                        <div class="card-footer">
                            <input id="submitBtn" type="submit" value="Change" class="btn btn-primary btn-dark">
                            <a href="{{url('home')}}"><button type="button" value="Cancel" class="btn btn-secondary btn-dark">Cancel</button></a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
</section>
@endsection

    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
@section('javascript')
  <script>
   $( document ).ready(function() {
        $('#showPswd').click(function(){
            var x = document.getElementById("new_password");
            var y = document.getElementById("confirm_password");
            if (x.type === "password") {
              x.type = "text";
              y.type = "text";
            } else {
              x.type = "password";
              y.type = "password";
            } 
        });
        $('#generatePswdButton').click(function(){
              var hash = randomPassword(8);
            $('#new_password').val(hash);
            $('#confirm_password').val(hash);
        });

    });
        $("#user_create").validate({
        rules: {
                password: "required",
                new_password: "required",
                confirm_password: "required"
                
        },
        messages:{
                password: "Please enter password",
                confirm_password: "Please enter new password.",
                confirm_password: "Please enter confirm password."
        },
        submitHandler: function (form) {
            form.submit();
        }


          });

    
    function randomPassword(length) {
        var chars = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890";
        var pass = "";
        for (var x = 0; x < length; x++) {
            var i = Math.floor(Math.random() * chars.length);
            pass += chars.charAt(i);
        }
        return pass;
    }

  </script>
@endsection
 


