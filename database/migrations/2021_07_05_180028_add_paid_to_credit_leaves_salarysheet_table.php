<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPaidToCreditLeavesSalarysheetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('salarysheet', function (Blueprint $table) {
            $table->text('credit_leave_cf')->nullable();
            $table->text('leave_of_month')->nullable();
            $table->text('balance_leave')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('salarysheet', function (Blueprint $table) {
            //
        });
    }
}
