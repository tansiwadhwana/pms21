@extends('layouts.master')
@section('content')
@php
    $pageRangArray = config('constant.page_range');
@endphp
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/salarysheet')}}">Salary Sheet</a></li>
                </ol>
            </div>
            <div>
               <div class="card-body text-left pt-2 pb-2">
                          
                     <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Task Entry" 
                                                       class="btn btn-dark timeEntry" data-task-id="" data-id=''>
                            Generate Salary
                          </a>
                </div>
            </div>
        </div>
    </div>
</section>
<style>
  .salary_sheet_popup_modal .modal-body {
       max-height: calc(100vh - 210px);
       overflow-y: auto;
   }
   .salary_sheet_popup_modal th{
     white-space: nowrap;
   }
</style>
@include('sweet::alert') 
    <section class="content">
         <div class="container-fluid">
            @if(!empty($salraysheetdata))
     
             <div id="accordion">
               <div class="card">
                 <div class="card-header bg-dark {{(!$request->has('search_submit'))?'collapsed':''}}" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false">
                    <h3 class="card-title">Filter Salary Sheet</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-chevron-down"></i></button>
                    </div>
                 </div>
                 <div id="collapseOne" class="panel-collapse in {{(!$request->has('search_submit'))?'collapse':'collapse show'}}">
                    <form class="form-horizontal" method="get" action="{{url('/salarysheet/')}}" name="search_filter" id="search_filter">
                       <input type="hidden" name="ReportMonth" id="ReportMonth" value="{{$month}}">
                        <input type="hidden" name="ReportYear" id="ReportYear" value="{{$year}}">
                        <div class="card-body">
                            <div class="row">
                                <div class="form-group col-lg col-md-6 col-sm-6">
                                    <label class="">User Name</label>
                                    <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="search_by_user_name" id="search_by_user_name">
                                        <option value="">Select</option>
                                        @foreach($empName as $ename)
                                        <option value="{{$ename}}" {{(isset($request->search_by_user_name) && ($request->search_by_user_name == $ename))?'selected':''}}>{{$ename}}</option>
                                        @endforeach
                                    </select>  
                                </div>
                                <div class="form-group col-lg col-md-6 col-sm-6">
                                    <label class="">Employee Id</label>
                                    <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="search_by_employee_id" id="search_by_employee_id">
                                        <option value="">Select</option>
                                         @foreach($empId as $eid)
                                          <option value="{{$eid}}" {{(isset($request->search_by_employee_id) && ($request->search_by_employee_id == $eid))?'selected':''}}>{{'TRD'.$eid}}</option>
                                          @endforeach	
                                    </select>  
                                </div>
                               
                                
                            </div>

                            
                            
                        </div>
                        {{-- <input type="hidden" value="{{(isset($request->page_range))?$request->page_range:''}}" name="page_range" id="page_range"> --}}
                        <div class="card-footer">
                            <button name="search_submit" value="1" type="submit" class="btn btn-primary btn-dark">
                                Search
                            </button>
                            <button name="search_reset" type="reset" class="btn btn-info btn-secondary" id="form_reset">
                                Reset
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
      <div id="right-sidebar" class="settings-panel">
        <i class="settings-close ti-close"></i>
     
      </div>
      @endif
      <!-- partial -->
      <!-- partial:../../partials/_sidebar.html -->
      <!-- partial -->
      <?php if(!empty($month) && !empty($year)){?>
      <button class="btn btn-dark edit_mutilple_data">Edit Multiple Data</button>
      <?php }?>
          <div class="row">
            <section class="content-header">
                  <div>
                      <div class="card-body text-left pt-2 pb-2">
                          
                         
                      </div>
                  </div>
              </section>
            <div class="col-md-12">
  @if(!empty($salraysheetdata))
            <div class="mb-2" style="margin-left:1120px;">
              <form action="{{url('/salarysheet')}}" method ="post">
                    @csrf
                    <input type="hidden" name="ReportMonth" value="{{$month}}">
                    <input type="hidden" name="ReportYear" value="{{$year}}">
                      <input type="submit"  title="Back"  name="export_to_excel" value="Export Excle" class="btn btn-dark">
                                
                         
            </form>
        </div>
            @endif
            @if(!empty($salraysheetdata))
             <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Import TDS" 
                                                       class="btn btn-dark importtds" data-task-id="" data-id=''>
                            Import TDS
                          </a>
            @endif
              <div class="card">
                    <div class="row update_checked_data_row">
                      <div class="col-md-1 "> 
                          <div class="form-group col">
                            <form action="{{url('/salarysheet/'.$month.'/'.$year)}}" method ="post" name="getpage" id="getpage">
                              @csrf
                                <?php if(isset($salraysheetdata) || isset($salraysheetattechmentdata)){?>
                              <input type="hidden" name="month" value="{{isset($month)?$month:''}}">
                                 <input type="hidden" name="year" value="{{isset($year)?$year:''}}">
                               
                              <select class="select2 form-control custom-select page_range" name="page_range" id="page_range">
                                  @if(is_array($pageRangArray))
                                      @foreach ($pageRangArray as $key => $node)
                                          <option value="{{$key}}" {{(isset($request->page_range) && ($request->page_range == $node))?'selected':''}}> {{ $node }}</option>
                                      @endforeach 
                                  @endif
                              </select>
                            <?php } ?>
                             </form>
                          </div>
                      </div>
                      <div class="col-md-7 float-right"> </div>
                      <div class="col-md-4">

                          <div class="card-body show_total_no">
                            <?php if(isset($salraysheetdata)){?>
                             Showing {{ $salraysheetdata->firstItem() }} to {{ $salraysheetdata->lastItem() }} of total {{$salraysheetdata->total()}} entries
                           <?php }?>
                           <?php if(isset($salraysheetattechmentdata)){?>
                             Showing {{ $salraysheetattechmentdata->firstItem() }} to {{ $salraysheetattechmentdata->lastItem() }} of total {{$salraysheetattechmentdata->total()}} entries
                           <?php }?>

                          </div>  
                      </div>

                     
                  </div>

                  <div class="table-responsive">
                
                 @if(!empty($salraysheetdata))
                  @php  
                     $getexistingsalarysheet = '';

                     @endphp
                     
                    <table class="table table-striped table-bordered">
                      <thead>
                          <h4 class="card-title">SalarySheet</h4>
                          <tr>
                            <th><input type="checkbox" name="locationthemesCheckAll" id="locationthemesCheckAll"></th>
                            <th>Id</th>
                          <th>Emp id</th>
                          <th>Emp name</th>
                          <th>Designation </th>
                          <th>Department</th>
                           <th>joining Date</th>
                            <th>pan Number</th> 
                            <th>Bank name</th>
                            <th>Account number</th>
                            <th>IFSC code</th>
                            <th>UAN Number</th>
                            <th>Leave Balance</th>
                            <th>Leave of The Month</th>
                            <th>Month Days</th>
                            <th>Working Days</th>
                            <th>Extra Leaves / Comp-off</th>
                            <th>Present Days</th>
                            <th>Total Days</th>
                            <th>Salary</th>
                            <th>Additional</th>
                            <th>Add</th>
                            <th>Salary Payable</th>
                            <th>Permenant_deduction</th>
                            <th>Refund</th>
                            <th>p.Tax</th>

                            <th>TDS</th>
                            <th>Gratuity</th>
                            <th>Provi.Fund @ 18%</th>
                            <th>Other Deduction</th>
                          
                            <th>Net Payable</th>
                            <th>Mode</th>
                            <th>Note</th>
                            {{-- <th>Mode</th>
                            <th>Month</th>
                            <th>Year</th> --}}
                            <th>Action</th>








                          </tr>
                          
                        </thead> 
                         <tbody>
                    
                           @if($salraysheetdata->count()>0)
                          <?php $i=$salraysheetdata->firstItem();?>
                              @foreach($salraysheetdata as $s)
                              <?php $i++;?>
                              <tr>
                                <td><input type="checkbox" name="locationthemes" class="locationthemes" value="{{$s->id}}"></td>
                                
                             <td>{{$i}}</td>
                               <td>{{'TRD'.$s->employee_id }}</td>
                               <td>{{$s->name }}</td>
                               
                               <td>{{ $s->designation  }}</td>
                               
                              <td>{{isset($s->getuser->department)?$s->getuser->department:''}}</td>
                               <td>{{ $s->joining_date }}</td>
                              <td>{{ $s->pan_number }}</td>
                              <td>{{ $s->bank_name }}</td>
                              <td>{{ $s->account_number }}</td>
                              <td>{{ $s->ifsc_code }}</td>
                               <td>{{ $s->UAN_number }}</td>
                              <td>{{ $s->credit_leave_cf }}</td>
                               <td>{{ $s->leave_of_month }}</td>
                               <td>{{ $s->month_days }}</td>
                               <td>{{ $s->month_working_days }}</td>
                               <td>{{ $s->salaryCalculation['incase_comp_off'] }}</td>
                               <td>{{ $s->salaryCalculation['present_days']}}</td>
                               <td>{{ $s->salaryCalculation['total_days'] }}</td>
                               <td>{{round($s->salaryCalculation['salary'])}}</td>
                               <td>{{ round($s->salaryCalculation['additional']) }}</td>
                               <td>{{ $s->salaryCalculation['add'] }}</td>


                               <td>{{ round($s->salaryCalculation['salary_payable']) }}</td>
                               <td>{{ $s->salaryCalculation['permenant_deduction'] }}</td>
                               <td>{{ $s->salaryCalculation['refund'] }}</td>
                               <td>{{$s->salaryCalculation['p_tax']}}</td>
                               <td>{{ $s->salaryCalculation['tds'] }}</td>
                                <td>{{ round($s->salaryCalculation['gratuity'])}}</td>
                                <td>{{round($s->salaryCalculation['provi_fund'])}}</td>

                               <td>{{ $s->salaryCalculation['other_deduction'] }}</td>
                               <td>{{ round($s->salaryCalculation['net_payable']) }}</td>
                               <td>HDFC</td>
                               <td>{{ $s->notes }}</td>
                               {{-- <td>{{ $s->mode }}</td>
                               <td>{{ $s->month }}</td>
                               <td>{{ $s->year }}</td> --}}
                               <td> <a href="javascript:void(0);" data-toggle="modal" data-placement="top" title="Edit"
                                                       class="btn editentry"  data-id='{{ $s->id }}'>
                         <i class="mdi mdi-pencil"></i>
                          </a></td>
 



                               
                          </tr>
                           @endforeach
                          @else
                              <tr>
                              <td colspan="15">
                              <center><h4 class="card-title">No User Found</h4></center>
                              </td>
                            </tr>
                          @endif
                       
                      </tbody> 
                    </table>
                     <div class="d-flex justify-content-center">
                    {!! $salraysheetdata->appends(request()->input())->links() !!}
                    </div>
<!-- <<<<<<< HEAD
                  
=======
                   <form action="{{url('/salarysheet')}}" method ="post" >
                    @csrf
                    <input type="hidden" name="ReportMonth" id="ReportMonth" value="{{$month}}">
                    <input type="hidden" name="ReportYear" id="ReportYear" value="{{$year}}">
                      <input type="submit"  title="Back"  name="export_to_excel" value="Export Excle" class="btn btn-dark">
                                
                         
                          </form>
>>>>>>> 3028a9168782b4477b92b22ca41d1725aca9b95c -->
                     @else
                     
                    <table class="table table-striped table-bordered">
                      <thead>
                        <tr>
                       
                          <th>Month</th>
                          <th>Year</th>
                          <th>Action</th>

                        </tr>

                      </thead>
                      <tbody>
                       @if(!empty($getexistingsalarysheet))
                         @php $i = 1; @endphp
                      @php $salraysheetdata = ''; $genralsettingdata = '';
                    @endphp
                      <tr>
                         
                          <td>{{$getexistingsalarysheet->month}}</td>
                          <td>{{$getexistingsalarysheet->year}}</td>
                    
                            <td>
                             <a href="{{url('/salarysheet/'.$getexistingsalarysheet['month'].'/'.$getexistingsalarysheet['year'])}}" data-toggle="tooltip" data-placement="top" title="" data-original-title="View"><i  style="margin-right:30px;" class="mdi mdi-pencil"></i>
                            
                          </a> 
                        </td>
                      </tr>
                      <!--   @php  $i++;@endphp -->
                      @else
                      @if(!empty($salraysheetattechmentdata))
                    @foreach($salraysheetattechmentdata as $sdata)
                       <tr>
                          
                          <td>{{$sdata->month}}</td>
                          <td>{{$sdata->year}}</td>
                   
                            <td>
                             <a href="{{url('/salarysheet/'.$sdata['month'].'/'.$sdata['year'])}}" data-toggle="tooltip" data-placement="top" title="" data-original-title="View"><i  style="margin-right:30px;" class="mdi mdi-pencil"></i>
                              
                          </a> 
                          <a href="javascript:void(0)" class="sheetExitingDelete" data-id="{{ $sdata->id }}"><i  style="margin-right:30px;" class="mdi mdi-delete"></i></a>
                        </td>
                      </tr>
                      @endforeach
                        
                      @endif
                      @endif
                      </tbody>
                        
                    </table>
                        <div class="d-flex justify-content-center">
                        
                  <?php 
                     if(isset($salraysheetattechmentdata)){
                      ?>
                        {!!$salraysheetattechmentdata->appends(request()->input())->links() !!}
                      <?php }?>
                    </div>
                 
                    
                        

                     @endif  
                  </div>
               </div> 

                </div>

              </div>

             
            </div>
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        
        <!-- partial -->
     
  </div>
</section>
@include('modal.Salary_Filter_modal')
@include('modal.import_tds_modal')

@include('modal.edit_salary_modal')
<div id="salary_sheet_popup_modal" class="modal fade salary_sheet_popup_modal" role="dialog">
  <div class="modal-dialog modal-lg" style="max-width:1500px !important;">
      <div class="modal-content">
          <div class="modal-header card-header bg-dark">
              <h4 class="modal-title">User salary data</h4>
              <button type="button" class="close" data-dismiss="modal" style="color: #fff; opacity: 1;">&times;</button>
          </div>
           <form class="forms-sample" enctype="form-data/multipart" method="post" action="{{url('salarysheetpopup/update/')}}" autocomplete="off" id="editform">
                 @csrf
                 <input type="hidden" name="ReportMonthPopup" id="ReportMonthPopup" value="">
                  <input type="hidden" name="ReportYearPopup" id="ReportYearPopup" value="">
                  <input type="hidden" name="type" id="type" value="add">
         <!--  <form method="post" action="{{url('save/project/entry')}}" id="saveTaskEntry">
              @csrf
              <input type="hidden" name="projectId" id="projectId" value="">

            
                  <input type="hidden" name="redirectSameScreen" id="redirectSameScreen" value="true" />
             
                  <input type="hidden" name="redirectSameScreen" id="redirectSameScreen" value="false" />
            
              <input type="hidden" name="projectTaskId" id="projectTaskId" value="" /> -->

              <div class="modal-body modal_body_salary_sheet_popup" id="modal-body">
             
                  

                 
              </div>
              <div class="modal-footer">
                  <input type="submit" class="btn btn-primary btn-dark" value="Save"/>
                  <button type="button" class="btn btn-info btn-secondary" data-dismiss="modal">Close</button>
              </div>
          </form>
      </div>
  </div>
</div>
@endsection
@section('javascript')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
   $(document).on("click", ".timeEntry", function () {
     $('#taskTimeEntryModal').modal();
     $("#StartDate").datepicker({
                            todayBtn: 1,
                            autoclose: true,
                            format: 'dd-mm-yyyy',             
          });
     $("#EndDate").datepicker({
                            todayBtn: 1,
                            autoclose: true,
                            format: 'dd-mm-yyyy',                
          });
   });

$(document).on("click", ".importtds", function () {
  //alert("click");
     $('#importtdsModal').modal();
   });
  
  $(document).ready(function(){
    
    // $('#locationthemesCheckAll').change(function() {
    //     if(this.checked) {
    //       $("input[type=checkbox]").prop('checked', $(this).prop('checked'));
    //     }else{
    //       $("input[type=checkbox]").removeProp('checked', $(this).prop('checked'));
    //     }
               
    // });
    $('#locationthemesCheckAll').click(function () {  
        $('input:checkbox').prop('checked', this.checked);
    });
    $(".editentry").click(function(){
        let id = $(this).attr('data-id');
      // alert(id);
          $.ajax({
            url:getsiteurl() +'/salarysheetedit/edit/'+id,
            method:'get',
            success: function(data) {
              // alert(data.success);
              if(data.success = true)
                      $('.modal_body_salary').html(data.html);
                      $("#edit_modal").modal('show');
              },
          })
      });
  });
  
  $(".edit_mutilple_data").click(function(){
    var array = $.map($('input[name="locationthemes"]:checked'), function(c){return c.value; });
    console.log(array);
    // var ReportYear = $('#ReportYear').val();
    // var ReportMonth = $('#ReportMonth').val();
    $('#type').val('edit');
    $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
    $.ajax({
          url:getsiteurl() +'/salarysheetpopup',
          method:'post',
          data:{'month':$('#ReportMonth').val(),'year':$('#ReportYear').val(),"id":array},
          success: function(data) {
           // alert(data.success);
            $('#ReportMonthPopup').val($('#ReportMonth').val());
            $('#ReportYearPopup').val($('#ReportYear').val());
            if(data.success = true)
            $('.modal_body_salary_sheet_popup').html(data.html);
            $("#salary_sheet_popup_modal").modal('show');
            },
        })
  });
  $(".dateyearbutton").click(function(){
    if(($('#ReportMonthDropdown').val() != '' && $('#ReportYearDropdown').val() != '')){
      $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
      $.ajax({
            url:getsiteurl() +'/salarysheetpopup',
            method:'post',
            data:{'month':$('#ReportMonthDropdown').val(),'year':$('#ReportYearDropdown').val()},
            success: function(data) {
              $('#ReportMonthPopup').val($('#ReportMonthDropdown').val());
              $('#ReportYearPopup').val($('#ReportYearDropdown').val());
              if(data.success = true)
              $("#type").val(data.type);
              //alert(data.type);
              $('.modal_body_salary_sheet_popup').html(data.html);
              $("#salary_sheet_popup_modal").modal('show');
              },
          })
     
     }else{
       alert("Please enter start date and end date or start month and start year");
      return false;
     }
  });
  //  $('body').on('submit', '#reportform', function (e) {
    
     
  //  });

    $('body').on('submit', '#editform', function (e) {
    
    
      $( "#editform" ).submit();
    
   });
  
$("#form_reset").click(function(){
  $('#search_by_employee_id').val('').attr("selected", "selected");
  $('#search_by_user_name').val('').attr("selected", "selected");
   $( "#search_filter" ).submit();
});

$('#page_range').change(function(){

        $( "#getpage" ).submit();
         /* or:
         $('#formElementId').trigger('submit');
            or:
         $('#formElementId').submit();
         */
    });
    $(".sheetExitingDelete").click(function(){
      var id  = $(this).attr('data-id');
      swal({
          title: 'Are you sure you want remove salary sheet data?',
          text: '',
          icon: 'warning',
          buttons: ["Cancel", "Yes!"],
      }).then(function(value) {
          if (value) {
           
            $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
            $.ajax({
                  url:getsiteurl() +'/salarysheetDelete',
                  method:'post',
                  data:{'id':id},
                  success: function(data) {
                    // alert(data.success);
                  location.reload();
                }
          });
          }
      });


     
  });
    
</script>
@endsection
                                                   
                           
                            
                                                     
                          
                         
                         
                           
                      
                         
                        
                         
                           
                     
                         
                            
                          
                         
                         
                          
                          
                         
                         
                   
           