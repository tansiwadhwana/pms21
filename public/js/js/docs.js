///////// Add Technology
$( document ).ready(function() {
    $('[data-toggle="tooltip"]').popover({html:true});
    $('.select2').select2();
    $(document).on("click", ".techModal", function(){
        $.ajax({
            type: "get",
            url: getsiteurl() + '/docs_modal/',
            data: "",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                $('#addDocsModal').html(response);
                $('#addDocsModal').modal();
            }
        });
        return false;
    });

    $(document).on("click", ".editDocsModal", function(){
        var id = $(this).attr('docs-id');
        if(id != ''){
            jQuery.ajax({
                url  : getsiteurl() + '/docs_name/edit/'+ id,
                type : "get",
                success: function(data) {
                    $('#edit_name').val(data.name);
                    $('#id').val(data.id);
                    $('.edit_name-error').text('');
                    $('#editDocsModal').modal('show');
                }
            });
        }else{
            swal(response.message,"", "success");
        }
    });

    $('#editDocs').submit(function(evt) {
        evt.preventDefault();
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        if( $('#editDocs').valid() ) {
            var edit_name = $('#edit_name').val();
            var id = $('#id').val();
            jQuery.ajax({
                url  : getsiteurl() + '/docs_name/update',
                type : "POST",
                data: {_token: CSRF_TOKEN,id:id,edit_name:edit_name},
                success: function(response) {
                    if(response.status == "success"){
                        swal(response.message,"", "success");
                        $('#updateDocsModal').modal('toggle');
                        location.reload();
                    }
                    else{
                        $('.edit_name-error').html(response.message).show();
                    }
                }
            });
        }
    });

    editTechForm();

    function editTechForm() {
        $("#editDocs").validate({
            rules: {
                edit_name: "required",
            },
            messages: {
                edit_name: "The edit name field is required",
            },
        });
    }

    $('.deleteTech').on('click',function () {
        var docsId = $(this).attr('docs-id');
    
        if(docsId != ''){
            swal({
                title: "Are you sure you want to delete Documents?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    deleteDocs(docsId);
                }
            });
        }
    });

    function deleteDocs(docsId){
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        jQuery.ajax({
        url  : getsiteurl() + '/documents/delete',
        type : "DELETE",
        data: {_token: CSRF_TOKEN,docsId:docsId},
        success: function(response) {
                if(response.status == 'success'){
                    swal(response.message,"", "success");
                    location.reload();
                }else if(response.status == 'error'){
                    swal(response.message,"", "error");
                }
            }
        });
    }

    $('#page_rang_dropdown').change(function(){
        var limit = $(this).val();
        var link  = window.location.pathname;
        var query_string = window.location.search;

        var search_str = (query_string.indexOf('?') == 0 ? query_string.substring(1) : query_string);
        if(limit != '' && query_string.indexOf('search_submit') != -1){
            if(query_string.indexOf('page_range')==1){
                var page_range = query_string.slice(0,query_string.indexOf('='));
                var srch_str = query_string.slice(query_string.indexOf('&'));
                var redirectLink = link +page_range+'='+limit+srch_str;
            }else{
                var redirectLink = link +'?page_range='+limit + '&'+search_str;
            }
        }else{
            var redirectLink = link +'?page_range='+limit;
        }
        window.location = redirectLink;  
    });
});
