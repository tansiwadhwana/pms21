<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Fields;
use App\Models\Salary;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Schema;
use DynamicFieldsHelper;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    /*this function will get user data from user table and display on user module.*/
    public function index(Request $request)
    {
        $page_limit  = ($request['page_range'])?$request['page_range']:config('constant.recordPerPage');
        $array['table_name'] = 'users';
        $fields = DynamicFieldsHelper::getfieldlist($array);
        $array['pluck'] ='field_text';
        $fielddata = DynamicFieldsHelper::getfieldlist($array);
         $dataQuery = User::select()->where('status','=','Active');
         if ($request->has('search_submit') && $request->search_submit != '') {
            if ($request->has('search_by_user_name') && $request->search_by_user_name != '') {
                $dataQuery->where('name','like','%'. $request->search_by_user_name . '%');
            }
            if ($request->has('search_by_employee_id') && $request->search_by_employee_id != '') {
                $dataQuery->where('employee_id', $request->search_by_employee_id );
            }
         }
         $users=$dataQuery->latest()->paginate($page_limit);
       //  dd($users);
         $empName = User::where('status','Active')->orderBy('name','asc')->pluck('name')->toArray();
         $empId = User::where('status','Active')->orderBy('employee_id','asc')->pluck('employee_id')->toArray();
        return view('home',compact('users','fields','fielddata','request','empName','empId'));
    }

    /*this function will insert user api data to user table.*/
    public function users()
    { 
        $user = User::select()->where('id','!=',Auth::user()->id)->get();
        foreach($user as $val)
        {
            $val->delete();
        }

        $url = 'https://pms.tridhyatech.com/api/users';
        $ch = curl_init();
        $payload = '{""}';
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT,true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
// echo "<pre>";print_r($result);exit;
        $final_result=json_decode($result);
       if(!empty($final_result->data)){
            foreach($final_result->data as $index=>$value)
            {
            // echo "<pre>";print_r($value);exit;
                $salaryUser = Salary::where('employee_id',$value->employee_id)->first();
                if(!empty($salaryUser)){
                    $salaryUser->name = $value->first_name.' '.$value->last_name;
                    $salaryUser->confirmation_date = $value->confirmation_date;
                    $salaryUser->joining_date = $value->joining_date;
                    $salaryUser->save();
                }
                $user = New User();
                $user->employee_id = $value->employee_id;
                $user->company_id = $value->company_id;
            //  $user->old_user_id = $value->id;
                $user->first_name = $value->first_name;
                $user->last_name = $value->last_name;
                $user->password = $value->userpassword;
                $user->name = $value->first_name.' '.$value->last_name;
                $user->email = $value->company_email;
                $user->from_shift = $value->from_shift;
                $user->to_shift = $value->to_shift;
                $user->personal_email = $value->personal_email;
                $user->contact_number = $value->contact_number;
                $user->birth_date = $value->birthdate;
                $user->reporting_user = $value->reporting_user;
                $user->company_name = $value->company_name;
                $user->designation_name = $value->designation_name;
                $user->confirmation_date = $value->confirmation_date;
                $user->joining_date = $value->joining_date;
                $user->department = $value->department;
                $user->bank_name = $value->bank_name;
                $user->bank_account_no = $value->bank_account_no;
                $user->pan_no = $value->pan_no;
                $user->available_leaves = $value->available_leaves;
                $user->used_leaves = $value->used_leaves;
                $user->code = $value->code;
                $user->status = $value->status;
                $user->gratuity = $value->gratuity;
                $user->employee_status = $value->employee_status;
                $user->provi_fund = $value->provident_fund;
                $user->provi_fund_amount = $value->provident_fund_amount;
                $user->bonus_applicable = $value->bonus_applicable;
                $user->salary_structure = $value->salary_structure;
                $user->ifsc_code = $value->ifsc_code;
                $user->save();
            }
       }
        
     //  exit;   
        curl_close($ch);
        alert()->success('All Users Synchronization Done')->persistent('close')->autoclose("36000");
        return redirect('/home');
    }
     public function usersync()
    { 
        $userArray = Salary::pluck('employee_id')->toArray();
        //echo "<pre>";print_r($user);exit;
        // foreach($user as $val)
        // {
        //     $val->delete();
        // }

        $url = 'https://pms.tridhyatech.com/api/getActivaUser';
        $ch = curl_init();
        $payload = '{""}';
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT,true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);

    
        $final_result=json_decode($result);
    //echo "<pre>";print_r($result);exit;
       if(!empty($final_result)){
            foreach($final_result as $index=>$value)
            {
            // echo "<pre>";print_r($value);exit;
                if (($key = array_search($value->employee_id, $userArray)) !== false) {
                    unset($userArray[$key]);
                }
                $user = Salary::where('employee_id',$value->employee_id)->first();
                if(!empty($user)){
                    $user->name = $value->name;
                    $user->joining_date = $value->joining_date;
                    $user->confirmation_date = $value->confirmation_date;
                    $user->employee_id = $value->employee_id;
                    $user->save();
                }else{
                    $user = new Salary();
                    $user->name = $value->name;
                    $user->joining_date = $value->joining_date;
                    $user->confirmation_date = $value->confirmation_date;
                    $user->employee_id = $value->employee_id;
                    $user->save();
                }
            }
       }
       if(!empty($userArray)){
            $delete = Salary::whereIn('employee_id',$userArray)->delete();
       }  
        curl_close($ch);
        alert()->success('All Users Synchronization Done')->persistent('close')->autoclose("36000");
        return redirect('/salary');
    }
    /*this function will display user fields page.*/
    public function DisplayFiled()
    {
        $array['table_name'] = 'users';
        $array['pluck'] ='field_name';
        $field_id = DynamicFieldsHelper::getfieldlist($array);
         $fields = Schema::getColumnListing('users');
         $final_data = [];
         foreach ($fields as $key => $value) {
             $final_data[$value] = ucwords(str_replace('_',' ',$value));
         }
        return view('UpdateUserField',compact('field_id','final_data'));
    }
    public function ReportDisplayFiled(){
        $array['table_name'] = 'reports';
        $array['pluck'] ='field_name';
        $field_id = DynamicFieldsHelper::getfieldlist($array);
        return view('UpdateReportField',compact('field_id'));
    }
    /*this function will insert fields to fields table which fields person want to display in user data.*/
    public function InsertFields(Request $request)
    {
         $fields = Schema::getColumnListing('users');
         $final_data = [];
         foreach ($fields as $key => $value) {
             $final_data[$value] = ucwords(str_replace('_',' ',$value));
         }
        $array['table_name'] = 'users';
        $fields = DynamicFieldsHelper::getfieldlist($array);
        foreach($fields as $value)
        {
            $value->delete();
        }

        $FieldData=$request->update_user_row;
        if(isset($FieldData) && !empty($FieldData))
        {
            foreach($FieldData as $val)
            {

                DynamicFieldsHelper::storeFieldTable(["field_name" => $val,"field_text" => $final_data[$val],"table_name" => 'users']);
            }
        }
        alert()->success('Display Fields Changed Successfully')->persistent('close')->autoclose("36000");
        return redirect('/home');
    }
    public function InsertReportFields(Request $request){
        $final_data = config('constant.reportfields');
        $array['table_name'] = 'reports';
        $fields = DynamicFieldsHelper::getfieldlist($array);
        foreach($fields as $value)
        {
            $value->delete();
        }

        $FieldData=$request->update_user_row;
        if(isset($FieldData) && !empty($FieldData))
        {
            foreach($FieldData as $val)
            {

                DynamicFieldsHelper::storeFieldTable(["field_name" => $val,"field_text" => $final_data[$val],"table_name" => 'reports']);
            }
        }
        alert()->success('Display Fields Changed Successfully')->persistent('close')->autoclose("36000");
        return redirect('/report');
    }
    /*this function will edit user data when click on sync button of particular user data.*/
    public function edit($id)
    {
        $url = 'https://pms.tridhyatech.com/api/users/'.$id;
        $ch = curl_init($url);
        $payload = '{""}';
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT,true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $final_result=json_decode($result);
        if(!empty($final_result->data)){
            foreach($final_result->data as $index=>$value)
            {
                $salaryUser = Salary::where('employee_id',$value->employee_id)->first();
                if(!empty($salaryUser)){
                    $salaryUser->name = $value->first_name.' '.$value->last_name;
                    $salaryUser->confirmation_date = $value->confirmation_date;
                    $salaryUser->joining_date = $value->joining_date;
                    $salaryUser->save();
                }
                $userid = User::where('employee_id','=',$id)->pluck('id')->toArray();
                $user=User::find($userid[0]);
                $user->employee_id = $value->employee_id;
                $user->company_id = $value->company_id;
            //  $user->old_user_id = $value->old_id;
                $user->first_name = $value->first_name;
                $user->last_name = $value->last_name;
                $user->password = $value->userpassword;
                $user->name = $value->first_name.' '.$value->last_name;
                $user->email = $value->company_email;
                $user->from_shift = $value->from_shift;
                $user->to_shift = $value->to_shift;
                $user->personal_email = $value->personal_email;
                $user->contact_number = $value->contact_number;
                $user->birth_date = $value->birthdate;
                $user->reporting_user = $value->reporting_user;
                $user->company_name = $value->company_name;
                $user->designation_name = $value->designation_name;
                $user->confirmation_date = $value->confirmation_date;
                $user->joining_date = $value->joining_date;
                $user->department = $value->department;
                $user->bank_name = $value->bank_name;
                $user->bank_account_no = $value->bank_account_no;
                $user->pan_no = $value->pan_no;
                $user->available_leaves = $value->available_leaves;
                $user->used_leaves = $value->used_leaves;
                $user->code = $value->code;
                $user->status = $value->status;
                $user->gratuity = $value->gratuity;
                $user->employee_status = $value->employee_status;
                $user->provi_fund = $value->provident_fund;
                $user->provi_fund_amount = $value->provident_fund_amount;
                $user->bonus_applicable = $value->bonus_applicable;
                $user->salary_structure = $value->salary_structure;
                $user->save();
            }
        }

        curl_close($ch);
        alert()->success('User Synchronize Done')->persistent('close')->autoclose("36000");
        return redirect('/home');
    }

    public function logout(Request $request){
        Auth::logout();
        return redirect('/login');
    }
    public function changePassword(Request $request){
        if ($request->isMethod('post')) {
            $data = $request->input();
            $user = Auth::user();
            if (Hash::check($data['password'], $user->password)) {
                $validator = Validator::make($request->all(), [
                     'new_password' => 'required|different:password',
                     'confirm_password' => 'required|same:new_password',
                ]);
            
                if ($validator->fails()) {
                    return redirect('/changepassword')->withErrors($validator)->withInput();
                }
            }else{
                $validator = Validator::make($request->all(), [
                        'password' => 'required|same:old password'
                ]);
                if ($validator->fails()) {
                   // return Redirect::to('changepassword')->withErrors($validator);
                    return redirect('/changepassword')->withErrors($validator)->withInput();
                    
                }
            }
            $user->password = Hash::make($data['new_password']);
            $user->save();

            // $userData = new User();
            // $userDetails = $userData->getUserDetails($user->id);

            // $sendData = array('password' => $data['new_password'], 'first_name' => $userDetails['first_name'], 'last_name' => $userDetails['last_name']);

            
            alert()->success('Your password changed successfully!')->persistent('close')->autoclose("36000");
           // return Redirect::to('changepassword');
            return redirect('/changepassword');

        }
    //   /  echo "<pre>";print_r($request->all());exit;
        return view('change_password',compact('request'));
        //return view('pms.change_password');
    }
    
}
