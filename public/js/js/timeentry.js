$(document).ready(function(){
    $('#validatedCustomFile').change(function(e){
        var fileName = e.target.files[0].name;
        $('#uploadedFileName').text(fileName);
    });
    
    $('#validatedManuallySheet').change(function(e){
        var fileName = e.target.files[0].name;
        $('#uploadedManuallySheetFileName').text(fileName);
    });

    $("#frmUploadTimeEntry").validate({
        rules: {
		xlsfile: {required:true,extension:"xls"}
        },
        messages: {
            xlsfile: {
            	required: "Please select file",
            	extension:"Please select .xls extension file to upload"
            }
        },
        submitHandler: function (form){
            form.submit();
        }
    });
    $("#frmUploadManuallyTimeEntry").validate({
        rules: {
		csvfile: {required:true,extension:"csv"}
        },
        messages: {
            xlsfile: {
            	required: "Please select file",
            	extension:"Please select .csv extension file to upload"
            }
        },
        submitHandler: function (form){
            form.submit();
        }
    });
    $("#entryDate").datepicker({
        todayBtn: 1,
        autoclose: true,
        format: 'yyyy-mm-dd',
    });
    $('#deleteTimeEntry').click(function(){
        var companyId = $('#company').val();
        var entryDate = $('#entryDate').val();
        if(companyId != '' && entryDate != ''){
                swal({
                    title: "Are you sure you want to delete "+entryDate+" entry",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then(function(willDelete) {
                if (willDelete) {
                  deleteTimeEntry(companyId,entryDate);
                }
             });
        }
    });	
    
});
function deleteTimeEntry(companyId,entryDate){
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        jQuery.ajax({
            url  : getsiteurl() + '/delete/time_entries',
            type : "DELETE",
            data: {_token: CSRF_TOKEN,companyId:companyId,entryDate:entryDate},
            success: function(response) {
                if(response.status == 'success'){
                    swal(response.message,"", "success");
                }else if(response.status == 'error'){
                    swal(response.message,"", "error");
                }
                 location.reload();
        }
    });
}