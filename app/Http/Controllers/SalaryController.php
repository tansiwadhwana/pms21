<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\report;
use App\Models\User;
use App\Models\Salary;
use App\Models\Settings;
use App\Models\Attendence;
use App\Imports\UsersSalaryImport;
use Illuminate\Support\Facades\Validator;
use App\Models\Salarysheet;
use App\Models\Salarysheet_attechment;
use App\Exports\Exportsalarysheet;
use App\Imports\TdsUpload;
use App\Models\Fields;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Excel;
use DB;


use Illuminate\Support\Facades\Auth;
class SalaryController extends Controller
{
    /*this function will display report file where person can apply filter on report.*/
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function salary(Request $request)
    {
      $page_limit  = ($request['page_range'])?$request['page_range']:config('constant.recordPerPage');
      $empName = User::where('status','Active')->orderBy('name','asc')->pluck('name')->toArray();
      $empId = User::where('status','Active')->orderBy('employee_id','asc')->pluck('employee_id')->toArray();
       $dataQuery = Salary::select();
      if ($request->has('search_submit') && $request->search_submit != '') {
        if ($request->has('search_by_user_name') && $request->search_by_user_name != '') {
            $dataQuery->where('name','like','%'. $request->search_by_user_name . '%');
        }
        if ($request->has('search_by_employee_id') && $request->search_by_employee_id != '') {
            $dataQuery->where('employee_id', $request->search_by_employee_id );
        }
     }
     $salaryData = $dataQuery->paginate($page_limit);
      return view('salary.list',compact('empName','empId','salaryData','request'));
    }

    public function create(){
        $empName = User::where('status','Active')->orderBy('name','asc')->pluck('name')->toArray();
        $empId = User::where('status','Active')->orderBy('employee_id','asc')->pluck('employee_id')->toArray();
        return view('salary.create',compact('empName','empId'));
    }
    public function getUserDetails(Request $request){
        if(isset($request->name)){
            $userDetails = User::where('name',$request->name)->first();
        }
        if(isset($request->empid)){
            $userDetails = User::where('employee_id',$request->empid)->first();
        }
        return response()->json(array('success' => true, 'details'=>$userDetails));
    }
    public function store(Request $request){
        $salary = new Salary();
        $salary->name = $request->name;
        $salary->employee_id = $request->employee_code;
        $salary->joining_date = $request->joining_date;
        $salary->confirmation_date = $request->confirmation_date;
        $salary->salary_amount = $request->salary;
        $salary->save();
        alert()->success('Record Inserted Successfully')->persistent('close')->autoclose("36000");
        return redirect('/salary');
    }
    public function edit(Request $request,$id){
        $empName = User::where('status','Active')->pluck('name')->toArray();
        $empId = User::where('status','Active')->pluck('employee_id')->toArray();
        $data = Salary::find($id);
        return view('salary.edit',compact('empName','empId','data'));
    }
    public function update(Request $request){
        $salary = Salary::find($request->hidden_id);
        $salary->name = $request->name;
        $salary->employee_id = $request->employee_code;
        $salary->joining_date = $request->joining_date;
        $salary->confirmation_date = $request->confirmation_date;
        $salary->salary_amount = $request->salary;
        $salary->save();
        alert()->success('Record Updated Successfully')->persistent('close')->autoclose("36000");
        return redirect('/salary');
    }

     public function Salarysheet(Request $request,$m = null ,$y = null)
    {
         $month = '';
        $year = '';
      // echo '<pre>';print_r($request->all());exit;
     //   $url = 'http://localhost/pms19/public/api/attendnce/'.$m.'/'.$y;
     //  
     //   $ch = curl_init();
     //    $payload = '{""}';
     //    curl_setopt($ch, CURLOPT_URL, $url);
     //    curl_setopt($ch, CURLOPT_FRESH_CONNECT,true);
     //    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
     //    $AttendenceResult = curl_exec($ch);
     //    $AttendenceData=json_decode($AttendenceResult);
     //      // echo "<pre>";print_r($AttendenceData);exit;
     //    if(!empty($AttendenceData->data)){
     //    foreach($AttendenceData->data as $index=>$value)
     //    {
     //        $attendence = New Attendence();
     //        $attendence->entry_date = $value->entry_date;
     //        $attendence->emp_code = $value->emp_code;
     //        $attendence->emp_name = $value->emp_name;
     //        $attendence->emp_comp = $value->emp_comp;
     //        $attendence->first_in = $value->first_in;
     //        $attendence->last_out = $value->last_out;
     //        $attendence->late_comer = $value->late_comer;
     //        $attendence->full_day = $value->full_day;
     //        $attendence->early_going = $value->early_going;
     //        $attendence->absent = $value->absent;
     //        $attendence->month = $request->ReportMonth;
     //        $attendence->year = $request->ReportYear;
     //        $attendence->save();
            
     //    }
     // }
     //    curl_close($ch);
    
       $page_limit  = ($request['page_range'])?$request['page_range']:config('constant.recordPerPage');
       // echo 'hidden'; print_r($page_limit);exit;
       $genralsettingdata = Settings::first();
       $empName = User::where('status','Active')->orderBy('name','asc')->pluck('name')->toArray();
       $empId = User::where('status','Active')->orderBy('employee_id','asc')->pluck('employee_id')->toArray();

         if(isset($request['ReportMonth']) && isset($request['ReportYear']) && !empty($request->ReportMonth) && !empty($request->ReportYear))
                { 
                        $month = $request->ReportMonth;
                        $year = $request->ReportYear;
                }else{
                        if(!empty($m) && !empty($y))
                        {
                             $month = $m;
                              $year = $y;
  
                       // echo "here";

                              $dataQuery = Salarysheet::with('salaryDetails','getuser')->where('month',$month)->where('year',$year);
                              if ($request->has('search_submit') && $request->search_submit != '') {
                                if ($request->has('search_by_user_name') && $request->search_by_user_name != '') {
                                    $dataQuery->where('name','like','%'. $request->search_by_user_name . '%');
                                }
                                if ($request->has('search_by_employee_id') && $request->search_by_employee_id != '') {
                                    $dataQuery->where('employee_id','like','%'.  $request->search_by_employee_id .'%');
                                }
                             }
                                $salraysheetdata = $dataQuery->groupBy('salarysheet.employee_id')->paginate($page_limit);
                                 foreach ($salraysheetdata as $key => $value) {
                                       if(isset($value->salaryDetails) && !empty($value->salaryDetails)){
                                             $getusercompoff = report::where('employee_code',$value->employee_id)->where('month',$month)->where('year',$year)->first();
                                // echo "<pre>";print_r($getusercompoff);exit;
                                            $getsalarydata =  $this->salaryCalculation($value,$genralsettingdata,$getusercompoff);
                                            $value->salaryCalculation = $getsalarydata;
//echo "<pre>";print_r($getsalarydata);exit;
                                              // $value->additional = $getsalarydata['additional'];
                                              // $value->gratuity = $getsalarydata['gratuity'];
                                              // $value->salary_payable = $getsalarydata['salary_payable'];
                                              // $value->net_payable = $getsalarydata['net_payable'];
                                            // $value->basic_salary = $getsalarydata['Basic_Salary'];
                                            // $value->houseRentAllowCtc = $getsalarydata['houseRentAllowCtc'];
                                            // $value->specialAllowCtc = $getsalarydata['specialAllowCtc'];
                                            // $value->otherAllowCtc = $getsalarydata['otherAllowCtc'];
                                            // $value->Adjustments = $getsalarydata['Adjustments'];
                                            // $value->refund = $getsalarydata['refund'];
                                            // $value->total_eranings = $getsalarydata['total_eranings'];
                                            // $value->permanent_deduction = $getsalarydata['permanent_deduction'];
                                            // $value->pf_employee_contribution = $getsalarydata['pf_employee_contribution'];
                                            // $value->gratuity = $getsalarydata['gratuity'];
                                            // $value->professional_tax = $getsalarydata['professional_tax'];
                                            // $value->other_deduction = $getsalarydata['other_deduction'];
                                            // $value->variable_deduction = $getsalarydata['variable_deduction'];
                                            // $value->lwp = $getsalarydata['lwp'];
                                            // $value->net_amount = $getsalarydata['net_amount'];
                                           //  echo "<pre>";print_r($value);exit;
                                         }      
                                }
                             
                      //      echo "out";exit;
                                 return view('salary.salarysheet',compact('salraysheetdata','genralsettingdata','month','year','empName','empId','request'));

                        }
                           
                     }


                if(isset($request['export_to_excel']))
                 {

                    $getexistingsalarysheet = Salarysheet_attechment::where('month',$month)->where('year',$year)->first(); 
                             if(!empty($getexistingsalarysheet))
                             {
                                $getexistingsalarysheet->delete();
                             }

                 }
                 if(isset($request['export_to_excel']))
                 {  
                      
                     $salraysheet=Salarysheet::with('salaryDetails','getuser')->where('month',$month)->where('year',$year)->groupBy('salarysheet.employee_id')->get();
                      $genralsettingdata = Settings::first();
                       foreach ($salraysheet as $key => $value) {
                                       if(isset($value->salaryDetails) && !empty($value->salaryDetails)){
                                            $getusercompoff = report::where('employee_code',$value->employee_id)->where('month',$month)->where('year',$year)->first();

                                            $getsalarydata =  $this->salaryCalculation($value,$genralsettingdata,$getusercompoff);

                                              $value->salaryCalculation = $getsalarydata;
                                             
                                         }      
                                }
                             // foreach ($salraysheet as $key => $savevalue) {
                             //          $updatesalraysheet=Salarysheet::find($savevalue->id);
                             //                   // echo '<pre>'; print_r($savevalue);exit;
                             //          $updatesalraysheet->salary = isset($savevalue->salaryDetails->salary_amount)?$savevalue->salaryDetails->salary_amount:''; 
                             //          $updatesalraysheet->gratuity = $savevalue->gratuity;
                             //          $updatesalraysheet->provi_Fund_18_persent = isset($savevalue->getuser->provi_fund_amount)?$savevalue->getuser->provi_fund_amount:'';;
                             //          $updatesalraysheet->salary_payable = $savevalue->salary_payable;
                             //          $updatesalraysheet->net_payable = $savevalue->net_payable;
                             //          $updatesalraysheet->save();


                             //    }   

                         Excel::store(new Exportsalarysheet($salraysheet,$genralsettingdata),$month.'_ '.$year.'.xls');   
                         $filename = $month.'_ '.$year.'.xls';
                        
            
                           $salrarysheet_attechment = new Salarysheet_attechment();
                           $salrarysheet_attechment->document = $filename;
                           $salrarysheet_attechment->month = $month;
                           $salrarysheet_attechment->year = $year;
                           $salrarysheet_attechment->save();
                
                         return Excel::download(new Exportsalarysheet($salraysheet,$genralsettingdata),$month.'_ '.$year.'.xls');
                

                     }
              
                    
                 if(!empty($month) && !empty($year))
                 {
                   
                $existingsalarysheet = Salarysheet_attechment::where('month',$month)->where('year',$year)->get();

                    if(!$existingsalarysheet->isEmpty() && !isset($request->search_submit) && $request->search_submit != '')
                    {    
                       
                        $getexistingsalarysheet = Salarysheet_attechment::where('month',$month)->where('year',$year)->first(); 
                   
                        return view('salary.salarysheet',compact('getexistingsalarysheet','request','empName','empId','month','year'));
                    }
                    else{
                        
                          $dataQuery =Salarysheet::select()->where('month',$month)->where('year',$year);
                         if ($request->has('search_submit') && $request->search_submit != '') {
                            if ($request->has('search_by_user_name') && $request->search_by_user_name != '') {
                                $dataQuery->where('name','like','%'. $request->search_by_user_name . '%');
                            }
                            if ($request->has('search_by_employee_id') && $request->search_by_employee_id != '') {
                                $dataQuery->where('employee_id','like','%'.  $request->search_by_employee_id .'%');
                            }
                         }
                         $salraysheetdata1 =  $dataQuery->groupBy('salarysheet.employee_id')->paginate($page_limit);

                            
                            if($salraysheetdata1->isEmpty()){
         
                                 $this->SalraysheetStore($request);



                           

                             $dataQuery =Salarysheet::with('salaryDetails','getuser')->where('month',$month)->where('year',$year);
                               
                              if ($request->has('search_submit') && $request->search_submit != '') {
                                if ($request->has('search_by_user_name') && $request->search_by_user_name != '') {
                                    $dataQuery->where('name','like','%'. $request->search_by_user_name . '%');
                                }
                                if ($request->has('search_by_employee_id') && $request->search_by_employee_id != '') {
                                    $dataQuery->where('employee_id','like','%'.  $request->search_by_employee_id .'%');
                                }
                             }
                             $salraysheetdata = $dataQuery->groupBy('salarysheet.employee_id')->paginate($page_limit);
                                foreach ($salraysheetdata as $key => $value) {

                                      if(isset($value->salaryDetails) && !empty($value->salaryDetails)){

                                            $getusercompoff = report::where('employee_code',$value->employee_id)->where('month',$month)->where('year',$year)->first();
                                          
                                          $getsalarydata =  $this->salaryCalculation($value,$genralsettingdata,$getusercompoff);
                                          //  // echo "456 <pre>";print_r($getsalarydata);exit;
                                          // $value->additional = $getsalarydata['additional'];
                                        $value->salaryCalculation = $getsalarydata;
                                          // $value->gratuity = $getsalarydata['gratuity'];
                                          // $value->salary_payable = $getsalarydata['salary_payable'];
                                          // $value->net_payable = $getsalarydata['net_payable'];
                                        
                                      }
                                }
                               
                        
                              return view('salary.salarysheet',compact('salraysheetdata','genralsettingdata','month','year','empName','empId','request'));
                             }else{

                               $dataQuery =Salarysheet::with('salaryDetails','getuser')->where('month',$month)->where('year',$year);
                                 
                                 
                                if ($request->has('search_submit') && $request->search_submit != '') {
                                    if ($request->has('search_by_user_name') && $request->search_by_user_name != '') {
                                        $dataQuery->where('name','like','%'. $request->search_by_user_name . '%');
                                    }
                                    if ($request->has('search_by_employee_id') && $request->search_by_employee_id != '') {
                                        $dataQuery->where('employee_id','like','%'.  $request->search_by_employee_id .'%');
                                    }
                                 }
                                 $salraysheetdata = $dataQuery->paginate($page_limit);
                                   foreach ($salraysheetdata as $key => $value) {
                                
                                   
                                       if(isset($value->salaryDetails) && !empty($value->salaryDetails)){
                                         
                                             $getusercompoff = report::where('employee_code',$value->employee_id)->where('month',$month)->where('year',$year)->first();
                                             
                                            $getsalarydata =  $this->salaryCalculation($value,$genralsettingdata,$getusercompoff);

                                              // $value->additional = $getsalarydata['additional'];
                                              // $value->gratuity = $getsalarydata['gratuity'];
                                              // $value->salary_payable = $getsalarydata['salary_payable'];
                                              // $value->net_payable = $getsalarydata['net_payable'];
                                            $value->salaryCalculation = $getsalarydata;
                                         }      
                                }

                                 return view('salary.salarysheet',compact('salraysheetdata','genralsettingdata','month','year','empName','empId','request'));
                             }
                    }
                 } 
           
                   
             
                         

                   
         // echo 'hidden'; print_r($page_limit);exit;
     
             
            $salraysheetattechmentdata=Salarysheet_attechment::select()->orderBy('month','asc')->paginate($page_limit);
    
                
 
               
                     return view('salary.salarysheet',compact('salraysheetattechmentdata','month','year','request','empName','empId'));
    }
    public function salaryCalculation($salarysheet,$Settings,$getcompoff,$editAddtional = ''){
        $data['incase_comp_off'] = $getcompoff->incase_comp_off;
        $joining_date = $salarysheet->getuser->joining_date;
        if(date("m", strtotime($joining_date)) == $salarysheet->month && date("Y", strtotime($joining_date)) == $salarysheet->year)
            {
                
                $data['present_days'] = ($getcompoff->total_worked_days + $getcompoff->leave_of_month);
            }else{

                $data['present_days'] = ($salarysheet->month_working_days - $getcompoff->leave_of_month);
            }
       // echo "<pre>";print_r($getcompoff);exit;
        if(($getcompoff->credit_leave_cf - $getcompoff->leave_of_month) < 0){
            
            if(date("m", strtotime($joining_date)) == $salarysheet->month && date("Y", strtotime($joining_date)) == $salarysheet->year)
            {
        //echo $joining_date;exit;
                $data['total_days'] = ($data['present_days'] - ($getcompoff->leave_of_month - $getcompoff->credit_leave_cf));
            }else{
                $data['total_days'] = ($salarysheet->month_working_days - ($getcompoff->leave_of_month - $getcompoff->credit_leave_cf));
            }
            
        }else{
             if(date("m", strtotime($joining_date)) == date("m") && date("Y", strtotime($joining_date)) == date("Y"))
            {
               $data['total_days'] = $data['present_days'] + $getcompoff->leave_of_month; 
            }else{
                $data['total_days'] = $getcompoff->total_worked_days + $getcompoff->leave_of_month; 
            }
        }
        $data['salary'] = $salarysheet->salaryDetails->salary_amount;
        $data['additional'] = ($data['salary'] * $getcompoff->incase_comp_off) / $salarysheet->month_working_days;
        $data['add'] = $salarysheet->add;
        $data['permenant_deduction'] = $salarysheet->permenant_deduction;
        $data['refund'] = $salarysheet->refund;
        $data['salary_payable'] = (($data['salary'] + $data['additional'] - $data['permenant_deduction'])* $data['total_days'])/$salarysheet->month_working_days-$data['refund']+$data['add'];
        if($data['salary_payable'] >= 12000){
            $professional_tax = 200;
        }elseif($data['salary_payable'] >= 9000){
             $professional_tax = 150;
        }elseif ($data['salary_payable'] >= 6000) {
            $professional_tax = 80;
        }else{
             $professional_tax = 0;
        }
        $data['p_tax'] = $professional_tax;
        if(!empty($salarysheet->tds) || $salarysheet->tds != 0){
            $tds = $salarysheet->tds;
        }else if($salarysheet->getuser->salary_structure == 2 ){
            $tds = ($data['salary']*10)/100;
        }else if($salarysheet->getuser->salary_structure == 1 ){
            $tds = 0;
        }
        else if($data['salary'] >= 10000 && $data['salary'] <= 21000){
            $tds = ($data['salary']*10)/100;
        }else{
            $tds = $salarysheet->tds;
        }
        $data['tds'] = $tds;
        if(!empty($salarysheet->gratuity) || $salarysheet->gratuity != 0){
            $data['gratuity'] = $salarysheet->gratuity;
        }else{
            $data['gratuity'] = ($data['salary']*$Settings->gratuity)/100;
        }
        if(!empty($salarysheet->provi_Fund_18_persent) || $salarysheet->provi_Fund_18_persent != 0){
            $data['provi_fund'] = $salarysheet->provi_Fund_18_persent;
        }else if($salarysheet->getuser->provi_fund == 1){
            $data['provi_fund'] = ($data['salary']* $Settings->provi_fund)/100;
        }else{
            $data['provi_fund'] = 0;
        }

        $data['other_deduction'] = $salarysheet->other_deduction;
        $data['month_working_days'] = $salarysheet->month_working_days;
        $data['net_payable'] = $data['salary_payable'] - $data['p_tax'] - $data['tds'] - $data['gratuity'] - $data['provi_fund'] - $data['other_deduction'];
        return $data;
    }
    public function salaryCalculationAjax(Request $request){
        $salarysheet = Salarysheet::find($request->id);
        $data = array();
        $Settings = Settings::first();
        $add = $request->additional;
        $permenant_deduction = $request->permenant_deduction;
        $refund = $request->refund;
        $other_deduction = $request->other_deduction;
        $data['salary_payable']  = (($request->salary + $request->additional - $permenant_deduction)* $request->total_days)/$request->month_working_days-$refund+$add;
       // $data['other_deduction'] = $salarysheet->other_deduction;
        $data['net_payable'] = $data['salary_payable'] - $request->proffessional_tax - $request->tds - $request->gratuity - $request->provi_Fund_18_persent - $other_deduction;

       return $data;



   }
     public function SalraysheetStore(Request $request)
       {
        
        $month = date('m');
                $salarysheet = User::select('users.employee_id','users.name','designation_name','pan_no','bank_name','bank_account_no','available_leaves','gratuity','users.joining_date','reports.total_number_of_days','reports.total_working_days','reports.total_worked_days','reports.comp_off','reports.balance_leave','reports.credit_leave_cf','reports.leave_of_month','reports.balance_leave_after_companyoff','users.joining_date','attendence.month','users.status')->leftjoin('salarys','users.employee_id','salarys.employee_id')->leftjoin('reports','reports.employee_code','users.employee_id')->leftJoin('attendence','users.employee_id', 'attendence.emp_code')->whereMonth('users.joining_date','<=',$m)
                       
                 ->where(function($qry) use($month,$request){
      
             
      
                                  

    
             if(isset($request['ReportMonth']) && isset($request['ReportYear'])){
                   
                   $qry->whereMonth('attendence.entry_date','=',$m)->whereYear('attendence.entry_date','=',$y);
                    }
                   
        
    })->get();
  // echo "<pre>";print_r($salarysheet);exit;
           
                if(!empty($salarysheet)){
                    foreach($salarysheet as $index=>$value)
                    {
                        // echo '<pre>'; print_r($value->designation_name);exit;
                        $insertsalary = New Salarysheet();
                        $insertsalary->employee_id = $value->employee_id;
                        $insertsalary->name = $value->name;
                        $insertsalary->designation = $value->designation_name;
                        $insertsalary->joining_date = $value->joining_date;
                        $insertsalary->pan_number = $value->pan_no;
                        $insertsalary->bank_name = $value->bank_name;
                        $insertsalary->account_number = $value->bank_account_no;
                        $insertsalary->leaves = $value->available_leaves;

                        $insertsalary->month_days = $value->total_number_of_days;
                        $insertsalary->month_working_days = $value->total_working_days;
                        $insertsalary->working_days = ($value->total_worked_days + $value->leave_of_month);
                        $insertsalary->present_days = $value->total_worked_days;
                        $insertsalary->total_days = $value->total_working_days;

                        
                        $insertsalary->extra_leave  = $value->comp_off;
                        $insertsalary->credit_leave_cf  = $value->credit_leave_cf;
                        $insertsalary->leave_of_month  = $value->leave_of_month;
                        $insertsalary->balance_leave  = $value->balance_leave;
                        $insertsalary->balance_leave_after_companyoff  = $value->balance_leave_after_companyoff;

                      
                        $insertsalary->gratuity = 0;
                        $insertsalary->month = $request->ReportMonth;
                        $insertsalary->year = $request->ReportYear;
                        $insertsalary->lwp = $value->lwp;
                        $insertsalary->save();
                     
                    }
                }
         return redirect('/salarysheet');

      }


     public function EditSalarysheet($id)
     {
       
        $data = Salarysheet::with('salaryDetails','getuser')->find($id);
         $genralsettingdata = Settings::first();
           $getusercompoff = report::where('employee_code',$data->employee_id)->where('month',$data->month)->where('year',$data->year)->first();
           //echo "<pre>";print_r($getusercompoff);exit;
       if(isset($data->salaryDetails) && !empty($data->salaryDetails)){
              $getsalarydata =  $this->salaryCalculation($data,$genralsettingdata,$getusercompoff);
                                              // $data->additional = $getsalarydata['additional'];
                                              // $data->gratuity = $getsalarydata['gratuity'];
                                              // $data->salary_payable = $getsalarydata['salary_payable'];
                                              // $data->net_payable = $getsalarydata['net_payable'];
              $data->salaryCalculation = $getsalarydata;
        }                                      
       // / echo "<pre>";print_r($getusercompoff);exit;
        $editdatahtml = view("salary.edit_salarysheet",compact('data','genralsettingdata'))->render();
       

       return response()->json(array('success' => true, 'html'=> $editdatahtml) );



     }

     public function UpdateSalarysheet(Request $request)
     {
         
        $updatesheet = Salarysheet::find($request->id);
        $updatesheet->employee_id = $request->emp_id;
        $updatesheet->name = $request->emp_name;
        $updatesheet->designation = $request->designation;
        $updatesheet->technology = $request->technology;
        $updatesheet->joining_date = $request->joining_date;
        $updatesheet->pan_number = $request->pan_number;
        $updatesheet->bank_name = $request->bank_name;
        $updatesheet->account_number = $request->account_number;
        $updatesheet->ifsc_code = $request->ifsc_code;
        $updatesheet->UAN_number = $request->UAN_number;
        
        $updatesheet->month_days = $request->month_days;
        $updatesheet->working_days = $request->working_days;
        
        $updatesheet->present_days = $request->present_days;
        $updatesheet->total_days = $request->total_days;
        $updatesheet->credit_leave_cf = $request->credit_leave_cf;
        $updatesheet->leave_of_month = $request->leave_of_month;
        $updatesheet->balance_leave = $request->balance_leave;
        $updatesheet->extra_leave = $request->extra_leave;
        $updatesheet->balance_leave_after_companyoff = $request->balance_leave_after_companyoff;
        $updatesheet->salary = $request->salary;
        $updatesheet->additional = $request->additional;
        $updatesheet->provi_Fund_18_persent = $request->provi_Fund_18_persent;

        $updatesheet->salary_payable = $request->salary_payable;
        $updatesheet->permenant_deduction = $request->permenant_deduction;
        $updatesheet->refund = $request->refund;
        $updatesheet->tds = $request->tds;
        $updatesheet->gratuity = $request->gratuity;
        $updatesheet->other_deduction = $request->other_deduction;
        $updatesheet->resignation_hold_release = $request->resignation_hold_release;
        $updatesheet->net_payable = $request->net_payable;
        $updatesheet->mode = $request->mode;
        $updatesheet->note = $request->note;
        $updatesheet->save();
       
        alert()->success('Record Updated Successfully')->persistent('close')->autoclose("36000");
        return redirect('/salarysheet/'.$updatesheet->month.'/'.$updatesheet->year);

     }
     public function userSalaryUpload(Request $request){
        if ($request->hasFile('csvfile')) {      
            $validator=Validator::make($request->all(),[
                'csvfile'=>'required|mimes:xlsx,xls'
            ]); 
            if ($validator->fails()) {
               // Alert::error('Oops, Invalid file type or file seems to be missing!');
                alert()->error('Oops, Invalid file type or file seems to be missing!')->persistent('close')->autoclose("36000");
                return redirect('/salary')
                ->withErrors($validator)
                ->withInput();
            }
            else {
                $file = $request->file('csvfile');
                $extension = $file->getClientOriginalExtension();
                $filename = 'user_salary_'.time().'.'.$extension;
                if ($request) {
                    Storage::disk('local')->putFileAs('', $file, $filename);
                }
                $importError = array();
                Excel::import(new UsersSalaryImport, $filename);
                $importError = Session::get('UserSalaryImportError');
                
                Session::forget('UserSalaryImportError');
                if(count($importError)>0){
                    $count = $importError[0]['success'];
                    alert()->success(' '.$count.' Record Successfully Updated!')->persistent('close')->autoclose("36000");
                }
                $importError = Session::forget('UserSalaryImportError');
                return redirect('/salary');
            }
        }
        return redirect('/salary');
     }
     public function salarySheetPopup(Request $request){

        $url = 'https://pms.tridhyatech.com/api/attendnce/'.$request->month.'/'.$request->year;
     
       $ch = curl_init();
        $payload = '{""}';
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT,true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $AttendenceResult = curl_exec($ch);
        $AttendenceData=json_decode($AttendenceResult);
          // echo "<pre>";print_r($AttendenceData);exit;
        if(!empty($AttendenceData->data)){
        foreach($AttendenceData->data as $index=>$value)
        {
            // echo '<pre>';print_r($request->all());exit;
            $attendence = Attendence::where('emp_code',$value->emp_code)->where('emp_comp',$value->emp_comp)->where('entry_date',$value->entry_date)->where('month',$request->month)->where('year',$request->year)->first();
            if(!empty($attendence)){
                $attendence->entry_date = $value->entry_date;
                $attendence->emp_code = $value->emp_code;
                $attendence->emp_name = $value->emp_name;
                $attendence->emp_comp = $value->emp_comp;
                $attendence->first_in = $value->first_in;
                $attendence->last_out = $value->last_out;
                $attendence->late_comer = $value->late_comer;
                $attendence->full_day = $value->full_day;
                $attendence->early_going = $value->early_going;
                $attendence->absent = $value->absent;
                $attendence->month = $request->month;
                $attendence->year = $request->year;
                $attendence->save();
            }else{
                $attendence = New Attendence();
                $attendence->entry_date = $value->entry_date;
                $attendence->emp_code = $value->emp_code;
                $attendence->emp_name = $value->emp_name;
                $attendence->emp_comp = $value->emp_comp;
                $attendence->first_in = $value->first_in;
                $attendence->last_out = $value->last_out;
                $attendence->late_comer = $value->late_comer;
                $attendence->full_day = $value->full_day;
                $attendence->early_going = $value->early_going;
                $attendence->absent = $value->absent;
                $attendence->month = $request->month;
                $attendence->year = $request->year;
                $attendence->save();
            }
             


             
        }
     }
        curl_close($ch);

        $genralsettingdata = Settings::first();
        if(isset($request->id)){
         //   echo "in";exit;
            $salraysheetdata1=Salarysheet::select()->whereIn('id',$request->id)->where('month',$request->month)->where('year',$request->year)->get();
        }else{
           // echo "out";exit;
            $salraysheetdata1=Salarysheet::select()->where('month',$request->month)->where('year',$request->year)->get();
        }
        // echo '<pre>'; print($request->month);
        // echo '<pre>'; print($request->year);
        // echo '<pre>'; print($request->id);exit;
        if(!$salraysheetdata1->isEmpty()){
            if(isset($request->id)){
                $dataQuery =Salarysheet::with('salaryDetails','getuser')->whereIn('id',$request->id)->where('month',$request->month)->where('year',$request->year);                        
            }else{
                $dataQuery =Salarysheet::with('salaryDetails','getuser')->where('month',$request->month)->where('year',$request->year);                        
            }
            
            $salarysheet = $dataQuery->get();
            foreach ($salarysheet as $key => $value) {
            
            
                if(isset($value->salaryDetails) && !empty($value->salaryDetails)){
                    
                        $getusercompoff = report::where('employee_code',$value->employee_id)->where('month',$request->month)->where('year',$request->year)->first();
                        
                        $getsalarydata =  $this->salaryCalculation($value,$genralsettingdata,$getusercompoff);
                        $value->salaryCalculation = $getsalarydata;
                        // $value->additional = $getsalarydata['additional'];
                        // $value->gratuity = $getsalarydata['gratuity'];
                        // $value->salary_payable = $getsalarydata['salary_payable'];
                        // $value->net_payable = $getsalarydata['net_payable'];
                    }      
            }
            $request->id = 'edit';
            $type = 'edit';
           // echo "in";exit;
        }else{
           // echo "out";exit;

           $type = "add";
          
            $enddate =  date('Y-m-t', strtotime($request->year.'-'.$request->month.'-1'));
          //   echo '<pre>'; print($request);exit;
            $salarysheet = User::select('users.employee_id','users.name','designation_name','pan_no','bank_name','bank_account_no','available_leaves','gratuity','users.joining_date','reports.total_number_of_days','reports.total_working_days','reports.total_worked_days','reports.comp_off','reports.balance_leave','reports.credit_leave_cf','reports.leave_of_month','reports.balance_leave_after_companyoff','attendence.entry_date','users.joining_date','reports.lwp')->leftjoin('salarys','users.employee_id','salarys.employee_id')->leftjoin('reports','reports.employee_code','users.employee_id')->leftJoin('attendence','attendence.emp_code', 'users.employee_id')->where('users.joining_date','<=',$enddate)->whereNotNull('salarys.deleted_at')

                     // echo "<pre>";print_r($salarysheet->get());exit;   
                 // ->where(function($qry) use($request){
                          
             
                   
                   ->whereMonth('attendence.entry_date','=',$request->month)->whereYear('attendence.entry_date','=',$request->year)->get();
                   
                  // })->get();

               // dd(DB::getQueryLog());
           // echo '<pre>'; print($salarysheet);exit;
                }
      
                                  

    
                   
        
        

        
        $returnHTML = view('modal.salary_sheet_popup')->with('salarysheet',$salarysheet)->with('request',$request)->with('id',"test")->render();
        return response()->json(array('success' => true, 'html'=>$returnHTML,'type'=>$type));
     }
     public function salarySheetPopupUpdate(Request $request){
       
         if($request->type == 'edit'){
           
            $salarysheet = Salarysheet::whereIn('salarysheet.id',$request->locationthemes)->get();
            if(!empty($salarysheet)){
                foreach($salarysheet as $index=>$value)
                {
                    $insertsalary = Salarysheet::find($value->id);
                    $additional = "emp_additional_".$value->employee_id;
                    $permenant_deduction = "emp_permenant_deduction_".$value->employee_id;
                    $refund = "emp_refund_".$value->employee_id;
                    $tds = "emp_tds_".$value->employee_id;
                    $other_deduction = "emp_other_deduction_".$value->employee_id;
                    $resignation_hold_release = "emp_resignation_hold_release_".$value->employee_id;
                    $notes = "emp_notes_".$value->employee_id;
                    
                    $insertsalary->add  = $request->$additional;
                    $insertsalary->permenant_deduction  = $request->$permenant_deduction;
                    $insertsalary->refund  = $request->$refund;
                   // $insertsalary->tds  = $request->$tds;
                    $insertsalary->other_deduction  = $request->$other_deduction;
                   // $insertsalary->resignation_hold_release  = $request->$resignation_hold_release;
                    $insertsalary->note  = $request->$notes;
                    $insertsalary->save();
                }
            }
         }else{
           // dd($request->all());exit;
             $enddate =  date('Y-m-t', strtotime($request->ReportYearPopup.'-'.$request->ReportMonthPopup.'-1'));
            $salarysheet = User::select('users.employee_id','users.name','designation_name','pan_no','bank_name','bank_account_no','available_leaves','gratuity','users.joining_date','reports.total_number_of_days','reports.total_working_days','reports.total_worked_days','reports.comp_off','reports.balance_leave','reports.credit_leave_cf','reports.leave_of_month','reports.balance_leave_after_companyoff','reports.lwp')->leftjoin('salarys','users.employee_id','salarys.employee_id')->leftjoin('reports','reports.employee_code','users.employee_id')->leftJoin('attendence','attendence.emp_code', 'users.employee_id')->where('users.joining_date','<=',$enddate)
                   ->whereMonth('attendence.entry_date','=',$request->ReportMonthPopup)->whereYear('attendence.entry_date','=',$request->ReportYearPopup)->get();
                   
              
                   
                          
             
         //     echo "<pre>";print_r($salarysheet);exit;     
                
               
            if(!empty($salarysheet)){
                foreach($salarysheet as $index=>$value)
                {
                    // echo '<pre>'; print_r($value->designation_name);exit;
                    $insertsalary = New Salarysheet();
                    $insertsalary->employee_id = $value->employee_id;
                    $insertsalary->name = $value->name;
                    $insertsalary->designation = $value->designation_name;
                    $insertsalary->joining_date = $value->joining_date;
                    $insertsalary->pan_number = $value->pan_no;
                    $insertsalary->bank_name = $value->bank_name;
                    $insertsalary->account_number = $value->bank_account_no;
                    $insertsalary->leaves = $value->available_leaves;
                    $insertsalary->month_days = $value->total_number_of_days;
                    $insertsalary->month_working_days = $value->total_working_days;
                    $insertsalary->working_days = ($value->total_worked_days + $value->leave_of_month);
                    $insertsalary->present_days = $value->total_worked_days;
                    $insertsalary->total_days = $value->total_working_days;
                    $insertsalary->extra_leave  = $value->comp_off;
                    $insertsalary->lwp  = $value->lwp;
                    $insertsalary->credit_leave_cf  = $value->credit_leave_cf;
                    $additional = "emp_additional_".$value->employee_id;
                    $permenant_deduction = "emp_permenant_deduction_".$value->employee_id;
                    $refund = "emp_refund_".$value->employee_id;
                    $tds = "emp_tds_".$value->employee_id;
                    $other_deduction = "emp_other_deduction_".$value->employee_id;
                    $resignation_hold_release = "emp_resignation_hold_release_".$value->employee_id;
                    $notes = "emp_notes_".$value->employee_id;
                    
                    $insertsalary->additional  = $request->$additional;
                    $insertsalary->permenant_deduction  = $request->$permenant_deduction;
                    $insertsalary->refund  = $request->$refund;
                    $insertsalary->tds  = $request->$tds;
                    $insertsalary->other_deduction  = $request->$other_deduction;
                    $insertsalary->resignation_hold_release  = $request->$resignation_hold_release;
                    $insertsalary->note  = $request->$notes;

                    $insertsalary->leave_of_month  = $value->leave_of_month;
                    $insertsalary->balance_leave  = $value->balance_leave;
                    $insertsalary->balance_leave_after_companyoff  = $value->balance_leave_after_companyoff;

                
                    $insertsalary->gratuity = 0;
                    $insertsalary->month = $request->ReportMonthPopup;
                    $insertsalary->year = $request->ReportYearPopup;
                    $insertsalary->save();
                
                }
            }
         }
        
        $redirect = "/salarysheet/".$request->ReportMonthPopup."/".$request->ReportYearPopup;
 return redirect($redirect);
     }
     public function salarysheetDelete(Request $request){
        $data = Salarysheet_attechment::find($request->id);
        $month = $data->month;
        $year = $data->year;
        $data->delete();
        $details = Salarysheet::where('month',$month)->where('year',$year)->delete();
        return true;
     }
     public function importTds(Request $request){
        // dd($request->hasFile('import_sheet'));
        // echo "<pre>";print_r($request->file('import_sheet'));exit;
        $file = $request->file('import_sheet');
        $extension = $file->getClientOriginalExtension();
        $filename = 'attendance_' . time() . '.' . $extension;
        if ($request) {
            Storage::disk('local')->putFileAs('', $file, $filename);
        }
        Excel::import(new TdsUpload($request), $filename); 
        $redirect = "/salarysheet/".$request->ReportMonth."/".$request->ReportYear;
         return redirect($redirect);
     }
}




