<?php

namespace App\Imports;
use App\Models\User;
use App\Models\Salary;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Illuminate\Support\Facades\Session;
use App\Mail\SalaryslipMail;
use App\Models\Settings;
use App\Models\Salarysheet;
use App\Models\SlipGenratedMonth;
use Carbon\Carbon;
use MPDF;
use Illuminate\Support\Facades\Mail;

class ExportSalarySlip implements ToCollection, WithStartRow
{
    public function startRow(): int {return 1; }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function collection(Collection $rows)
    {
        $success = $inValidRecord = 0;
        $data = array();
       // echo "<pre>";print_r($rows);exit;
        foreach ($rows  as $key => $row) {
            if($key > 0 && $row[0] != ''){
                $row[0] = str_replace('TRD','',$row[0]);
                $getuserdata = User::select('users.provi_fund','users.bonus_applicable','reports.lwp','users.employee_status','users.email','users.personal_email','users.department','users.employee_id','users.name','designation_name','pan_no','bank_name','bank_account_no','available_leaves','gratuity','users.joining_date','reports.total_number_of_days','reports.total_working_days','reports.total_worked_days','reports.comp_off','reports.balance_leave','reports.credit_leave_cf','reports.leave_of_month','reports.balance_leave_after_companyoff','users.joining_date','users.status','users.ifsc_code')->where('employee_id',$row[0])->leftjoin('reports','reports.employee_code','users.employee_id')->where('reports.month',$row[10])->where('reports.year',$row[11])->first();
               // echo "<pre>";print_r($getuserdata);exit;
                if(!empty($getuserdata)){
                    $flag = 1;
                    $insertsalary = Salarysheet::where('employee_id',$row[0])->where('month',$row[10])->where('year',$row[11])->first();
                    if(empty($insertsalary)){
                        $insertsalary = New Salarysheet();
                    }else{
                        if($insertsalary->slip_genrated == 1){
                            $flag = 0;
                        }
                    }

                    $insertsalary->employee_id = $getuserdata->employee_id;
                    $insertsalary->name = $getuserdata->name;
                    $insertsalary->designation = $getuserdata->designation_name;
                    $insertsalary->joining_date = $getuserdata->joining_date;
                    $insertsalary->pan_number = $getuserdata->pan_no;
                    $insertsalary->bank_name = $getuserdata->bank_name;
                    $insertsalary->account_number = $getuserdata->bank_account_no;
                    $insertsalary->leaves = $getuserdata->available_leaves;
                    $insertsalary->month_days = $getuserdata->total_number_of_days;
                    $insertsalary->month_working_days = $getuserdata->total_working_days;
                    $insertsalary->working_days = ($getuserdata->total_worked_days + $getuserdata->leave_of_month);
                    $insertsalary->present_days = $getuserdata->total_worked_days;
                    $insertsalary->total_days = $getuserdata->total_working_days;
                    $insertsalary->extra_leave  = $getuserdata->comp_off;
                    $insertsalary->credit_leave_cf  = $getuserdata->credit_leave_cf;
                    $insertsalary->leave_of_month  = $getuserdata->leave_of_month;
                    $insertsalary->balance_leave  = $getuserdata->balance_leave;
                    $insertsalary->balance_leave_after_companyoff  = $getuserdata->balance_leave_after_companyoff;
                    $insertsalary->gratuity = 0;
                    $insertsalary->month = $row[10];
                    $insertsalary->slip_genrated = 1;
                    $insertsalary->slip_genrated_date = Carbon::today();
                    $insertsalary->year = $row[11];
                    $insertsalary->lwp = $getuserdata->lwp;
                    $insertsalary->save();

                     $arr['from_email'] = config('constant.from_address');
                     $arr['from_name']  = config('constant.company_name');
                      if(!empty(trim($row[0])) || $row[0] === 0){
                        $arr['emp_code']  = $row[0] ;
                      }else{
                        $arr['emp_code']  = $getuserdata->employee_id ;
                      }
                     $arr['month_year'] = date('F', mktime(0, 0, 0, $row[10], 10)) .' - '. $row[11];

                     if(!empty(trim($row[1])) || $row[1] === 0){
                        $arr['emp_name'] = $row[1];
                     }else{
                        $arr['emp_name'] = (!empty($getuserdata))?$getuserdata->name:'';
                     }
                     $arr['emp_email'] = (!empty($getuserdata))?$getuserdata->email:'';
                     $arr['personal_email'] = (!empty($getuserdata))?$getuserdata->personal_email:'';
                     $arr['department'] = (!empty($getuserdata))?$getuserdata->department:'';
                     if(!empty(trim($row[2])) || $row[2] === 0){
                        $arr['designation'] = $row[2];
                     }else{
                        $arr['designation'] = (!empty($getuserdata))?$getuserdata->designation_name:'';
                     }
                     if(!empty(trim($row[6])) || $row[6] === 0){
                        $arr['bank_name'] = $row[6];
                     }else{
                        $arr['bank_name'] = (!empty($getuserdata))?$getuserdata->bank_name:'';
                     }
                     if(!empty(trim($row[8])) || $row[8] === 0){
                        $arr['bank_account_no'] = $row[8];
                     }else{
                        $arr['bank_account_no'] =(!empty($getuserdata))?$getuserdata->bank_account_no:'';
                     }
                     if(!empty(trim($row[4])) || $row[4] === 0){
                        $arr['location'] = $row[4];
                     }else{
                        $arr['location'] = 'Ahmedabad';
                     }
                      if(!empty(trim($row[5])) || $row[5] === 0){
                        $arr['joining_date'] = $row[5];
                      }else{
                         $arr['joining_date'] = (!empty($getuserdata))?$getuserdata->joining_date:'';
                      }
                     if(!empty(trim($row[3])) || $row[3] === 0){
                          $arr['pan_no'] = $row[3];
                     }else{
                         $arr['pan_no'] = (!empty($getuserdata))?$getuserdata->pan_no:'';
                     }
                     if(!empty(trim($row[7])) || $row[7] === 0){
                        $arr['ifsc_code'] = $row[7];
                     }else{
                        $arr['ifsc_code'] = $getuserdata->ifsc_code;
                     }
                     $arr['status'] = (!empty($getuserdata))?$getuserdata->employee_status:''; 
                     if(!empty(trim($row[9])) || $row[9] === 0){
                        $arr['uan_no'] = $row[9];
                     }else{
                        $arr['uan_no'] = 0; 
                     }
                      if(!empty(trim($row[12])) || $row[12] === 0){
                        $arr['leave_balance'] = $row[12];
                      }else{
                         $arr['leave_balance'] = $getuserdata->balance_leave;
                      }
                      if(!empty(trim($row[13])) || $row[13] === 0){
                        $arr['leave_taken'] = $row[13];
                      }else{
                        $arr['leave_taken'] = $getuserdata->leave_of_month;
                      }
                     $arr['working_days'] = $row[18];

                     $arr['month_days'] = $row[17];
                     $arr['present_days'] = $row[19];

                    if(!empty(trim($row[14])) || $row[14] === 0){
                        $arr['el_encash'] = $row[14];
                    }else{
                        $arr['el_encash'] = 0;
                    }

                     if(!empty(trim($row[15])) || $row[15] === 0){
                         $arr['balance_leave_after_this_month'] =  $row[15];
                     }else{
                            if(($getuserdata->balance_leave - $getuserdata->leave_of_month) >= 0){
                                $arr['balance_leave_after_this_month'] = $getuserdata->balance_leave - $getuserdata->leave_of_month;
                            }else{
                                $arr['balance_leave_after_this_month'] =  0;
                            }
                    }
                    $leaveCountLwp = $getuserdata->leave_of_month - $getuserdata->credit_leave_cf;
                      if($leaveCountLwp > 0){
                        $leaveWithoutPayNumber = (!empty($getuserdata->lwp))?$getuserdata->lwp:0;
                            $leaveWithoutPayNo = $leaveWithoutPayNumber + $leaveCountLwp;
                      }else{
                         $leaveWithoutPayNo = (!empty($getuserdata->lwp))?$getuserdata->lwp:0;
                      }
                    $arr['leave_without_pay_no'] = $row[20];
                    // $elEncashNo = (!empty($getuserdata->incase_comp_off))?$getuserdata->incase_comp_off:'';
                    // if(!empty($elEncashNo)){
                    //     $arr['el_encash_no'] = $elEncashNo;
                    //  }else{
                    //     $arr['el_encash_no'] = 0;
                    //  }
                    
                     $arr['el_encash_no'] = $row[16];
                     $arr['CTC_Main']= $row[21];
                     $arr['salary_payable'] = $row[22];
                   // if($arr['salary_payable'] <= 21000){
                   //      $arr['basic_salary'] = round(($arr['salary_payable'] * 60)/100);
                   //   }else{
                   //      $arr['basic_salary'] = round(($arr['salary_payable'] * 50)/100);
                   //   }
                     if(!empty(trim($row[23])) || $row[23] === 0){
                        $arr['basic_salary'] = $row[23];
                     }else{
                        $arr['basic_salary'] = round(($arr['salary_payable'] * 50)/100);
                     }
                     if(!empty(trim($row[24])) || $row[24] === 0){
                        $arr['house_rent_allowance'] = $row[24];
                     }else{
                        $arr['house_rent_allowance'] = round(($arr['salary_payable'] * 15)/100);
                     }
                     if(!empty(trim($row[25])) || $row[25] === 0){
                        $arr['privileged_leave'] =  $row[25];
                     }else{
                       $arr['privileged_leave'] = round(($arr['basic_salary'] * 4.81)/100);
                     }
                    
                        $bonus_applicable = (!empty($getuserdata))?$getuserdata->bonus_applicable:'';
                     if(!empty(trim($row[26])) || $row[26] === 0){
                        $arr['bonus'] =  $row[26];
                     }else{
                        if($bonus_applicable == 1){
                            $arr['bonus'] = round(($arr['CTC_Main'] / 13));
                        }else{
                             if($arr['basic_salary'] <= 21000){
                                $arr['bonus'] = round(($arr['basic_salary'] * 20)/100);
                             }else{
                                $arr['bonus'] = 0;
                             }
                        }
                       
                     }
                     if(!empty(trim($row[27])) || $row[27] === 0){
                        $arr['gratuity'] = $row[27];
                     }else{
                        if($getuserdata->gratuity == 1){
                            $arr['gratuity'] = round(($arr['salary_payable'] * 1.92)/100);
                        }else{
                            $arr['gratuity'] = 0;
                         }
                     }
                   
                    if(!empty(trim($row[28])) || $row[28] === 0){
                        $arr['pf_employer'] = $row[28];
                    }else{
                        if($getuserdata->provi_fund == 1){
                            if(($arr['salary_payable'] - $arr['house_rent_allowance']) < 15000){
                                $arr['pf_employer'] = round((($arr['salary_payable'] - $arr['house_rent_allowance']) * 12)/100);
                             }else{
                                $arr['pf_employer'] = round((15000 * 12)/100);
                             }
                         }else{
                            $arr['pf_employer'] = 0;
                         }
                    } 
                    if(!empty(trim($row[29])) || $row[29] === 0){
                        $arr['esic_employer'] = $row[29];
                    }else{
                        if(($arr['CTC_Main'] - $arr['gratuity'] - $arr['pf_employer'] ) < 21000){
                            $arr['esic_employer'] = round((($arr['CTC_Main'] - $arr['pf_employer'])*3.25)/100);
                         }else{
                            $arr['esic_employer'] = 0;
                         }
                    }
                   //  $arr['pf_employer'] = 0;
                    if(!empty(trim($row[30])) || $row[30] === 0){
                        $arr['special_allowance'] = $row[30];
                    }else{
                        $arr['special_allowance'] = ($arr['salary_payable'] - $arr['basic_salary'] - $arr['house_rent_allowance'] - $arr['privileged_leave'] - $arr['bonus'] - $arr['gratuity'] - $arr['pf_employer'] - $arr['esic_employer']);
                    }

                     $arr['other_allowance'] = $row[31] ;
                     $arr['comp_off_enchashment'] = $row[32];
                     $arr['pl_leave_encachement'] = $row[33];
                    if(!empty(trim($row[34])) || $row[34] === 0){
                        $arr['total_earnings'] = $row[34];
                    }else{
                        $arr['total_earnings'] = $arr['basic_salary'] + $arr['house_rent_allowance'] + $arr['privileged_leave'] + $arr['bonus'] + $arr['gratuity'] + $arr['pf_employer'] + $arr['esic_employer'] + $arr['special_allowance'] + $arr['other_allowance'] + $arr['comp_off_enchashment'] + $arr['pl_leave_encachement']; 
                    }
                      
                    if(!empty(trim($row[35])) || $row[35] === 0){
                        $arr['professional_tax'] = $row[35];
                    }else{
                         if($arr['CTC_Main'] <= 12000){
                             $arr['professional_tax'] = 0;
                         }else{
                            $arr['professional_tax'] = 200;
                         }
                    }
                    if(!empty(trim($row[36])) || $row[36] === 0){
                       // echo "in";exit;
                        $arr['pf_employee_side'] = $row[36];
                    }else{
                        //echo "out";exit;
                        if($getuserdata->provi_fund == 1){
                            $arr['pf_employee_side'] = $arr['pf_employer'];
                         }else{
                            $arr['pf_employee_side'] = 0;
                         }
                    }
                    if(!empty(trim($row[37])) || $row[37] === 0){
                        $arr['pf_employer_side_deductions'] = $row[37];
                    }else{
                        $arr['pf_employer_side_deductions'] = $arr['pf_employer'];
                    }
                    if(!empty(trim($row[38])) || $row[38] === 0){
                        $arr['esic'] = $row[38];
                    }else{
                        if(($arr['CTC_Main'] - $arr['gratuity'] - $arr['pf_employer'] ) < 21000){
                            $arr['esic'] = round((($arr['CTC_Main'] - $arr['gratuity'] - $arr['pf_employer']) * 0.75)/100);
                         }else{
                            $arr['esic'] = 0;
                         }
                    }
                    // $arr['pf_employee_side'] = 0;
                    
                     $arr['other_deduction'] = $row[42];
                     // if($getuserdata->gratuity == 1){
                     //    if(!empty($row[11])){
                     //        $arr['gratuity_deduction'] = $row[11];
                     //    }else{
                            
                     //    }
                        
                     // }else{
                     //    $arr['gratuity_deduction'] = 0;
                     // }
                    if(!empty(trim($row[39])) || $row[39] === 0){
                        $arr['gratuity_deduction'] = $row[39];
                    }else{
                         $arr['gratuity_deduction'] = $arr['gratuity'];
                    }
                    
                     //$arr['gratuity_deduction'] = $row[11];
                    if(!empty(trim($row[40])) || $row[40] === 0){
                        $arr['bonus_deduction'] = $row[40];
                    }else{
                         if($bonus_applicable == 1){
                            $arr['bonus_deduction'] = $arr['bonus'];
                         }else{
                            $arr['bonus_deduction'] = 0;
                         }
                    }
                     
                     $arr['tds'] = $row[41];
                     $arr['leave_with_out_pay'] = ($arr['CTC_Main'] - $arr['salary_payable']);
                    if(!empty(trim($row[43])) || $row[43] === 0){
                        $arr['total_deductions'] = $row[43];
                    }else{
                        $arr['total_deductions'] = $arr['professional_tax'] + $arr['pf_employee_side'] + $arr['esic'] + $arr['gratuity_deduction'] + $arr['tds'] + $arr['other_deduction'] + $arr['pf_employer_side_deductions'] + $arr['bonus_deduction'];
                    }
                     if(!empty(trim($row[44])) || $row[44] === 0){
                        $arr['net_amount'] = $row[44];
                    }else{
                        $arr['net_amount'] = $arr['total_earnings'] - $arr['total_deductions'];
                    } 
                      
                      $pdf = MPDF::loadView('salaryslip.salaryPDF', $arr);
                                        $pdf->output();
                        //dd($pdf);
                     $genralsettingdata = Settings::first(); 
                     if(trim($row[45]) === 1){
                            if(!empty($genralsettingdata->mail_to_cc)){
                                $ccArray = explode(",",$genralsettingdata->mail_to_cc);
                                Mail::send(new SalaryslipMail($pdf,$arr,$ccArray));
                            }else{
                                  Mail::send(new SalaryslipMail($pdf,$arr));
                            }
                        
                     }else{
                         if($flag == 1){
                                if(!empty($genralsettingdata->mail_to_cc)){
                                    $ccArray = explode(",",$genralsettingdata->mail_to_cc);
                                    Mail::send(new SalaryslipMail($pdf,$arr,$ccArray));
                                }else{
                                      Mail::send(new SalaryslipMail($pdf,$arr));
                                }
                         }
                     } 
                     
                       
                           // Mail::send(new SalaryslipMail($pdf,$arr));
                    $getgenratedmonth = SlipGenratedMonth::where('month',$row[10])->where('year',$row[11])->first();
                    if(empty($getgenratedmonth))
                    {
                        $SlipGenratedMonth = new SlipGenratedMonth();
                        $SlipGenratedMonth->month = $row[10];
                        $SlipGenratedMonth->year= $row[11];
                        $SlipGenratedMonth->save(); 
                    }  
                     $success++;
                   
                }else{
                    $inValidRecord++;
                }
                
            }
        }
        $data['success']=$success;
        $data['skipped']= $inValidRecord;
        Session::push('UserSalaryImportError',$data);
    }
}
