<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-light-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{url('/home')}}" class="brand-link">
        <img src="{{url('/img/logo.png')}}" alt="PMS" class="brand-image img-circle elevation-3"
       style="opacity: .8">
    <span class="brand-text font-weight-light">PMS</span>
    </a>
    
    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
           
        <nav class="mt-2 user-panel">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item has-treeview {{ Request::is('viewprofile')|| Request::is('changepassword')|| Request::is('logout')? ' menu-open' : '' }}">
                    <a  href="javascript:void(0);" class="align-items-center line-h-0 nav-link {{ Request::is('viewprofile')|| Request::is('changepassword')|| Request::is('logout')? ' active' : '' }}">
                        <i class="username-panel" aria-hidden="true"><img src="{{url('/img/profile.png')}}" class="img-circle elevation-2" alt="User Image"></i>
                        <p>
                          <span>
                          {{auth()->user()->first_name!=null ? auth()->user()->first_name ." ".auth()->user()->last_name  : "Administrator"}}
                          </span>
                           <i class="right fa fa-angle-left"></i></p>
                    </a>
                    <ul class="nav nav-treeview" style="display:none">
                         {{-- <li class="nav-item has-treeview ">
                            <a href="{{url('/viewprofile')}}" class="nav-link {{ Request::is('viewprofile') ? 'active' : '' }}">
                                <i class="fas fa-user-circle"></i>
                                <p>View Profile</p>
                            </a>
                        </li>--}}
                        <li class="nav-item has-treeview ">
                            <a href="{{url('changepassword')}}" class="nav-link {{ Request::is('changepassword') ? 'active' : '' }}">
                                <i class="fas fa-key"></i>
                                <p>Change Password</p>
                            </a>
                        </li> 
                        <li class="nav-item has-treeview">
                            <a href="{{url('/logout')}}" class="nav-link {{ Request::is('logout') ? 'active' : '' }}">
                                <i class="fas fa-sign-out-alt"></i>
                                <p>Logout</p>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item has-treeview  {{ Request::is('users') ||Request::is('designation') ? 'menu-open' : '' }}">
                    <a href="javascript:void(0);" class="nav-link {{ Request::is('users') ||Request::is('designation')? 'active' : '' }}">
                      <i class="fa fa-users"></i>
                      <p>
                        User Management
                        <i class="right fa fa-angle-left"></i>
                      </p>
                    </a>
                    <ul class="nav nav-treeview" style="display:none">
                      <li class="nav-item">
                        <a href="{{url('/home')}}" class="nav-link  {{ Request::is('/home') ? 'active' : '' }}">
                          <i class="fas fa-user"></i>
                          <p>Users</p>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a href="{{url('/home/userfield')}}" class="nav-link {{ Request::is('/home/userfield') ? 'active' : '' }}">
                          <i class="mdi mdi-note-outline"></i>
                          <p>User Filed</p>
                        </a>
                      </li>
                    </ul>
                </li>

                 <li class="nav-item has-treeview {{Request::is('/report*') ? 'menu-open' : '' }}">
                        <a href="javascript:void(0);" class="nav-link {{ Request::is('/report*') ? 'active' : '' }}">
                            <i class="fas fa-tachometer-alt"></i>
                            <p>
                              Report<i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview" style="display:none">
                          <li class="nav-item">
                              <a href="{{url('/report')}}" class="nav-link {{ Request::is('/report') ? 'active' : '' }}">
                                  <i class="mdi mdi-folder-account mdi-18px"></i>
                                  <p>Leave Report</p>
                              </a>
                          </li>
                          <li class="nav-item">
                            <a href="{{url('/home/reportfield')}}" class="nav-link {{ Request::is('/home/reportfield') ? 'active' : '' }}">
                              <i class="mdi mdi-note-outline"></i>
                              <p>Report Filed</p>
                            </a>
                          </li>
                      </ul>
                    </li>
                    <li class="nav-item has-treeview {{Request::is('/salary*') ? 'menu-open' : '' }}">
                      <a href="javascript:void(0);" class="nav-link {{ Request::is('/salary*') ? 'active' : '' }}">
                          <i class="mdi mdi-receipt"></i>
                          <p>
                            Salary<i class="right fa fa-angle-left"></i>
                          </p>
                      </a>
                      <ul class="nav nav-treeview" style="display:none">
                        <li class="nav-item">
                            <a href="{{url('/salary')}}" class="nav-link {{ Request::is('/salary') ? 'active' : '' }}">
                                <i class="mdi mdi-folder-account mdi-18px"></i>
                                <p>User Salary</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('/salarysheet')}}" class="nav-link {{ Request::is('/salary') ? 'active' : '' }}">
                                <i class="mdi mdi-folder-account mdi-18px"></i>
                                <p>Salary Sheet</p>
                            </a>
                        </li>
                           <li class="nav-item">
                            <a href="{{url('/salaryslip')}}" class="nav-link {{ Request::is('/salary') ? 'active' : '' }}">
                                <i class="mdi mdi-folder-account mdi-18px"></i>
                                <p>Salary Slip</p> 
                            </a>
                        </li>
                       
                    </ul>
                  </li>
                    <li class="nav-item has-treeview {{Request::is('/settings*') ? 'menu-open' : '' }}">
                      <a href="{{url('/settings')}}" class="nav-link {{ Request::is('/settings*') ? 'active' : '' }}">
                          <i class="fas fa-cogs"></i>
                          <p>
                            General Settings  
                          </p>
                      </a>
                   
                  </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>

