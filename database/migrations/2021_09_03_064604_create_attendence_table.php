<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttendenceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attendence', function (Blueprint $table) {
             $table->id();
             $table->date('entry_date')->nullable();
            $table->text('emp_code')->nullable();
            $table->text('emp_name')->nullable();
            $table->text('emp_comp')->nullable();
            $table->time('first_in')->nullable();
            $table->time('last_out')->nullable();
            $table->tinyInteger('late_comer')->nullable();
            $table->tinyInteger('full_day')->nullable();
            $table->tinyInteger('early_going')->nullable();
            $table->tinyInteger('absent')->nullable();
             $table->integer('month')->nullable();
              $table->integer('year')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attendence');
    }
}
