<style>
    .modal_body_salary_sheet_popup thead th {
        position: sticky;
        top: -17px;
        z-index: 11;
        background: #f2f2f2;
    }
    .disableTextBold{
        pointer-events:none !important;
        font-weight: 900 !important;
    }
</style>
<div class="table-responsive20">
<table class="table table-striped table-bordered">
    <thead>
        <tr>
          {{-- <th>Id</th> --}}
          <?php if(isset($request->id)){
              echo "<th></th>";
          }?>
          <th>Emp id</th>
          <th>Emp name</th>
          <th>Additional</th>
          <th>Permenant Deduction</th>
          <th>Refund</th>
          <th>TDS</th> 
          <th>Other Deduction</th>
          <!-- <th>Resignation Hold Release</th> -->
          <th>Notes</th>
        </tr>
        
      </thead>
      <tbody>
                    
        @if($salarysheet->count()>0)
           @foreach($salarysheet as $s)
           <tr>
            <?php if(isset($request->id)){ ?>
                <td><input type="checkbox" class="disableTextBold" name="locationthemes[]" value="{{$s->id}}" checked></td>
            <?php }?>
               <td><input type="text" class="form-control disableTextBold" name="emp_id_{{ $s->employee_id }}" value="TRD{{ $s->employee_id }}"></td>
               <td><input type="text" class="form-control disableTextBold" name="emp_name_{{ $s->employee_id }}" value="{{ $s->name }}"></td>
               <td><input type="text" class="form-control" name="emp_additional_{{ $s->employee_id }}" value="{{(!empty($s->additional))?$s->additional:0}}"></td>
               <td><input type="text" class="form-control" name="emp_permenant_deduction_{{ $s->employee_id }}" value="{{(!empty($s->permenant_deduction))?$s->permenant_deduction:0}}"></td>
               <td><input type="text" class="form-control" name="emp_refund_{{ $s->employee_id }}" value="{{(!empty($s->refund))?$s->refund:0}}"></td>
               <td><input type="text" class="form-control" name="emp_tds_{{ $s->employee_id }}" value="{{(!empty($s->tds))?$s->tds:0}}"></td>
               <td><input type="text" class="form-control" name="emp_other_deduction_{{ $s->employee_id }}" value="{{(!empty($s->other_deduction))?$s->other_deduction:0}}"></td>
              <!--  <td><input type="text" class="form-control" name="emp_resignation_hold_release_{{ $s->employee_id }}" value="{{ $s->resignation_hold_release }}"></td> -->
               <td><input type="text" class="form-control" name="emp_notes_{{ $s->employee_id }}" value="{{ $s->note }}"></td>
           </tr>
           @endforeach
       @endif
      </tbody>
    </table>
</div>


              <!--    <input type="text" name=""> -->