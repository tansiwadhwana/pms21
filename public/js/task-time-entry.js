$(document).ready(function () {
    $('.select2').select2();

    $("#search_task_start_date,#search_task_end_date").datepicker({autoclose: true, format: 'dd-mm-yyyy'});

    $(document).on("click", ".timeEntry", function () {
        $('#saveTaskEntry').trigger("reset");
        var projectId = $(this).attr('data-task-id');
        getProjectTaskList(projectId);
        var ProjeTaskId = $(this).attr('data-id');
        var fullDate = new Date();
        var twoDigitMonth = ((fullDate.getMonth().length + 1) === 1) ? (fullDate.getMonth() + 1) : ((fullDate.getMonth() + 1) < 10 ? '0' + (fullDate.getMonth() + 1) : (fullDate.getMonth() + 1));//'0' + (fullDate.getMonth() + 1);
        var todayDate = fullDate.getFullYear() + "-" + twoDigitMonth + "-" + fullDate.getDate();
        jQuery.ajax({
            url  : getsiteurl() + '/check/time_entry',
            type : "GET",
            success: function(res) {
                jQuery.ajax({
                    url  : getsiteurl() + '/get/project/' + projectId,
                    type : "GET",
                    success: function(response) {
                        $('#taskDate').datepicker('destroy');
                        if(res == 0){
                            startDate = response.project_start_date; 
                        }else{
                            startDate = res;
                        }
                        $("#taskDate").datepicker({
                            todayBtn: 1,
                            autoclose: true,
                            format: 'dd-mm-yyyy',
                            startDate: new Date(startDate.replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")),
                            //endDate: new Date(todayDate.replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"))
                            endDate: new Date()
                        });
                        $("#taskDate").on("change", function () {
                            $('#task_list').select2("val", 0);
                            getProjectTaskList(projectId);
                        });
                        jQuery.ajax({
                            url: getsiteurl() + '/remainingProjectEntry',
                            type: "GET",
                            success: function (response) {
                                $('#workTimeHours option[value=' + response['hours'] + ']').attr('selected', 'selected');
                                $('#workTimeMinutes option[value=' + response['min'] + ']').attr('selected', 'selected');
                                $('#workTimeHours').trigger('change');
                                $('#workTimeMinutes').trigger('change');
                            }
                        });
                        $('#projectId').val(projectId);

                        $('#taskTimeEntryModal').modal();
                        if (ProjeTaskId != '') {
                            setTimeout(function(){
                                if($('#task_list').text() != "")
                                {
                                    $('#task_list').select2("val",ProjeTaskId);//.trigger('change');
                                }
                                else{
                                    $('#task_list').html('<option value="0">Select Task</option>');
                                }
                            }, 250);
                        }
                    }
                });
            }
        });
    });
    $(document).on("click", ".deleteProjectTask", function(){
        var projectTaskId = $(this).attr('data-task-id');

        if(projectTaskId != ''){
            swal({
                title: "Are you sure you want to delete project task ?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
              }).then((willDelete) => {
                if (willDelete) {
                  deleteProjectTask(projectTaskId);
                }
              });
        }
    });
    function deleteProjectTask(projectTaskId){
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            jQuery.ajax({
            url  : getsiteurl() + '/project_task/delete',
            type : "DELETE",
            data: {_token: CSRF_TOKEN,projectTaskId:projectTaskId},
            success: function(response) {
                if(response.status == 'success'){
                    swal(response.message,"", "success");
                    window.location.reload();
                }else if(response.status == 'error'){
                    swal(response.message,"", "error");
                    if(response.code == 'userNotValid')
                    {
                        location.href = getsiteurl();
                    }
                }
            }
        });
    }
});
function  getProjectTaskList(projectId) {
    jQuery.ajax({
        url: getsiteurl() + '/get/project/task_list/' + projectId + '/' + $('#taskDate').val(),
        type: 'GET',
        success: function (data) {
            var html = '';
            $('#task_list').html('');
            html += '<option value="0">Select Task</option>';
            if (Object.keys(data).length > 0) {
                jQuery.each(data, function (key, value) {
                    html += '<option value="' + value.id + '">' + value.task_name + '</option>';
                });
                $('#task_list').html(html);
                $('#task_list').change(function () {
                    if ($('#task_list').val() != 0) {
                        $('#task_list-error').text('');
                    }
                });
            }

        }
    });
}