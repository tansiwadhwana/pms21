/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


    $(function(){
        $('#dob').datepicker({
            endDate: "today",
            autoclose: true,
            todayHighlight: true,
            format: 'yyyy-mm-dd'
        });
    });
    $(document).ready(function(){
        $('.edit_form').hover().css('cursor','context-menu');
        $('.edit_form').click(function(){
            $(this).prev().css('display','block');
            $(this).css('padding-top','12px');
            $(this).parent().addClass('edit_td');
            $(this).prev().prev().css('display','none');
            $('#bottomButton').removeAttr('disabled');
        });
    });
    function submitForm(){
        $("#submitForm").trigger('click');
    }
    