<?php

namespace App\Imports;
use App\Models\User;
use App\Models\Salary;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Illuminate\Support\Facades\Session;

class UsersSalaryImport implements ToCollection, WithStartRow
{
    public function startRow(): int {return 1; }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function collection(Collection $rows)
    {
        $success = $inValidRecord = 0;
        $data = array();
        
        foreach ($rows  as $key => $row) {
            if($key > 0 && $row[0] != ''){
                
                $userDetails = User::where('employee_id',$row[0])->first();
                if(!empty($userDetails)){
                    $dataExits = Salary::where('employee_id',$row[0])->first();
                    if(!empty($dataExits)){
                        $dataExits->salary_amount = $row[1];
                        $dataExits->save();
                        $success++;
                    }else{
                        $Salary  = new Salary();
                        $Salary->name = $userDetails->name;
                        $Salary->employee_id = $row[0];
                        $Salary->confirmation_date = $userDetails->confirmation_date;
                        $Salary->joining_date = $userDetails->joining_date;
                        $Salary->salary_amount = $row[1];
                        $Salary->save();
                        $success++;
                    }
                   
                }else{
                    $inValidRecord++;
                }
                
            }
        }
        $data['success']=$success;
        $data['skipped']= $inValidRecord;
        Session::push('UserSalaryImportError',$data);
    }
}
