$('.select2').select2();

// $('#page_range_dropdown').change(function(){
//     var limit = $(this).val();
//     $('#page_range').val(limit);
//     $('#search_filter').submit();
// });

$('#page_range_dropdown').change(function(){
    var limit = $(this).val();
    var link  = window.location.pathname;
    var query_string = window.location.search;

    var search_str = (query_string.indexOf('?') == 0 ? query_string.substring(1) : query_string);
    if(limit != '' && query_string.indexOf('search_submit') != -1){
        if(query_string.indexOf('page_range')==1){
            var page_range = query_string.slice(0,query_string.indexOf('='));
            var srch_str = query_string.slice(query_string.indexOf('&'));
            var redirectLink = link +page_range+'='+limit+srch_str;
        }else{
            var redirectLink = link +'?page_range='+limit + '&'+search_str;
        }
    }else{
        var redirectLink = link +'?page_range='+limit;
    }
   window.location = redirectLink;

});

$("#search_by_created_date").datepicker({
    todayBtn: 1,
    autoclose: true,
    format: 'yyyy-mm-dd',
})

$("#search_by_start_date").datepicker({
    todayBtn: 1,
    autoclose: true,
    format: 'dd-mm-yyyy',
}).on('changeDate', function (selected) {
    var minDate = new Date(selected.date.valueOf());
    $('#search_by_end_date').datepicker('setStartDate', minDate);
});

$("#search_by_end_date").datepicker({
    autoclose: true,
    todayBtn: 1,
    format: 'dd-mm-yyyy',
}).on('changeDate', function (selected) {
    var maxDate = new Date(selected.date.valueOf());
    $('#search_by_start_date').datepicker('setEndDate', maxDate);
});

$("#reset_startdate").click(function () {
    $('#search_by_start_date').val('');
});

$("#reset_enddate").click(function () {
    $('#search_by_end_date').val('');
});

$("#reset_createddate").click(function () {
    $('#search_by_created_date').val('');
});
