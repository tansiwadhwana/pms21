<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Salary;

class Salarysheet extends Model
{
    use HasFactory;
     protected $table="salarysheet";

      public function salaryDetails(){
       return $this->belongsTo(Salary::class, 'employee_id', 'employee_id');
      }

        public function getuser(){
   			 return $this->belongsTo(User::class, 'employee_id', 'employee_id');
      	 } 
}
