<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\report;
use App\Models\Fields;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use DynamicFieldsHelper;
class ReportController extends Controller
{
    /*this function will display report file where person can apply filter on report.*/
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function Report(Request $request)
    {
      
        $page_limit  = ($request['page_range'])?$request['page_range']:config('constant.recordPerPage');

        $array['table_name'] = 'reports';
        $fields = DynamicFieldsHelper::getfieldlist($array);
        $array['pluck'] ='field_text';
        $fielddata = DynamicFieldsHelper::getfieldlist($array);

        // $fielddata=Fields::select()->where('table_name','reports')->pluck('field_text')->toArray();
        // $fields=Fields::where('table_name','=','reports')->get();
    	$dataQuery = report::select();
        if ($request->has('search_submit') && $request->search_submit != '') {
            if ($request->has('search_by_user_name') && $request->search_by_user_name != '') {
                $dataQuery->where('name','like','%'. $request->search_by_user_name . '%');
            }
            if ($request->has('search_by_employee_id') && $request->search_by_employee_id != '') {
                $dataQuery->where('employee_code','like','%'.  $request->search_by_employee_id .'%');
            }
         }
         $report_data = $dataQuery->latest()->paginate($page_limit);
         $empName = User::where('status','Active')->orderBy('name','asc')->pluck('name')->toArray();
         $empId = User::where('status','Active')->orderBy('employee_id','asc')->pluck('employee_id')->toArray();
        return view('ReportView',compact('report_data','fielddata','fields','request','empName','empId'));
    }

    public function ReportStore(Request $request)
    {
    	//print_r($request->all());exit;
    	$report='';
       // $report = report::get();
        // foreach($report as $val)
        // {
        //     $val->delete();
        // }
        $start_date=$request->StartDate;
        $end_date=$request->EndDate;
        $report_month=$request->ReportMonth;
        $report_year=$request->ReportYear;
        if(isset($start_date) && isset($end_date) && !empty($start_date) && !empty($end_date))
        {
        	 $url = 'https://pms.tridhyatech.com/api/reports/monthly-leave/'.$start_date.'/'.$end_date;
        }else
        {
        	 $url = 'https://pms.tridhyatech.com/api/reports/monthly-leave/'.$report_month.'/'.$report_year;
        }
    // echo $url;exit;
        $ch = curl_init();
        $payload = '{""}';
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT,true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $ReportResult = curl_exec($ch);
    // echo "<pre>";print_r($ReportResult);exit;
        $ReportData=json_decode($ReportResult);
     if(!empty($ReportData->data)){
        foreach($ReportData->data as $index=>$value)
        {
            $report = report::where('company_id',$value->company_id)->where('employee_code',$value->empCode)->where('month',$report_month)->where('year',$report_year)->first();
            if(!empty($report)){
                $report->company_id = $value->company_id;
                $report->name = $value->name;
                $report->employee_code = $value->empCode;
                $report->total_number_of_days = $value->total_number_of_days;
                $report->weekOff = $value->weekOff;
                $report->total_working_days = $value->total_working_days;
                $report->total_worked_days = $value->total_worked_days;
                $report->credit_leave_cf = $value->credit_leave_cf;
                $report->leave_of_month = $value->leave_of_month;
                $report->balance_leave = $value->balance_leave;
                $report->comp_off = $value->comp_off;
                $report->incase_comp_off = $value->incase_comp_off;
                $report->balance_leave_after_companyoff = $value->balance_leave_after_companyoff;
                $report->month = $report_month;
                $report->year = $report_year;
                $report->lwp = $value->lwpCount;
                $report->save();
            }else{
                $report = New report();
                $report->company_id = $value->company_id;
                $report->name = $value->name;
                $report->employee_code = $value->empCode;
                $report->total_number_of_days = $value->total_number_of_days;
                $report->weekOff = $value->weekOff;
                $report->total_working_days = $value->total_working_days;
                $report->total_worked_days = $value->total_worked_days;
                $report->credit_leave_cf = $value->credit_leave_cf;
                $report->leave_of_month = $value->leave_of_month;
                $report->balance_leave = $value->balance_leave;
                $report->comp_off = $value->comp_off;
                $report->incase_comp_off = $value->incase_comp_off;
                $report->balance_leave_after_companyoff = $value->balance_leave_after_companyoff;
                $report->month = $report_month;
                $report->year = $report_year;
                $report->lwp = $value->lwpCount;
                $report->save();
            }
            
        }
     }
          
        curl_close($ch);
        alert()->success('All Reports Synchronization Done')->persistent('close')->autoclose("36000");
        return redirect('/report');
    }
}
