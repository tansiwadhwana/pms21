<div id="importtdsModal" class="modal fade " role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header card-header bg-dark">
                <h4 class="modal-title">Import TDS Amount</h4>
                <button type="button" class="close" data-dismiss="modal" style="color: #fff; opacity: 1;">&times;</button>
            </div>
             <form class="forms-sample" method="post" action="{{url('importTds')}}" autocomplete="off" id="importForm" enctype="multipart/form-data">
                   @csrf
                <div class="modal-body" id="modal-body">
               
                    <input type="hidden" name="ReportMonth" value="{{$month}}">
                    <input type="hidden" name="ReportYear" value="{{$year}}">

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Import TDS Sheet<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6">   
                                        <input type="file" name="import_sheet" id="importsheet" class="form-control">              
                                    </div>                                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary btn-dark" value="Import"/>
                    <button type="button" class="btn btn-info btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>