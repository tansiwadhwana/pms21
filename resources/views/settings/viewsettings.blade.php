@extends('layouts.master')
@section('content')
@section('moduleName')
    User
@endsection
@include('sweet::alert')
<link rel="stylesheet" href="{{asset('css/sweetalert.css')}}">
<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/home')}}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{url('/settings')}}">General Settings</a></li>
                </ol>
            </div>
            <div>
                <div class="card-body text-left pt-2 pb-2">
                    <a href="{{url('/setting_sync')}}" title="Back" class="btn btn-dark">
                        Setting Sync
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
    
<section class="content">
    <div class="container-fluid">
        <div class="card card-primary">
            <div class="card-header bg-dark">
                <h3 class="card-title">General Settings</h3>
            </div>                    
            
         
            <input type = "hidden" name = "id" value="{{(isset($settings['id']))? $settings['id']:'' }}">
                <div class="card-body">
                    <form action="{{url('/settingsUpdate')}}" method="post"> 
                        @csrf
                    <div class="row">
                                             
                        <div class="col-md-12">                                
                            <div class="form-group">
                                <label class="">
                                    Gratuity
                                   
                                </label>
                                <input type="text" id="gratuity" name="gratuity" class="form-control" value="{{(isset($settings->gratuity)) ? $settings->gratuity : '' }}" readonly="readonly">
                            </div> 
                        </div>
                        <div class="col-md-12">                                
                            <div class="form-group">
                                <label class="">
                                   Professional Tax
                                   
                                </label>
                               <input type="text" id="p_tax" name="p_tax" class="form-control" value="{{(isset($settings->professional_tax)) ? $settings->professional_tax : '' }}" readonly="readonly">
                            </div> 
                        </div>
                        <div class="col-md-12">                                
                            <div class="form-group">
                                <label class="">
                                   Basic Salary
                                   
                                </label>
                               <input type="text" id="basic_salary" name="basic_salary" class="form-control" value="{{(isset($settings->basic_salary)) ? $settings->basic_salary : '' }}" readonly="readonly">
                            </div> 
                        </div>
                        <div class="col-md-12">                                
                            <div class="form-group">
                                <label class="">
                                  
                                Other Allownces
                                </label>
                               <input type="text" id="other_allownces" name="other_allownces" class="form-control" value="{{(isset($settings->other_allownces)) ? $settings->other_allownces : '' }}" readonly="readonly">
                            </div> 
                        </div> 
                        <div class="col-md-12">                                
                            <div class="form-group">
                                <label class="">
                                House Rent
                                   
                                </label>
                               <input type="text" id="house_rent" name="house_rent" class="form-control" value="{{(isset($settings->house_rent)) ? $settings->house_rent : '' }}" readonly="readonly">
                            </div> 
                        </div> 
                        <div class="col-md-12">                                
                            <div class="form-group">
                                <label class="">
                                Performance Allowance
                                </label>
                               <input type="text" id="performance_allowance" name="performance_allowance" class="form-control" value="{{(isset($settings->performance_allowance)) ? $settings->performance_allowance : '' }}" readonly="readonly">
                            </div> 
                        </div> 
                        <div class="col-md-12">                                
                            <div class="form-group">
                                <label class="">
                                Bonus Applicable Percentage
                                </label>
                               <input type="text" id="bonus_applicable_percentage" name="bonus_applicable_percentage" class="form-control" value="{{(isset($settings->bonus_applicable_percentage)) ? $settings->bonus_applicable_percentage : '' }}" readonly="readonly">
                            </div> 
                        </div>
                        
                        <div class="col-md-12">                                
                            <div class="form-group">
                                <label class="">
                                Mail To CC
                                </label>
                               <input type="text" id="mail_to_cc" name="mail_to_cc" class="form-control" value="{{(isset($settings->mail_to_cc)) ? $settings->mail_to_cc : '' }}">
                            </div> 
                        </div> 
                         <div class="col-md-12">                                
                            <div class="form-group">
                                <button class="btn btn-primary" type="submit">Save</button>
                            </div>
                        </div>
                       
                      
                    </div>
                     </form>
                    
                </div>
                                   
           
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
</section>

<!-- ============================================================== -->
<!-- End Page wrapper  -->
<!-- ============================================================== -->
@endsection
@section('javascript')
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<script type="text/javascript">
$.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
var server_url = "{{url('')}}";
    $( "#name" ).change(function() {
    //    alert($(this).val());
        $.ajax({
            type: 'POST',
            url: server_url + "/salary/get_user_details",
            data: {name : $(this).val()} ,
            success: function(resultData) {
                $('#employee_code').val(resultData.details.employee_id);
                $('#joining_date').val(resultData.details.joining_date);
                $('#confirmation_date').val(resultData.details.confirmation_date);
                $('#confirmation_date').addClass('disableText');
                $('#joining_date').addClass('disableText');
                //$('#employee_code').addClass('disableText');
            }
        });
    });
    $( "#employee_code" ).change(function() {
    //    alert($(this).val());
        $.ajax({
            type: 'POST',
            url: server_url + "/salary/get_user_details",
            data: {empid : $(this).val()} ,
            success: function(resultData) {
                $('#name').val(resultData.details.name);
                $('#joining_date').val(resultData.details.joining_date);
                $('#confirmation_date').val(resultData.details.confirmation_date);
                $('#confirmation_date').addClass('disableText');
                $('#joining_date').addClass('disableText');
               // $('#name').addClass('disableText');
            }
        });
    });
   
  $("#user_create").validate({
	        rules: {
                joining_date: "required",
                employee_code:"required",
                name:"required",
                confirmation_date:"required",
                salary:"required",
	        },
	        messages: {
	            joining_date: "Please select joining date",
                employee_code:"Please select employee date",
                name:"Please select name",
                confirmation_date:"Please select confirmation date",
                salary:"Please Enter Salary",
	            
	        },
	        submitHandler: function (form) {
	           $( "#user_create" ).submit();
	        }
	    });
</script>
@endsection

