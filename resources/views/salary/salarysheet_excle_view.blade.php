
<div class="card-body">
    <div class="table-responsive">
        <table id="holiday_listing" class="table table-striped table-bordered">
            <thead>
                <tr>
                    
                        
                            <th>Sr. No</th>
                            <th>Employee Id</th>
                            <th>Name</th>
                            <th>Designation</th>
                            <th>Technology</th>
                            <th>joining Date</th>
                            <th>pan Number</th> 
                            <th>Bank name</th>
                            <th>Account number</th>
                            <th>IFSC code</th>
                            <th>UAN Number</th>
                            <th>Leave Balance</th>
                            <th>Leave of The Month</th>
                            <th>Month Days</th>
                            <th>Working Days</th>
                            <th>Extra Leaves / Comp-off</th>
                            <th>Present Days</th>
                            <th>Total Days</th>
                            <th>Salary</th>
                            <th>Additional</th>
                            <th>Add</th>
                            <th>Salary Payable</th>
                            <th>Permenant_deduction</th>
                            <th>Refund</th>
                            <th>p.Tax</th>

                            <th>TDS</th>
                            <th>Gratuity</th>
                            <th>Provi.Fund @ 18%</th>
                            <th>Other Deduction</th>
                          
                            <th>Net Payable</th>
                            <th>Mode</th>
                           <!--  <th>Resignation Hold release</th> -->
                            <th>Note</th>
                           

                </tr>
            </thead>
            <tbody>
               
              
               @if($salraysheet->count()>0)
                          <?php $i=1;?>
                              @foreach($salraysheet as $s)
                              <?php $i++;?>
                <tr>
                  
                             
                             <td>{{$i}}</td>
                                 <td>{{'TRD'.$s->employee_id }}</td>
                               <td>{{$s->name }}</td>
                               
                               <td>{{ $s->designation}}</td>
                              <td>{{isset($s->getuser->department)?$s->getuser->department:''}}</td>
                               <td>{{ $s->joining_date }}</td>
                              <td>{{ $s->pan_number }}</td>
                              <td>{{ $s->bank_name }}</td>
                              <td>{{ $s->account_number }}</td>
                              <td>{{ $s->ifsc_code }}</td>
                               <td>{{ $s->UAN_number }}</td>
                               <td>{{ $s->credit_leave_cf }}</td>
                               <td>{{ $s->leave_of_month }}</td>
                               <td>{{ $s->month_days }}</td>
                               <td>{{ $s->month_working_days }}</td>
                               <td>{{ $s->salaryCalculation['incase_comp_off'] }}</td>
                               <td>{{ $s->salaryCalculation['present_days']}}</td>
                               <td>{{ $s->salaryCalculation['total_days'] }}</td>
                               <td>{{round($s->salaryCalculation['salary'])}}</td>
                               <td>{{ round($s->salaryCalculation['additional']) }}</td>
                               <td>{{ $s->salaryCalculation['add'] }}</td>


                               <td>{{ $s->salaryCalculation['salary_payable'] }}</td>
                               <td>{{ $s->salaryCalculation['permenant_deduction'] }}</td>
                               <td>{{ $s->salaryCalculation['refund'] }}</td>
                               <td>{{$s->salaryCalculation['p_tax']}}</td>
                               <td>{{ $s->salaryCalculation['tds'] }}</td>
                                <td>{{round($s->salaryCalculation['gratuity'])}}</td>
                                <td>{{round($s->salaryCalculation['provi_fund'])}}</td>

                               <td>{{ $s->salaryCalculation['other_deduction'] }}</td>
                               <td>{{ round($s->salaryCalculation['net_payable']) }}</td>
                               <!-- <td>{{ $s->resignation_hold_release }}</td> -->
                               <td>HDFC</td>
                                <td>{{$s->note}}</td>
                </tr>
                @endforeach
                @endif
               
            </tbody>
        </table>
    </div>
</div>