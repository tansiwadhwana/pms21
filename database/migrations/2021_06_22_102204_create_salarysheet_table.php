<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalarysheetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salarysheet', function (Blueprint $table) {
            $table->id();
            $table->string('employee_id')->nullable();
            $table->string('name')->nullable();
            $table->string('designation')->nullable();
            $table->string('technology')->nullable();
            $table->string('joining_date')->nullable();
            $table->string('pan_number')->nullable();
            $table->string('bank_name')->nullable();
            $table->string('account_number')->nullable();
            $table->string('ifsc_code')->nullable();
            $table->string('UAN_number')->nullable();
            $table->string('leaves')->nullable();
            $table->string('month_days')->nullable();
            $table->string('working_days')->nullable();
            $table->string('extra_leave')->nullable();
            $table->string('present_days')->nullable();
            $table->string('total_days')->nullable();
            $table->string('salary')->nullable();
            $table->string('additional')->nullable();
            $table->string('salary_payable')->nullable();
            $table->string('permenant_deduction')->nullable();
            $table->string('refund')->nullable();
            $table->string('p_tax')->nullable();
            $table->string('tds')->nullable();
            $table->string('gratuity')->nullable();
            $table->string('provi_Fund_18_persent')->nullable();
            $table->string('other_deduction')->nullable();
            $table->string('resignation_hold_release')->nullable();
            $table->string('net_payable')->nullable();
            $table->string('mode')->nullable();
            $table->string('month')->nullable();
            $table->string('year')->nullable();
            $table->string('note')->nullable();
            $table->timestamps();







        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salarysheet');
    }
}
