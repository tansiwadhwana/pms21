<div id="taskTimeEntryModal" class="modal fade " role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header card-header bg-dark">
                <h4 class="modal-title">Filter Report</h4>
                <button type="button" class="close" data-dismiss="modal" style="color: #fff; opacity: 1;">&times;</button>
            </div>
             <form class="forms-sample" enctype="form-data/multipart" method="post" action="{{url('reportinsert')}}" autocomplete="off" id="reportform">
                   @csrf
           <!--  <form method="post" action="{{url('save/project/entry')}}" id="saveTaskEntry">
                @csrf
                <input type="hidden" name="projectId" id="projectId" value="">

              
                    <input type="hidden" name="redirectSameScreen" id="redirectSameScreen" value="true" />
               
                    <input type="hidden" name="redirectSameScreen" id="redirectSameScreen" value="false" />
              
                <input type="hidden" name="projectTaskId" id="projectTaskId" value="" /> -->

                <div class="modal-body" id="modal-body">
               
                    

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Month<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6">                 
                                        <select class="form-control select2" name="ReportMonth" id="ReportMonth">
                                            <option value="">Please Select Month</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8" >8</option>
                                            <option value="9">9</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                        </select>
                                        <label id="ReportMonth-error" class="error" for="ReportMonth"></label> 
                                    </div>                                                   </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Year<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6"> 
                                    <?php 
                                        $currentyear = date("Y");    
                                    ?>                 
                                        <select class="form-control select2" name="ReportYear" id="ReportYear">
                                            <option value="">Please Select Year</option>
                                          @for($year='2015';$year<=$currentyear;$year++)
                                           <option value="{{$year}}">{{$year}}</option>
                                          @endfor
                                        </select>
                                        <label id="ReportYear-error" class="error" for="ReportYear"></label> 
                                    </div>                                                   </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary btn-dark" value="Save"/>
                    <button type="button" class="btn btn-info btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>