<!DOCTYPE html>
<html>
<head>
<title>Tridhya - Salary Slip</title>



</head>
<body>

<style>
    table, td, th {
        border: 1px solid #cccccc;
        border-collapse: collapse;
        /*font-family: Arial, Helvetica, sans-serif;*/
        font-family: Arial, Helvetica, sans-serif;
        color: #000000;
        font-size: 11pt;
        letter-spacing: 0.5px;
    }
   
    
</style>

<table cellspacing="0" cellpadding="0" style="width:100%">
  <tr>
        <td style="text-align: center; border: 0px;">
          <table cellspacing="0" cellpadding="0" style="width:100%; border: 0px;">
            <tr>
              <td style="padding-top: 10px; padding-bottom: 10px; "><img src="https://www.tridhya.com/wp-content/uploads/2018/11/tridhyatech.svg" alt="" title="Home" width="auto" style="border: 0; vertical-align: top; font-size: 0; "></td>
            </tr>
          </table>
          </td>
    </tr>
    <tr>
        <td style="text-align: center; border: 0px;">
          <table cellspacing="0" cellpadding="0" style="width:100%; border: 0px;">
            <tr>
              <td style="padding-top: 10px; padding-bottom: 10px; border-top: 0px; border-bottom: 0px; "><strong>Salary Slip For The Month Of {{$month_year}}</strong></td>
            </tr>
          </table>
          </td>
    </tr>
    <tr>
        <td style="border: 0px;">
            <table cellspacing="0" cellpadding="5" style="width:100%; border: 0px;">
                <tr>
                    <td><strong>Employee Name</strong></td>
                    <td colspan="4">{{$emp_name}}</td>
                </tr>
                <tr>
                    <td style="width: 20%;"><strong>Employee ID</strong></td>
                    <td style="width: 25%;">TRD{{$emp_code}}</td>
                    <td style="width: 100px;">&nbsp;</td>
                    <td style="width: 20%;"><strong>Bank</strong></td>
                    <td style="width: 25%;">{{$bank_name}}</td>
                </tr>
                <tr>
                    <td><strong>Designation</strong></td>
                    <td>{{$designation}}</td>
                    <td>&nbsp;</td>
                    <td><strong>IFSC Code</strong></td>
                    <td>{{$ifsc_code}}</td>
                </tr>
                <tr>
                    <td><strong>PAN Number</strong></td>
                    <td>{{$pan_no}}</td>
                    <td>&nbsp;</td>
                    <td><strong>Account Number</strong></td>
                    <td>{{$bank_account_no}}</td>
                </tr>
                <tr>
                    <td><strong>Location</strong></td>
                    <td>Ahmedabad</td>
                    <td>&nbsp;</td>
                    <td><strong>UAN Number</strong></td>
                    <td>{{$uan_no}}</td>
                </tr>
                <tr>
                    <td><strong>Date of Joining</strong></td>
                    <td>{{$joining_date}}</td>
                    <td>&nbsp;</td>
                    <td><strong></strong></td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="5">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: center;"><strong>Employee Leave Details (EL)</strong></td>
                    <td>&nbsp;</td>
                    <td colspan="2" style="text-align: center;"><strong>Attendance Details</strong></td>
                </tr>
                <tr>
                    <td><strong>Opening Bal.</strong></td>
                    <td style="text-align: center;">{{$leave_balance}}</td>
                    <td>&nbsp;</td>
                    <td><strong>Total Days</strong></td>
                    <td style="text-align: center;">{{$month_days}}</td>
                </tr>
                <tr>
                    <td><strong>Leaves Taken</strong></td>
                    <td style="text-align: center;">{{$leave_taken}}</td>
                    <td>&nbsp;</td>
                    <td><strong>Working Days</strong></td>
                    <td style="text-align: center;">{{$working_days}}</td>
                </tr>
                <tr>
                    <td><strong>EL Encash</strong></td>
                    <td style="text-align: center;">{{$el_encash}}</td>
                    <td>&nbsp;</td>
                    <td><strong>Present Days</strong></td>
                    <td style="text-align: center;">{{$present_days}}</td>
                </tr>
                <tr>
                    <td><strong>Balance</strong></td>
                    <td style="text-align: center;">{{$balance_leave_after_this_month}}</td>
                    <td>&nbsp;</td>
                    <td><strong>Leave Without Pay</strong></td>
                    <td style="text-align: center;">{{$leave_without_pay_no}}</td>
                </tr>
                <tr>
                    <td><strong>Comp off</strong></td>
                    <td style="text-align: center;">{{$el_encash_no}}</td>
                    <td>&nbsp;</td>
                    <td><strong></strong></td>
                    <td style="text-align: center;"></td>
                </tr>
                <tr>
                    <td colspan="5">&nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align: center;"><strong>CTC</strong></td>
                    <td style="text-align: center;"><strong>{{$CTC_Main}}</strong></td>
                    <td>&nbsp;</td>
                    <td style="text-align: center;"><strong>Salary Payable</strong></td>
                    <td style="text-align: center;"><strong>{{$salary_payable}}</strong></td>
                </tr>
                <tr>
                    <td colspan="5" style="border-bottom: 0;">&nbsp;</td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="5" style="width:100%; border: 0px;">
                <tr>
                    <td style="width: 35%;"><strong>Earnings</strong></td>
                    <td style="text-align: right; width: 10%;"><strong>Amount</strong></td>
                    <td style="width: 100px;">&nbsp;</td>
                    <td style="width: 35%;"><strong>Deduction</strong></td>
                    <td style="text-align: right; width: 10%;"><strong>Amount</strong></td>
                </tr>
                <tr>
                    <td>Basic Salary</td>
                    <td style="text-align: right;">{{$basic_salary}}</td>
                    <td>&nbsp;</td>
                    <td>Professional Tax</td>
                    <td style="text-align: right;">{{$professional_tax}}</td>
                </tr>
                <tr>
                    <td>House Rent Allowance</td>
                    <td style="text-align: right;">{{$house_rent_allowance}}</td>
                    <td>&nbsp;</td>
                    <td>PF - Employee Side</td>
                    <td style="text-align: right;">{{$pf_employee_side}}</td>
                </tr>
                <tr>
                    <td>Privileged Leave</td>
                    <td style="text-align: right;">{{$privileged_leave}}</td>
                    <td>&nbsp;</td>
                    <td>PF - Employer Side</td>
                    <td style="text-align: right;">{{$pf_employer_side_deductions}}</td>
                </tr>
                <tr>
                    <td>Bonus</td>
                    <td style="text-align: right;">{{$bonus}}</td>
                    <td>&nbsp;</td>
                     <td>ESIC</td>
                    <td style="text-align: right;">{{$esic}}</td>
                    
                </tr>
                <tr>
                    <td>Gratuity</td>
                    <td style="text-align: right;">{{$gratuity}}</td>
                    <td>&nbsp;</td>
                    <td>Gratuity</td>
                    <td style="text-align: right;">{{$gratuity_deduction}}</td>
                    
                </tr>
                <tr>
                    <td>PF - Employer</td>
                    <td style="text-align: right;">{{$pf_employer}}</td>
                    <td>&nbsp;</td>
                    <td>Bonus</td>
                    <td style="text-align: right;">{{$bonus_deduction}}</td>
                    
                </tr>
                <tr>
                    <td>ESIC -Employer</td>
                    <td style="text-align: right;">{{$esic_employer}}</td>
                    <td>&nbsp;</td>
                    <td>TDS</td>
                    <td style="text-align: right;">{{$tds}}</td>
                    
                </tr>
                <tr>
                    <td>Special Allowance</td>
                    <td style="text-align: right;">{{$special_allowance}}</td>
                    <td>&nbsp;</td>
                    <td>Other Deduction</td>
                    <td style="text-align: right;">{{$other_deduction}}</td>
                </tr>
                <tr>
                    <td>Other Allowance</td>
                    <td style="text-align: right;">{{$other_allowance}}</td>
                    <td>&nbsp;</td>
                    <td></td>
                    <td style="text-align: right;"></td>
                </tr>
                <tr>
                    <td>Comp-Off Encashment</td>
                    <td style="text-align: right;">{{$comp_off_enchashment}}</td>
                    <td>&nbsp;</td>
                    <td></td>
                    <td style="text-align: right;"></td>
                </tr>
                <tr>
                    <td>PL Leave Encashment</td>
                    <td style="text-align: right;">{{$pl_leave_encachement}}</td>
                    <td>&nbsp;</td>
                    <td></td>
                    <td style="text-align: right;"></td>
                </tr>
                <tr>
                    <td></td>
                    <td style="text-align: right;"></td>
                    <td>&nbsp;</td>
                    <td><strong>Total Deductions</strong></td>
                    <td style="text-align: right;"><strong>{{$total_deductions}}</strong></td>
                </tr>
                <tr>
                    <td><strong>Total Earnings</strong></td>
                    <td style="text-align: right;"><strong>{{$total_earnings}}</strong></td>
                    <td>&nbsp;</td>
                    <td><strong>Net Amount</strong></td>
                    <td style="text-align: right;"><strong>{{$net_amount}}</strong></td>
                </tr>
                <tr>
                    <td colspan="5">&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="text-align: center; border: 0px;">
          <table cellspacing="0" cellpadding="0" style="width:100%; border-top: 0px;">
            <tr>
              <td style="padding-top: 10px; padding-bottom: 10px; border-top: 0px;"><strong>"This is computer generated Payslip and doesn't require signature"</strong></td>
            </tr>
          </table>
          </td>
    </tr>
</table>




</body>
</html> 