@extends('layouts.master')
@section('content')
@section('moduleName')
    User
@endsection
@include('sweet::alert')
<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 no-block align-items-center">
                <div class="text-left">                
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url('dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{url('salary')}}">Salary</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Update Salary</li>
                    </ol>
                </div>   
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
    
<section class="content">
    <div class="container-fluid">
        <div class="card card-primary">
            <div class="card-header bg-dark">
                <h3 class="card-title">Update Salary</h3>
            </div>                    
            <form class="form-horizontal error_fix_table" method="post" action="{{url('salary/update')}}" name="user_create" id="user_create">
            @csrf
                <div class="card-body">
                    <div class="row">
                        <input type="hidden" value="{{ $data->id }}" name="hidden_id">                     
                        <div class="col-md-12">                                
                            <div class="form-group">
                                <label class="">
                                    Name
                                    <span class="error">*</span>
                                </label>
                                <select class=" form-control custom-select disableText" name="name" id="name" style="width: 100%; height:36px;" >
                                    <option value="">Select Name</option>
                                    @foreach($empName as $ename)
                                    <option value="{{$ename}}" {{ $data->name == $ename ? 'selected' : ''}}>{{$ename}}</option>
                                    @endforeach	
                                </select>
                            </div> 
                        </div>
                        <div class="col-md-12">                                
                            <div class="form-group">
                                <label class="">
                                    Employee Code
                                    <span class="error">*</span>
                                </label>
                                <select class="select2 form-control custom-select disableText" name="employee_code" id="employee_code" style="width: 100%; height:36px;" >
                                    <option value="">Select Employee Code</option>
                                    @foreach($empId as $eid)
                                    <option value="{{$eid}}" {{ $data->employee_id == $eid ? 'selected' : ''}}>{{'TRD'.$eid}}</option>
                                    @endforeach	
                                </select>
                            </div> 
                        </div>
                        <div class="col-md-12">                                
                            <div class="form-group">
                                <label class="">
                                    Joining Date
                                    
                                </label>
                               <input type="text" id="joining_date" name="joining_date" class="form-control disableText" value="{{ $data->joining_date }}">
                            </div> 
                        </div>
                        <div class="col-md-12">                                
                            <div class="form-group">
                                <label class="">
                                    Confirmation Date
                                    
                                </label>
                                <input type="text" id="confirmation_date" name="confirmation_date" class="form-control disableText" value="{{ $data->confirmation_date }}">
                            </div> 
                        </div>
                        <div class="col-md-12">                                
                            <div class="form-group">
                                <label class="">
                                    Salary
                                   
                                    <span class="error">*</span>
                                </label>
                                <input type="text" id="salary" name="salary" class="form-control" value="{{ $data->salary_amount }}">
                            </div> 
                        </div>
                        
                    </div>
                    
                    
                </div>
                <div class="card-footer">
                    <input type="submit" value="Update" class="btn btn-primary btn-dark">
                    <a href="{{url('salary')}}"><button type="button" value="Cancel" class="btn btn-info btn-secondary">Cancel</button></a>                        
                </div>                    
            </form>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
</section>

<!-- ============================================================== -->
<!-- End Page wrapper  -->
<!-- ============================================================== -->
@endsection
@section('javascript')
<script type="text/javascript">
$.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
var server_url = "{{url('')}}";
    $( "#name" ).change(function() {
    //    alert($(this).val());
        $.ajax({
            type: 'POST',
            url: server_url + "/salary/get_user_details",
            data: {name : $(this).val()} ,
            success: function(resultData) {
                $('#employee_code').val(resultData.details.employee_id);
                $('#joining_date').val(resultData.details.joining_date);
                $('#confirmation_date').val(resultData.details.confirmation_date);
                $('#confirmation_date').addClass('disableText');
                $('#joining_date').addClass('disableText');
                $('#employee_code').addClass('disableText');
            }
        });
    });
    $( "#employee_code" ).change(function() {
    //    alert($(this).val());
        $.ajax({
            type: 'POST',
            url: server_url + "/salary/get_user_details",
            data: {empid : $(this).val()} ,
            success: function(resultData) {
                $('#name').val(resultData.details.name);
                $('#joining_date').val(resultData.details.joining_date);
                $('#confirmation_date').val(resultData.details.confirmation_date);
                $('#confirmation_date').addClass('disableText');
                $('#joining_date').addClass('disableText');
                $('#name').addClass('disableText');
            }
        });
    });
   $("#user_create").validate({
	        rules: {
               // joining_date: "required",
                employee_code:"required",
                name:"required",
              //  confirmation_date:"required",
                salary:"required",
	        },
	        messages: {
	          //  joining_date: "Please select joining date",
                employee_code:"Please select employee date",
                name:"Please select name",
               // confirmation_date:"Please select confirmation date",
                salary:"Please Enter Salary",
	            
	        },
	        submitHandler: function (form) {
	           form.submit();
	        }
	    });
  
</script>
@endsection

