<div id="importSheetModal" class="modal fade " role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header card-header bg-dark">
                <h4 class="modal-title">Import SalarySlip Data</h4>
                <button type="button" class="close" data-dismiss="modal" style="color: #fff; opacity: 1;">&times;</button>
            </div>
             <form class="forms-sample" enctype='multipart/form-data' method="post" action="{{url('importSalarySlipModal')}}" autocomplete="off" id="reportform">
                   @csrf
           <!--  <form method="post" action="{{url('save/project/entry')}}" id="saveTaskEntry">
                @csrf
                <input type="hidden" name="projectId" id="projectId" value="">

              
                    <input type="hidden" name="redirectSameScreen" id="redirectSameScreen" value="true" />
               
                    <input type="hidden" name="redirectSameScreen" id="redirectSameScreen" value="false" />
              
                <input type="hidden" name="projectTaskId" id="projectTaskId" value="" /> -->

                <div class="modal-body" id="modal-body">
               
                    

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12">
                            <p>Download example sheet for uploading  <a href="{{url('/DemoImportSheet.xlsx')}}"><b>here</b></a>
                            </p>
                        </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Import Sheet<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6">                 
                                     <input type="file" name="import_sheet_file" class="form-control" required>
                                    </div>            
                                </div>
                            </div>
                        </div>
                    </div>

                    
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary btn-dark" value="Save"/>
                    <button type="button" class="btn btn-info btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>