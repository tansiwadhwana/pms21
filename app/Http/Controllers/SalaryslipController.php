<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\report;
use App\Models\User;
use App\Models\Salary;
use App\Company;
use App\Models\Settings;
use App\Models\SlipGenratedMonth;
use App\Models\Attendence;
use App\Imports\UsersSalaryImport;
use Illuminate\Support\Facades\Validator;
use App\Models\Salarysheet;
use App\Models\Salarysheet_attechment;
use App\Exports\Exportsalarysheet;
use App\Imports\ExportSalarySlip;
use App\Models\Fields;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use App\Mail\SalaryslipMail;
use Illuminate\Support\Facades\Mail;
use Alert;
use PDF;
use MPDF;
use Excel;
use DB;
use Carbon\Carbon;

class SalaryslipController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function SalarySlip(Request $request, $m = null ,$y = null)
    {
    
        $month = '';
        $year = '';
        $page_limit  = ($request['page_range'])?$request['page_range']:config('constant.recordPerPage');
        // echo 'hidden'; print_r($page_limit);exit;
        $genralsettingdata = Settings::first();
        $empName = User::where('status','Active')->orderBy('name','asc')->pluck('name')->toArray();
        $empId = User::where('status','Active')->orderBy('employee_id','asc')->pluck('employee_id')->toArray();
 
          if(isset($request['ReportMonthDropdown']) && isset($request['ReportYearDropdown']) && !empty($request->ReportMonthDropdown) && !empty($request->ReportYearDropdown))
                 { 
                    
                         
                         $month = $request->ReportMonthDropdown;
                         $year = $request->ReportYearDropdown;
                 }else{
                         if(!empty($m) && !empty($y))
                         { 
                             
                              $month = $m;
                               $year = $y;
   
 
                               $dataQuery = Salarysheet::with('salaryDetails')->leftjoin('users','users.employee_id','salarysheet.employee_id')->where('salarysheet.month',$month)->where('salarysheet.year',$year)->select('salarysheet.*');
                               if ($request->has('search_submit') && $request->search_submit != '') {
                                 if ($request->has('search_by_user_name') && $request->search_by_user_name != '') {
                                     $dataQuery->where('salarysheet.name','like','%'. $request->search_by_user_name . '%');
                                 }
                                 if ($request->has('search_by_employee_id') && $request->search_by_employee_id != '') {
                                     $dataQuery->where('salarysheet.employee_id','like','%'.  $request->search_by_employee_id .'%');
                                 }
                              }
                                $dataQuery->where('users.salary_structure',0);
                                 $salrayslipdata = $dataQuery->groupBy('salarysheet.employee_id')->paginate($page_limit);
                                 $userdata = Salarysheet::select()->where('month',$month)->where('year',$year); 
                                //   foreach ($salraysheetdata as $key => $value) {
                                //         if(isset($value->salaryDetails) && !empty($value->salaryDetails)){
                                //               $getusercompoff = report::select('comp_off')->where('employee_code',$value->employee_id)->where('month',$month)->where('year',$year)->first();
                                  
                                //              $getsalarydata =  $this->salaryCalculation($value,$genralsettingdata,$getusercompoff);
 
                                //                // $value->additional = $getsalarydata['additional'];
                                //                $value->gratuity = $getsalarydata['gratuity'];
                                //                $value->salary_payable = $getsalarydata['salary_payable'];
                                //                $value->net_payable = $getsalarydata['net_payable'];
                                              
                                //           }      
                                //  }
                              
                             
                                  return view('salaryslip.list',compact('salrayslipdata','genralsettingdata','month','year','empName','empId','request'));
 
                         }
                       
                            
                      }
 
 
           
               
              
                
                  if(!empty($month) && !empty($year))
                  {
                    
                    $salrayslipmonthdata=SlipGenratedMonth::where('month',$month)->where('year',$year)->get();
                     if(!$salrayslipmonthdata->isEmpty() && !isset($request->search_submit) && $request->search_submit != '')
                     {    
                       
                         $getsalrayslipmonthdata = SlipGenratedMonth::where('month',$month)->where('year',$year)->first(); 
 
                    
                         return view('salaryslip.list',compact('getsalrayslipmonthdata','request','empName','empId','month','year'));
                     }
                     else{
                       
                        $dataQuery = Salarysheet::leftjoin('salarys','salarysheet.employee_id','salarys.employee_id')->leftjoin('reports','reports.employee_code','salarysheet.employee_id')->leftjoin('users','users.employee_id','salarysheet.employee_id')->where('salarysheet.month',$month)->where('salarysheet.year',$year)->select('salarys.*','reports.*','salarysheet.*','salarysheet.id as id');
                        if ($request->has('search_submit') && $request->search_submit != '') {
                            if ($request->has('search_by_user_name') && $request->search_by_user_name != '') {
                                $dataQuery->where('name','like','%'. $request->search_by_user_name . '%');
                            }
                            if ($request->has('search_by_employee_id') && $request->search_by_employee_id != '') {
                                $dataQuery->where('employee_id','like','%'.  $request->search_by_employee_id .'%');
                            }
                         }
                         $dataQuery->where('users.salary_structure',0);
                         $salrayslipdata = $dataQuery->groupBy('salarysheet.employee_id')->paginate($page_limit);
                     // /    echo "<pre>";print_r($salrayslipdata);exit;
                        return view('salaryslip.list',compact('salrayslipdata','month','year','empName','empId','request'));
                             
                           
                     }
                  } 
            
                    
                  if(isset($request['ReportMonth']) && isset($request['ReportYear']) && !empty($request->ReportMonth) && !empty($request->ReportYear))
                    {
                        $month = $request->ReportMonth;
                        $year = $request->ReportYear;
                        $dataQuery=Salarysheet::with('salaryDetails')->leftjoin('users','users.employee_id','salarysheet.employee_id')->where('salarysheet.month',$request['ReportMonth'])->where('salarysheet.year',$request['ReportYear']);
                        // echo '<pre>';print_r($dataQuery->get());exit;
                        if ($request->has('search_submit') && $request->search_submit != '') {
                            if ($request->has('search_by_user_name') && $request->search_by_user_name != '') {
                                $dataQuery->where('salarysheet.name','like','%'. $request->search_by_user_name . '%');
                            }
                             
                            if ($request->has('search_by_employee_id') && $request->search_by_employee_id != '') {
                                // echo 'hidden'; print_r($request->search_by_employee_id);exit;
                                $dataQuery->where('salarysheet.employee_id','like','%'.  $request->search_by_employee_id .'%');
                            }
                         }
                         $dataQuery->where('users.salary_structure',0);
                         $salrayslipdata = $dataQuery->groupBy('salarysheet.employee_id')->paginate($page_limit);
                        return view('salaryslip.list',compact('salrayslipdata','month','year','empName','empId','request')); 
                    }      
 
                    
          // echo 'hidden'; print_r($page_limit);exit;
         
              
             $salrayslipmonthdata=SlipGenratedMonth::select()->orderBy('month','asc')->paginate($page_limit);
            //  echo 'hii';exit;
            //   echo '<pre>'; print_r($salrayslipmonthdata);exit;
                 
  
                
                      return view('salaryslip.list',compact('salrayslipmonthdata','month','year','request','empName','empId'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function SalarySlipCreate(Request $request)
    { 
        $month = $request->ReportMonthDropdown;
        $year = $request->ReportYearDropdown;
        $page_limit  = ($request['page_range'])?$request['page_range']:config('constant.recordPerPage');
        $salrayslipdata=Salarysheet::select()->where('month',$month)->where('year',$year)->groupBy('salarysheet.employee_id')->paginate($page_limit);
        // echo '<pre>';print_r($salrayslipdata);exit;
        return view('salaryslip.list',compact('salrayslipdata','month','year'));
        
    }

    public function SendSalarySlip(Request $request,$cron = '')
    {
       

        $arr = array();
        $elEncashNo = 0;
        $leaveWithoutPayNo = 0;
       
        $elEncashAmount = 0;
        $leaveWithoutPayAmount = 0;
        if(isset($request))
        {
            $month = $request->month;
            $year = $request->year;
    
        }
        else{
            $month = $data['month'];
            $year = $data['year'];
        }

               if(!empty($cron))
        {
            $salraysheetdata1=Salarysheet::leftjoin('salarys','salarysheet.employee_id','salarys.employee_id')->leftjoin('reports','reports.employee_code','salarysheet.employee_id')->leftjoin('users','users.employee_id','salarysheet.employee_id')->where('salarysheet.month',$month)->where('salarysheet.year',$year)->where('salarysheet.slip_genrated','!=',1)->groupBy('salarysheet.employee_id')->get();
        }
       
        if(isset($request->id)){
            //   echo "in";exit;
            //echo "<pre>"; print_r($request->id);exit;
               $salraysheetdata1=Salarysheet::leftjoin('salarys','salarysheet.employee_id','salarys.employee_id')->leftjoin('reports','reports.employee_code','salarysheet.employee_id')->leftjoin('users','users.employee_id','salarysheet.employee_id')->whereIn('salarysheet.id',$request->id)->where('salarysheet.month',$request->month)->where('salarysheet.year',$request->year)->groupBy('salarysheet.employee_id')->get();

           }else{
             
               $salraysheetdata1=Salarysheet::leftjoin('salarys','salarysheet.employee_id','salarys.employee_id')->leftjoin('reports','reports.employee_code','salarysheet.employee_id')->leftjoin('users','users.employee_id','salarysheet.employee_id')->where('salarysheet.month',$request->month)->where('salarysheet.year',$request->year)->groupBy('salarysheet.employee_id')->get();

           }
           
                foreach ($salraysheetdata1 as $key => $value) {
                 
                 $getuserdata= User::where('employee_id',$value->employee_id)->first();
                 $genralsettingdata = Settings::first();  
                 $elEncashNo = (!empty($value->incase_comp_off))?$value->incase_comp_off:'';
                 if($value->salary_payable != 0 && $value->working_days != 0 && $elEncashNo != 0){
                    $elEncashAmount = round(($value->salary_payable/$value->working_days)*$elEncashNo);
                 }
                 $leaveCountLwp = $value->leave_of_month - $value->credit_leave_cf;
                  if($leaveCountLwp > 0){
                    $leaveWithoutPayNumber = (!empty($value->lwp))?$value->lwp:0;
                        $leaveWithoutPayNo = $leaveWithoutPayNumber + $leaveCountLwp;
                  }else{
                     $leaveWithoutPayNo = (!empty($value->lwp))?$value->lwp:0;
                  }
                   

                    if($value->salary_payable != 0 && $value->working_days != 0 && $leaveWithoutPayNo != 0){
                        $leaveWithoutPayAmount = round(($value->salary_payable/$value->working_days)*$leaveWithoutPayNo);
                    }
                    if($value->salary_payable<=21000)
                    {
                        $basicSalaryCtc = $value->salary_payable;
                    }
                    else{
                        $basicSalaryCtc = ($value->salary_payable*$genralsettingdata['basic_salary'])/100;
                    }

                    $gratuity = round(($value->salary_payable*$genralsettingdata['gratuity'])/100);
                    $houseRentAllowCtc =  round(($value->salary_payable*$genralsettingdata['house_rent'])/100);
                  
                    $performanceAllowCtc =  round(($value->salary_payable*$genralsettingdata['performance_allowance'])/100);
                    $otherAllowCtc =  round(($value->salary_payable*$genralsettingdata['other_allownces'])/100);
                    
                //    $compayname  = Company::select('company_name')->where('id',$value->company_id)->first();
               

                        if($value->working_days != 0 &&  $value->present_days != 0){
                            $basicSalaryAmount = round(($basicSalaryCtc/$value->working_days)*$value->present_days);
                            $houseRentAllowSalAmount = round(($houseRentAllowCtc/$value->working_days)*$value->present_days);
                            $otherAllowSalAmount = round(($otherAllowCtc/$value->working_days)*$value->present_days);
                            $performanceAllowSalAmount = round(($performanceAllowCtc/$value->working_days)*$value->present_days);
                        }
                      $balanceCF = 0;
                      $arr['emp_code']  = $value->employee_id ;
                      //  $arr['company_name'] = $compayname;
                     $arr['month_year'] = date('F', mktime(0, 0, 0, $value->month, 10)) .' - '. $value->year;
                     $arr['user_id'] = $getuserdata->id;
                     $arr['emp_name'] = (!empty($getuserdata))?$getuserdata->name:'';
                     $arr['emp_email'] = (!empty($getuserdata))?$getuserdata->email:'';
                     $arr['personal_email'] = (!empty($getuserdata))?$getuserdata->personal_email:'';
                     $arr['department'] = (!empty($getuserdata))?$getuserdata->department:'';
                     $arr['designation'] =(!empty($getuserdata))?$getuserdata->designation_name:'';
                     $arr['bank_name'] =(!empty($getuserdata))?$getuserdata->bank_name:'';
                     $arr['bank_account_no'] =(!empty($getuserdata))?$getuserdata->bank_account_no:'';
                     $arr['joining_date'] = (!empty($getuserdata))?$getuserdata->joining_date:'';
                     $arr['pan_no'] = (!empty($getuserdata))?$getuserdata->pan_no:'';
                     $arr['ifsc_code'] = (!empty($getuserdata))?$getuserdata->ifsc_code:'';
                     $arr['status'] = (!empty($getuserdata))?$getuserdata->employee_status:'';

                    $arr['leave_balance'] = $value->credit_leave_cf;
                    $arr['leave_taken'] = $value->leave_of_month;
                    $arr['working_days'] = $value->working_days;
                    $arr['CTC_Main']= $value->salary_amount;
                     if( ( $arr['leave_balance'] - $arr['leave_taken']) < 0 ){
                        $arr['total_paid_Days'] =  $arr['working_days'] - ($arr['leave_taken'] -  $arr['leave_balance']);
                    }else{
                        $arr['total_paid_Days'] = $arr['working_days'];
                    }
                    $arr['salary_payable'] = $arr['CTC_Main'] * $arr['total_paid_Days'] / $arr['working_days'];
                  //  dd($arr['salary_payable']);

                    $arr['CTC_Basic_Salary'] = ($arr['CTC_Main']*50)/100;
                    $arr['CTC_House_Rent_Allowance'] = ($arr['CTC_Basic_Salary']*40)/100;
                    $arr['CTC_Special_Allowance'] = ($arr['CTC_Main'] - ($arr['CTC_Basic_Salary'] + $arr['CTC_House_Rent_Allowance']))/2;
                    $arr['CTC_Other_Allowance'] = $arr['CTC_Special_Allowance'];
                    $arr['CTC_Total_Earning'] = $arr['CTC_Basic_Salary'] + $arr['CTC_House_Rent_Allowance'] + $arr['CTC_Special_Allowance'] + $arr['CTC_Other_Allowance'] ;

                     if($value->salary_amount<=21000)
                    {

                        $basicSalaryCtc = $value->salary_amount;
                   // echo "das" + $basicSalaryCtc;exit;
                    }
                    else{

                        $basicSalaryCtc = round(($arr['salary_payable']*50)/100);
                    //echo "df ".$basicSalaryCtc;exit;
                    }
                   
                    $arr['Basic_Salary']= $basicSalaryCtc;
                    if($value->bonus_applicable == 1){
                        $arr['variable_deduction'] = round($value->salary_amount/13);
                    }else{
                        $arr['variable_deduction'] = 0;
                    }
                   // $arr['pf_employee_contribution'] = (!empty($value->provi_Fund_18_persent))?$value->provi_Fund_18_persent:0;
                    // if(!empty($value->provi_Fund_18_persent)){
                    //     $arr['pf_employee_contribution'] = $value->provi_Fund_18_persent /2;
                    // }else{
                    //     $arr['pf_employee_contribution'] = 0;
                    // }
                    if(!empty($value->provi_Fund_18_persent) || $value->provi_Fund_18_persent != 0){
                        if($value->provi_Fund_18_persent <= 3600){

                            $arr['pf_employee_contribution'] = round($value->provi_Fund_18_persent /2);
                        }else{
                            $arr['pf_employee_contribution'] = round(3600/2);
                        }
                    }else if($getuserdata->provi_fund == 1){
                        if(round(($arr['CTC_Main']* 18)/100) <= 3600){
                            $arr['pf_employee_contribution'] = round((($arr['CTC_Main']* 18)/100)/2);
                        }else{
                            $arr['pf_employee_contribution'] = round(3600/2);
                        }

                         
                    }else{
                        $arr['pf_employee_contribution'] = 0;
                    }
                    $gratuity = round(($value->salary_amount*$genralsettingdata['gratuity'])/100);
                  //  dd($genralsettingdata['gratuity']);
                     if($value->salary_amount<=21000)
                    {
                        $houseRentAllowCtc =  0;
                        $arr['houseRentAllowCtc']= $houseRentAllowCtc;
                        $specialOther = 0;
                        $arr['specialAllowCtc']= $specialOther;
                        $arr['otherAllowCtc']= $specialOther;
                    }else{
                        $houseRentAllowCtc =  round(($basicSalaryCtc*40)/100);
                        $arr['houseRentAllowCtc']= $houseRentAllowCtc;
                        $specialOther = round(($arr['salary_payable'] - ($basicSalaryCtc + $houseRentAllowCtc))/2 - $arr['variable_deduction']);
                        $arr['specialAllowCtc']= round($specialOther - $arr['pf_employee_contribution']);


                        $arr['otherAllowCtc']= round(($arr['salary_payable'] - ($basicSalaryCtc + $houseRentAllowCtc))/2 - $gratuity);
                    }
                   // $arr['otherAllowCtc'] = round(($arr['specialAllowCtc'] - (($arr['CTC_Total_Earning']*1.92)/100)));

                    $arr['additionalAmountForCompoff'] = round(($value->salary_amount*((int)$elEncashNo))/$value->working_days);
                  //  $arr['additionalAmountForCompoff'] = 1190;
                    $arr['Adjustments'] = 0;
                    $arr['refund'] = $value->refund;

                    
                    
                    //$arr['gratuity'] = $gratuity;
                    $arr['gratuity'] = 0;
                    
                    $arr['permanent_deduction'] = $value->permenant_deduction;
                    $arr['other_deduction'] = $value->other_deduction;
                    
                    $arr['lwp'] = round(($arr['CTC_Main']*$leaveWithoutPayNo)/$value->working_days);

                    $arr['total_eranings'] = $basicSalaryCtc + $houseRentAllowCtc + $specialOther + $arr['otherAllowCtc'] + $arr['additionalAmountForCompoff'] + $arr['Adjustments'] + $arr['refund'];


                   if($arr['total_eranings'] >= 12000){
                        $professional_tax = 200;
                    }elseif($arr['total_eranings'] >= 9000){
                         $professional_tax = 150;
                    }elseif ($arr['total_eranings'] >= 6000) {
                        $professional_tax = 80;
                    }else{
                         $professional_tax = 0;
                    }
                    $arr['professional_tax'] = $professional_tax;
                    $arr['total_deductions'] = $arr['professional_tax'] + $arr['gratuity'] + $arr['pf_employee_contribution'] +  $arr['permanent_deduction'] + $arr['other_deduction'] + $arr['variable_deduction'] + $arr['lwp'];
                    $arr['new_total_deductions'] = $arr['professional_tax'] + $arr['pf_employee_contribution'];
                    $arr['new_net_amount'] =  $arr['total_eranings'] - $arr['new_total_deductions'];
                    $arr['net_amount'] = $arr['total_eranings'] - $arr['total_deductions'];
                   
                    
                    if(($value->credit_leave_cf - $value->leave_of_month) >= 0){
                        $arr['balance_leave_after_this_month'] = $value->credit_leave_cf - $value->leave_of_month;
                    }else{
                        $arr['balance_leave_after_this_month'] =  0;
                    }
                    $arr['leave_without_pay_no'] = $leaveWithoutPayNo;
                    

                     $arr['uan_no'] = 0;
                     $arr['esic_no'] = 0;
                     $arr['pf_no'] = 0;
                     if(!empty($elEncashNo)){
                        $arr['el_encash_no'] = $elEncashNo;
                     }else{
                        $arr['el_encash_no'] = 0;
                     }
                     
                     $arr['leave_issue'] = $value->leaves;
                     
                     
                     $arr['el_encash_amount'] = $elEncashAmount;
                     $arr['balance_cf'] = $balanceCF;
                     $arr['adjusted'] = 0 ;
                     $arr['month_days'] = $value->month_days;
                     
                     $arr['present_days'] = $value->present_days;
                     
                     $arr['ctc'] = $value->salary_payable;
                     $arr['basic_salary_ctc'] = $basicSalaryCtc;
                     $arr['house_rent_allowance_ctc'] = $houseRentAllowCtc;
                     $arr['other_allownces_ctc'] = $otherAllowCtc;
                     $arr['performance_allowance_ctc'] = $performanceAllowCtc;
                     $arr['performance_allowance'] = (!empty($performanceAllowSalAmount))?$performanceAllowSalAmount:0;
                     $arr['basic_salary_amount'] =  (!empty($basicSalaryAmount))?$basicSalaryAmount:0;
                     $arr['house_rent_allowance_salary'] = (!empty($houseRentAllowSalAmount))?$houseRentAllowSalAmount:0;
                     $arr['other_allownces_salary'] = (!empty($otherAllowSalAmount))?$otherAllowSalAmount:0;
                     $arr['bonus'] = 0;
                     $arr['other'] = 0;
                     
                     $arr['income_tax'] = 0;
                     //$arr['gratuity'] = $gratuity;
                     
                     $arr['esi_employee_contribution'] = 0;
                     $arr['leave_without_pay_amount'] = (!empty($leaveWithoutPayAmount))?$leaveWithoutPayAmount:0;
                    // $arr['other_deduction'] = 0;


                     // New Array created at 28 july 2022

                     $arr['salary_payable'] = round(($arr['CTC_Main'] * $arr['present_days']) / $arr['working_days']);
                     // if($arr['salary_payable'] <= 21000){
                     //    $arr['basic_salary'] = round(($arr['salary_payable'] * 60)/100);
                     // }else{
                     //    $arr['basic_salary'] = round(($arr['salary_payable'] * 50)/100);
                     // }
                     $arr['basic_salary'] = round(($arr['salary_payable'] * 50)/100);
                     $arr['house_rent_allowance'] = round(($arr['salary_payable'] * 15)/100);
                     $arr['privileged_leave'] = round(($arr['basic_salary'] * 4.81)/100);

                     $bonus_applicable = (!empty($getuserdata))?$getuserdata->bonus_applicable:'';
                     if($bonus_applicable == 1){
                        $arr['bonus'] = round(($arr['CTC_Main'] / 13));
                     }else{
                        if($arr['basic_salary'] <= 21000){
                            $arr['bonus'] = round(($arr['basic_salary'] * 20)/100);
                         }else{
                            $arr['bonus'] = 0;
                         }
                     }
                     
                    if($getuserdata->gratuity == 1){
                         $arr['gratuity'] = round(($arr['salary_payable'] * 1.92)/100);
                     }else{
                        $arr['gratuity'] = 0;
                     }

                      if($getuserdata->provi_fund == 1){
                         if(($arr['salary_payable'] - $arr['house_rent_allowance']) < 15000){
                            $arr['pf_employer'] = round((($arr['salary_payable'] - $arr['house_rent_allowance']) * 12)/100);
                         }else{
                            $arr['pf_employer'] = round((15000 * 12)/100);
                         }
                     }else{
                        $arr['pf_employer'] = 0;
                     }
                   //  $arr['pf_employer'] = 0;
                     if(($arr['CTC_Main'] - $arr['gratuity'] - $arr['pf_employer'] ) < 21000){
                        $arr['esic_employer'] = round((($arr['CTC_Main'] - $arr['pf_employer'])*3.25)/100);
                     }else{
                        $arr['esic_employer'] = 0;
                     }

                     $arr['special_allowance'] = ($arr['CTC_Main'] - $arr['basic_salary'] - $arr['house_rent_allowance'] - $arr['privileged_leave'] - $arr['bonus'] - $arr['gratuity'] - $arr['pf_employer'] - $arr['esic_employer']);
                     $arr['other_allowance'] = 0;
                     if($arr['el_encash_no'] >= 1){
                        $arr['comp_off_enchashment'] = round(($arr['el_encash_no'] * $arr['CTC_Main'])/$arr['present_days']);
                     }else{
                        $arr['comp_off_enchashment'] = 0;
                     }
                     
                     $arr['el_encash'] = 0;
                     $arr['pl_leave_encachement'] = round($arr['el_encash'] * ($arr['CTC_Main'] /2)/$arr['present_days']);

                     $arr['total_earnings'] = $arr['basic_salary'] + $arr['house_rent_allowance'] + $arr['privileged_leave'] + $arr['bonus'] + $arr['gratuity'] + $arr['pf_employer'] + $arr['esic_employer'] + $arr['special_allowance'] + $arr['other_allowance'] + $arr['comp_off_enchashment'] + $arr['pl_leave_encachement']; 
                     if($arr['CTC_Main'] <= 12000){
                         $arr['professional_tax'] = 0;
                     }else{
                        $arr['professional_tax'] = 200;
                     }
                     if($getuserdata->provi_fund == 1){
                         if(($arr['salary_payable'] - $arr['house_rent_allowance']) < 15000){
                            $arr['pf_employee_side'] = round((($arr['salary_payable'] - $arr['house_rent_allowance']) * 12)/100);
                         }else{
                            $arr['pf_employee_side'] = round((15000 * 12)/100);
                         }
                     }else{
                        $arr['pf_employee_side'] = 0;
                     }
                     $arr['pf_employer_side_deductions'] = $arr['pf_employee_side'];
                    // $arr['pf_employee_side'] = 0;
                     if(($arr['CTC_Main'] - $arr['gratuity'] - $arr['pf_employer'] ) < 21000){
                        $arr['esic'] = round((($arr['CTC_Main'] - $arr['gratuity'] - $arr['pf_employer']) * 0.75)/100);
                     }else{
                        $arr['esic'] = 0;
                     }
                     if($getuserdata->gratuity == 1){
                        $arr['gratuity_deduction'] = $arr['gratuity'];
                     }else{
                        $arr['gratuity_deduction'] = 0;
                     }
                     if($bonus_applicable == 1){
                        $arr['bonus_deduction'] = $arr['bonus'];
                     }else{
                        $arr['bonus_deduction'] = 0;
                     }
                     
                     $arr['tds'] = 0;
                     $arr['other_deduction'] = 0;
                  //   $arr['leave_with_out_pay'] = ($arr['CTC_Main'] - $arr['salary_payable']);
                     $arr['total_deductions'] = $arr['professional_tax'] + $arr['pf_employee_side'] + $arr['esic'] + $arr['gratuity_deduction'] + $arr['tds'] + $arr['other_deduction'] + $arr['pf_employer_side_deductions'] + $arr['bonus_deduction'];
                     $arr['net_amount'] = $arr['total_earnings'] - $arr['total_deductions'];
                     
                     $arr['year'] = $value->year;

                        if (!empty($arr)) {
                        $sumDeduction = round($arr['professional_tax'] + $arr['income_tax'] + $arr['gratuity'] + $arr['pf_employee_contribution'] +
                                            $arr['esi_employee_contribution']+ $arr['other_deduction'] + $arr['leave_without_pay_amount']) ;
                        $sumSalAmount = round($arr['basic_salary_amount'] +$arr['house_rent_allowance_salary'] +$arr['other_allownces_salary'] + $arr['performance_allowance'] +
                                        $arr['bonus'] + $arr['other'] + $arr['el_encash_amount']);
                        $netAmount =  round($sumSalAmount - $sumDeduction);
                        $arr['total_deduction'] = $sumDeduction;
                        $arr['sumSalAmount'] = $sumSalAmount;
                        $arr['from_email'] = config('constant.from_address');
                        $arr['from_name']  = config('constant.company_name');
                        $arr['comp_name']  = config('constant.company_name');
                        $arr['comp_address']  = config('constant.comp_address');
                        $arr['comp_website']  = config('constant.comp_website');
                        $arr['comp_logo']  = config('constant.comp_logo');
                        $arr['comp_contact']  = config('constant.comp_contact');
                        $arr['comp_email']  = config('constant.comp_email');
                       
                       }
                      //  dd("dsa");
                       $pdf = MPDF::loadView('salaryslip.salaryPDF', $arr);
                                        $pdf->output();
                        //$settings = Settings::first();
                        if(!empty($genralsettingdata->mail_to_cc)){
                            $ccArray = explode(",",$genralsettingdata->mail_to_cc);
                            Mail::send(new SalaryslipMail($pdf,$arr,$ccArray));
                        }else{
                              Mail::send(new SalaryslipMail($pdf,$arr));
                        }

                          
                        
                    

                       $changemailstatus= Salarysheet::where('employee_id',$value->employee_id)->where('month',$request->month)->where('year',$request->year)->first();
                     $changemailstatus->slip_genrated = 1;
                     $changemailstatus->slip_genrated_date = Carbon::today();
                     $changemailstatus->save();
                     
                   
                       
                      
                  
                    }
                    $getgenratedmonth = SlipGenratedMonth::where('month',$month)->where('year',$year)->first();
                    if(empty($getgenratedmonth))
                    {
                        $SlipGenratedMonth = new SlipGenratedMonth();
                        $SlipGenratedMonth->month = $month;
                        $SlipGenratedMonth->year= $year;
                        $SlipGenratedMonth->save(); 
                    }
                 
                  

                    if(isset($SlipGenratedMonth))
                    {
                        return response()->json(array('success' => true, 'msg'=>'Email Sent Successfully'));
                        // alert()->success('Email Sent Successfully')->persistent('close')->autoclose("36000");
                        // return view('salaryslip.list',compact('month','year'));
                    }else{
                        return response()->json(array('success' => false, 'msg'=>'Somthing is wrong.')); 
                    }
                   







              
                    
                   
      

    }

    public function ImportSalarySlipModal(Request $request)
    {
       

         $file = $request->file('import_sheet_file');
       
        $extension = $file->getClientOriginalExtension();
        $filename = 'import_salaryslip_'.time().'.'.$extension;
        if ($request) {
            Storage::disk('local')->putFileAs('', $file, $filename);
        }
        $importError = array();
      //  dd($filename);
        $importError = array();
        Excel::import(new ExportSalarySlip, $filename);
        $importError = Session::get('UserSalaryImportError');
      //  dd($importError);
       if(count($importError)>0){
            $count = $importError[0]['success'];
            alert()->success(' '.$count.' SalarySlip successfully sent over mail.')->persistent('close')->autoclose("36000");
        }
        $importError = Session::forget('UserSalaryImportError');
        return redirect('/salaryslip');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
