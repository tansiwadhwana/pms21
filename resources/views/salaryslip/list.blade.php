@extends('layouts.master')
@section('content')
@php
    $pageRangArray = config('constant.page_range');
@endphp
<style>
  .disabletext{
    /* pointer-events:none; */
    /* opacity:0.5; */
  }
  </style>
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/salaryslip')}}">Salary Slip</a></li>
                </ol>
            </div>
            <div class="col-sm-6">
               <div class="text-left pt-2 pb-2">
                          
                     <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Task Entry" 
                                                       class="btn btn-dark timeEntry1" data-task-id="" data-id=''>
                            Generate Salary Slip
                          </a>
                </div>
            </div>
            <div class="col-sm-6">
                 <div class=" text-left pt-2 pb-2" style="float:right;">
                          
                     <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Task Entry" 
                                                       class="btn btn-dark importsheet" data-task-id="" data-id=''>
                           Import SalarySlip Data 
                          </a>
                </div>
              </div>
            
        </div>
    </div>
</section>
<style>
  .salary_sheet_popup_modal .modal-body {
       max-height: calc(100vh - 210px);
       overflow-y: auto;
   }
   .salary_sheet_popup_modal th{
     white-space: nowrap;
   }
</style>
@include('sweet::alert') 
    <section class="content">
    <div class="container-fluid">
     @if(!empty($salrayslipdata))
   
     <div id="accordion">
            <div class="card">
            <div class="card-header bg-dark {{(!$request->has('search_submit'))?'collapsed':''}}" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false">
                    <h3 class="card-title">Filter Salary Sheet</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-chevron-down"></i></button>
                    </div>
                 </div>
                 <div id="collapseOne" class="panel-collapse in {{(!$request->has('search_submit'))?'collapse':'collapse show'}}">
                    <form class="form-horizontal" method="get" action="{{url('/salaryslip/')}}" name="search_filter" id="search_filter">
                       <input type="hidden" name="ReportMonth" id="ReportMonth" value="{{$month}}">
                        <input type="hidden" name="ReportYear" id="ReportYear" value="{{$year}}">
                        <div class="card-body">
                            <div class="row">
                                <div class="form-group col-lg col-md-6 col-sm-6">
                                    <label class="">User Name</label>
                                    <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="search_by_user_name" id="search_by_user_name">
                                        <option value="">Select</option>
                                        @foreach($empName as $ename)
                                        <option value="{{$ename}}" {{(isset($request->search_by_user_name) && ($request->search_by_user_name == $ename))?'selected':''}}>{{$ename}}</option>
                                        @endforeach
                                    </select>  
                                </div>
                                <div class="form-group col-lg col-md-6 col-sm-6">
                                    <label class="">Employee Id</label>
                                    <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="search_by_employee_id" id="search_by_employee_id">
                                        <option value="">Select</option>
                                         @foreach($empId as $eid)
                                          <option value="{{$eid}}" {{(isset($request->search_by_employee_id) && ($request->search_by_employee_id == $eid))?'selected':''}}>{{'TRD'.$eid}}</option>
                                          @endforeach	
                                    </select>  
                                </div>
                               
                                
                            </div>

                            
                            
                        </div>
                        {{-- <input type="hidden" value="{{(isset($request->page_range))?$request->page_range:''}}" name="page_range" id="page_range"> --}}
                        <div class="card-footer">
                            <button name="search_submit" value="1" type="submit" class="btn btn-primary btn-dark">
                                Search
                            </button>
                            <button name="search_reset" type="reset" class="btn btn-info btn-secondary" id="form_reset">
                                Reset
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
      <div id="right-sidebar" class="settings-panel">
        <i class="settings-close ti-close"></i>
     
      </div>
      @endif
      <!-- partial -->
      <!-- partial:../../partials/_sidebar.html -->
      <!-- partial -->
      
        
      
          <div class="row">
        
            <section class="content-header">
           
                  <div>
                      <div class="card-body text-left pt-2 pb-2">
                     
                         
                      </div>
                  </div>
              </section>
              
            <div class="col-md-12">
            <?php if(!empty($month) && !empty($year)){?>
      <button class="btn btn-dark send_mail mb-2" style="margin-left:1150px;">send Mail</button>
      <?php }?>
              <div class="card">
                    <div class="row update_checked_data_row">
                      <div class="col-md-1 "> 
                          <div class="form-group col">
                            <form action="{{url('/salarysheet/'.$month.'/'.$year)}}" method ="post" name="getpage" id="getpage">
                              @csrf
                                <?php if(isset($salrayslipdata) || isset($salrayslipmonthdata)){?>
                              <input type="hidden" name="month" value="{{isset($month)?$month:''}}">
                                 <input type="hidden" name="year" value="{{isset($year)?$year:''}}">
                               
                              <select class="select2 form-control custom-select page_range" name="page_range" id="page_range">
                                  @if(is_array($pageRangArray))
                                      @foreach ($pageRangArray as $key => $node)
                                          <option value="{{$key}}" {{(isset($request->page_range) && ($request->page_range == $node))?'selected':''}}> {{ $node }}</option>
                                      @endforeach 
                                  @endif
                              </select>
                            <?php } ?>
                             </form>
                          </div>
                      </div>
                      <div class="col-md-7 float-right"> </div>
                      <div class="col-md-4">

                      <div class="card-body show_total_no">
                            <?php if(isset($salrayslipdata)){?>
                             Showing {{ $salrayslipdata->firstItem() }} to {{ $salrayslipdata->lastItem() }} of total {{$salrayslipdata->total()}} entries
                           <?php }?>
                           <?php if(isset($salrayslipmonthdata)){?>
                             Showing {{ $salrayslipmonthdata->firstItem() }} to {{ $salrayslipmonthdata->lastItem() }} of total {{$salrayslipmonthdata->total()}} entries
                           <?php }?>

                          </div> 
                      </div>

                     
                  </div>

                  <div class="table-responsive">
                
                 @if(!empty($salrayslipdata))
                  @php  
                     $getsalrayslipmonthdata = '';

                     @endphp
                     
                    <table class="table table-striped table-bordered">
                      <thead>
                          <h4 class="card-title">SalarySheet</h4>
                          <tr>
                            <th><input type="checkbox" name="locationthemesCheckAll" id="locationthemesCheckAll" ></th>
                           
                          <th>Emp id</th>
                          <th>Emp name</th>
                          <th>Email send Date</th>
                         








                          </tr>
                          
                        </thead> 
                         <tbody>
                  
                           @if($salrayslipdata->count()>0)
                          <?php $i=0?>
                              @foreach($salrayslipdata as $s)
                              <?php $i++;?>
                              <tr class="{{($s->slip_genrated==1)?'disabletext':''}}">
                                <?php if($s->slip_genrated==1) {?>
                               <td><input type="checkbox" name="locationthemes" class="locationthemes" value="{{$s->id}}" checked></td>
                               <?php } else {?>
                                <td><input type="checkbox" name="locationthemes" class="locationthemes" value="{{$s->id}}"></td>
                                <?php }?>
                               <td>{{'TRD'.$s->employee_id }}</td>
                               <td>{{$s->name }}</td>
                               <td>{{$s->slip_genrated_date}}</td>
                               
                             




                               
                          </tr>
                           @endforeach
                          @else
                              <tr>
                              <td colspan="15">
                              <center><h4 class="card-title">No User Found</h4></center>
                              </td>
                            </tr>
                          @endif
                       
                      </tbody> 
                    </table>
                     <div class="d-flex justify-content-center">
                     {!! $salrayslipdata->appends(request()->input())->links() !!}
                    </div>
<!-- <<<<<<< HEAD
                  
=======
                   <form action="{{url('/salarysheet')}}" method ="post" >
                    @csrf
                    <input type="hidden" name="ReportMonth" id="ReportMonth" value="{{$month}}">
                    <input type="hidden" name="ReportYear" id="ReportYear" value="{{$year}}">
                      <input type="submit"  title="Back"  name="export_to_excel" value="Export Excle" class="btn btn-dark">
                                
                         
                          </form>
>>>>>>> 3028a9168782b4477b92b22ca41d1725aca9b95c -->
                     @else
                     
                    <table class="table table-striped table-bordered">
                      <thead>
                        <tr>
                       
                          <th>Month</th>
                          <th>Year</th>
                          <th>Action</th>

                        </tr>

                      </thead>
                      <tbody>
                       @if(!empty($getsalrayslipmonthdata))
                         @php $i = 1; @endphp
                      @php $salrayslipdata = ''; $genralsettingdata = '';
                    @endphp
                      <tr>
                         
                          <td>{{$getsalrayslipmonthdata->month}}</td>
                          <td>{{$getsalrayslipmonthdata->year}}</td>
                    
                            <td>
                             <a href="{{url('/salaryslip/'.$getsalrayslipmonthdata['month'].'/'.$getsalrayslipmonthdata['year'])}}" data-toggle="tooltip" data-placement="top" title="" data-original-title="View"><i  style="margin-right:30px;" class="mdi mdi-pencil"></i>
                            
                          </a> 
                        </td>
                      </tr>
                      <!--   @php  $i++;@endphp -->
                      @else
                      @if(!empty($salrayslipmonthdata))
                    @foreach($salrayslipmonthdata as $sdata)
                       <tr>
                          
                          <td>{{$sdata->month}}</td>
                          <td>{{$sdata->year}}</td>
                   
                            <td>
                             <a href="{{url('/salaryslip/'.$sdata['month'].'/'.$sdata['year'])}}" data-toggle="tooltip" data-placement="top" title="" data-original-title="View"><i  style="margin-right:30px;" class="mdi mdi-pencil"></i>
                              
                          </a> 
                          <!-- <a href="javascript:void(0)" class="sheetExitingDelete" data-id="{{ $sdata->id }}"><i  style="margin-right:30px;" class="mdi mdi-delete"></i></a> -->
                        </td>
                      </tr>
                      @endforeach
                        
                      @endif
                      @endif
                      </tbody>
                        
                    </table>
                        <div class="d-flex justify-content-center">
                        
                  <?php 
                     if(isset($salrayslipmonthdata)){
                      ?>
                        {!!$salrayslipmonthdata->appends(request()->input())->links() !!}
                      <?php }?>
                    </div>
                 
                    
                        

                     @endif  
                  </div>
               </div> 

                </div>

              </div>

             
            </div>
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        
        <!-- partial -->
     
  </div>
</section>
@include('modal.salaryslip_filter_modal')
@include('modal.import_sheet_modal')

@endsection
@section('javascript')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
$('#locationthemesCheckAll').click(function () {  
        $('input:checkbox').prop('checked', this.checked);
    });
   $(document).on("click", ".timeEntry1", function () {
     $('#taskTimeEntryModal1').modal();
     $("#StartDate").datepicker({
                            todayBtn: 1,
                            autoclose: true,
                            format: 'dd-mm-yyyy',             
          });
     $("#EndDate").datepicker({
                            todayBtn: 1,
                            autoclose: true,
                            format: 'dd-mm-yyyy',                
          });
   });
 $(document).on("click", ".importsheet", function () {
     $('#importSheetModal').modal();
     $("#StartDateImport").datepicker({
                            todayBtn: 1,
                            autoclose: true,
                            format: 'dd-mm-yyyy',             
          });
     $("#EndDateImport").datepicker({
                            todayBtn: 1,
                            autoclose: true,
                            format: 'dd-mm-yyyy',                
          });
   });

   $(".dateyearbutton").click(function(){
    if(($('#ReportMonthDropdown').val() != '' && $('#ReportYearDropdown').val() != '')){
      $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
      $.ajax({
            url:getsiteurl() +'/salaryslip/create',
            method:'post',
            data:{'month':$('#ReportMonthDropdown').val(),'year':$('#ReportYearDropdown').val()},
            success: function(data) {
              // alert(data.success);
              $('#ReportMonthPopup').val($('#ReportMonthDropdown').val());
              $('#ReportYearPopup').val($('#ReportYearDropdown').val());
            //   if(data.success = true)
            //   $("#type").val(data.type);
            //   $('.modal_body_salary_sheet_popup').html(data.html);
            //   $("#salary_sheet_popup_modal").modal('show');
              },
          })
     
     }else{
       alert("Please enter start date and end date or start month and start year");
      return false;
     }
  });

  $(".send_mail").click(function(){
    var array = $.map($('input[name="locationthemes"]:checked'), function(c){return c.value; });
    console.log(array);
    //alert($('#ReportMonth').val());
    // var ReportYear = $('#ReportYear').val();
    // var ReportMonth = $('#ReportMonth').val();
    // $('#type').val('edit');
    $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
    $.ajax({
          url:getsiteurl() +'/sendsalaryslip',
          method:'post',
          data:{'month':$('#ReportMonth').val(),'year':$('#ReportYear').val(),"id":array},
          success: function(data) {
            console.log(data);
            return false;
            $('#ReportMonthPopup').val($('#ReportMonth').val());
            $('#ReportYearPopup').val($('#ReportYear').val());
            if(data.success = true){
             
              swal({
              title:'',
              text: 'Email sent succesfully',
              icon: 'success',
              autoclose:'30000',
              buttons: 'ok',
             });
             location.reload();
            }
            else{
              swal({
              title:'',
              text: 'Somthing is wrong.',
              icon: 'Warning',
              autoclose:'30000',
              buttons: 'ok',
             });
             location.reload();
            }
        }
  });
});
  
$("#form_reset").click(function(){
  $('#search_by_employee_id').val('').attr("selected", "selected");
  $('#search_by_user_name').val('').attr("selected", "selected");
   $( "#search_filter" ).submit();
});
    
</script>
@endsection
                                                   
                           
                            
                                                     
                          
                         
                         
                           
                      
                         
                        
                         
                           
                     
                         
                            
                          
                         
                         
                          
                          
                         
                         
                   
           