<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->id();
            $table->integer('company_id')->nullable();
            $table->string('name')->nullable();
            $table->string('employee_code')->nullable();
            $table->string('total_number_of_days')->nullable();
            $table->integer('weekOff')->nullable();
            $table->integer('total_working_days')->nullable();
            $table->integer('total_worked_days')->nullable();
            $table->integer('credit_leave_cf')->nullable();
            $table->integer('leave_of_month')->nullable();
            $table->integer('balance_leave')->nullable();
            $table->integer('comp_off')->nullable();
            $table->integer('balance_leave_after_companyoff')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports');
    }
}
