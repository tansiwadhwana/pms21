$(document).ready(function () {
    $('.select2').select2();
    
    $("#search_task_start_date,#search_task_end_date").datepicker({ autoclose: true,format: 'dd-mm-yyyy'});
    
    $('#checkAll').click(function () {
        if(!$(this).prop("checked")){
             $('#move_task').css('display','none');
         }else{
            $('input:checkbox').prop('checked', this.checked);
            $('#move_task').css('display','block');
        }
    });
    $('.update_task_row').click(function(){
        if($('.update_task_row:checked').length > 0){
            $('#move_task').css('display','block');
        }else{
             $('#move_task').css('display','none');
        }
    })
    if (!$(this).prop("checked")) {
            $("#checkAll").prop("checked", false);
    }
    

    // $('#page_range_dropdown').change(function () {
    //    var limit = $(this).val();
    //     $('#page_range').val(limit);
    //     $('#search_filter').submit();

    // });

    $('#page_range_dropdown').change(function () {
       var limit = $(this).val();
        var link  = window.location.pathname;
        var query_string = window.location.search;

        var search_str = (query_string.indexOf('?') == 0 ? query_string.substring(1) : query_string);
        if(limit != '' && query_string.indexOf('search_submit') != -1){
            if(query_string.indexOf('page_range')==1){
                var page_range = query_string.slice(0,query_string.indexOf('='));
                var srch_str = query_string.slice(query_string.indexOf('&'));
                var redirectLink = link +page_range+'='+limit+srch_str;
            }else{
                var redirectLink = link +'?page_range='+limit + '&'+search_str;
            }
        }else{
            var redirectLink = link +'?page_range='+limit;
        }
       window.location = redirectLink;
    
    });

    $("#reset_startdate").click(function () {
        $('#search_by_start_date').val('');
    });

    $("#reset_enddate").click(function () {
        $('#search_by_end_date').val('');
    });

    $("#search_by_start_date").datepicker({
        todayBtn: 1,
        autoclose: true,
        format: 'dd-mm-yyyy',
    }).on('changeDate', function (selected) {
        var minDate = new Date(selected.date.valueOf());
        $('#search_by_end_date').datepicker('setStartDate', minDate);
    });

    $("#search_by_end_date").datepicker({
        autoclose: true,
        todayBtn: 1,
        format: 'dd-mm-yyyy',
    }).on('changeDate', function (selected) {
        var maxDate = new Date(selected.date.valueOf());
        $('#search_by_start_date').datepicker('setEndDate', maxDate);
    });

    // Task Time Entry
    $("#saveTaskEntry").validate({
        rules: {
            taskDate: "required",
            workTimeHours: "required",
            workTimeMinutes: "required",
            //taskDescription: "required"
        },
        messages: {
            taskDate: "Please select date",
            workTimeHours: "Please select hours",
            workTimeMinutes: "Please select minutes",
            //taskDescription: "Please enter task description"
        },
        submitHandler: function (form) {
            var workTimeHours = $('#workTimeHours').val();
            var workTimeMinutes = $('#workTimeMinutes').val();

            if (workTimeHours == 0 && workTimeMinutes == 0) {
                $('#workTimeHours-error').css({'display': 'block'});
                $('#workTimeMinutes-error').css({'display': 'block'});

                $('#workTimeHours-error').text('Please select hours');
                $('#workTimeMinutes-error').text('Please select minutes');
            } else {
                form.submit();
            }
        }
    });

   
    function remainingProjectEntry(){
        jQuery.ajax({
                url  : getsiteurl() + '/remainingProjectEntry',
                type : "GET",
                success: function(response) {
                        return response;
                }
        });
    }
    $(document).on("click", "#deleteTask", function(){
        var taskEntryId = $(this).attr('data-task-id');
        if(taskEntryId != ''){
                swal({
                title: "Are you sure you want to delete project entry ?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
              }).then((willDelete) => {
                if (willDelete) {
                    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                    jQuery.ajax({
                        url  : getsiteurl() + '/project_misc_task_entry/delete',
                        type : "DELETE",
                        data: {_token: CSRF_TOKEN,taskEntryId:taskEntryId},
                        success: function(response) {
                            if(response.status == 'success'){
                                swal(response.message,"", "success");
                                $('#taskEntryTable > tbody tr#'+taskEntryId).slideUp('slow');
                                 window.location.reload();
                            }else if(response.status == 'error'){
                                swal(response.message,"", "error");
                            }
                        }
                    });
                }
              });
        }
    });
    $(document).on("click", "#updateProjectTask", function(){
        var projectTaskEntryId = $(this).attr('data-task-id');

        if(projectTaskEntryId != ''){
                jQuery.ajax({
                url  : getsiteurl() + '/edit/editProjectTask',
                type : "GET",
                data: {projectTaskEntryId:projectTaskEntryId},
                success: function(response) {
                        $('#editTask').html(response);
                        manageEditTaskEntryInputs();
                        $('#editTask').modal('show');
                }
            });
        }
    });

    function manageEditTaskEntryInputs(){
            $('.select2').select2();
            var fullDate = new Date();
            var twoDigitMonth = ((fullDate.getMonth().length + 1) === 1) ? (fullDate.getMonth() + 1) : ((fullDate.getMonth() + 1) < 10 ? '0' + (fullDate.getMonth() + 1) : (fullDate.getMonth() + 1));
            var todayDate = fullDate.getFullYear() + "-" + twoDigitMonth + "-" + fullDate.getDate();
            var projectStartDate = $('#editTaskProjectStartDate').val();
            var projectEndDate = $('#editTaskProjectEndDate').val();
            $('#taskStartDate').datepicker('destroy');
            $('#taskEndDate').datepicker('destroy'); 
            if(projectEndDate == 'undefined'){
                $("#taskStartDate").datepicker({
                    todayBtn: 1,
                    autoclose: true,
                    format: 'dd-mm-yyyy',
                    startDate: new Date(projectStartDate.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")), 
                    endDate: new Date(),
                }).on('changeDate', function (selected) {
                    var minDate = new Date(selected.date.valueOf());
                    $('#taskEndDate').datepicker('setStartDate', minDate);
                });
                $("#taskEndDate").datepicker({
                    autoclose: true,
                    todayBtn: 1,
                    format: 'dd-mm-yyyy',
                    startDate: new Date(projectStartDate.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")),
                    endDate: new Date(projectEndDate.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")),
                }).on('changeDate', function (selected) {
                    var maxDate = new Date(selected.date.valueOf());
                    $('#taskStartDate').datepicker('setEndDate', maxDate);
                });
            }else{
                $("#taskStartDate").datepicker({
                        todayBtn: 1,
                        autoclose: true,
                        format: 'dd-mm-yyyy',
                        startDate: new Date(projectStartDate.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"))
                }).on('changeDate', function (selected) {
                    var minDate = new Date(selected.date.valueOf());
                    $('#taskEndDate').datepicker('setStartDate', minDate);
                });

                $("#taskEndDate").datepicker({
                    autoclose: true,
                    todayBtn: 1,
                    format: 'dd-mm-yyyy',
                    startDate: new Date(projectStartDate.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"))
                }).on('changeDate', function (selected) {
                    var maxDate = new Date(selected.date.valueOf());
                    $('#taskStartDate').datepicker('setEndDate', maxDate);
                });
            }
    }
    $("#updateTask").validate({
        rules: {
            taskName: "required",
            taskStartDate: "required",
            taskEndDate: "required",
        },
        messages: {
            taskName: "Please enter task name",
            taskStartDate: "Please select start date",
            taskEndDate: "Please select end date",
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
    
});