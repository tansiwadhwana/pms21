<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableReportsChangeLeaves extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reports', function (Blueprint $table) {
            $table->text('weekOff')->change();
            $table->text('total_working_days')->change();
            $table->text('total_worked_days')->change();
            $table->text('credit_leave_cf')->change();
            $table->text('leave_of_month')->change();
            $table->text('balance_leave')->change();
            $table->text('comp_off')->change();
            $table->text('balance_leave_after_companyoff')->change();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports', function (Blueprint $table) {
            //
        });
    }
}
