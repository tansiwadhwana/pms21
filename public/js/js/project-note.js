$(document).ready(function(){
    $('#search_user').select2(); 
	$(document).on("click", ".deleteProjectNote", function(){
        var projectNoteId = $(this).attr('data-note-id');
        
		if(projectNoteId != ''){
            swal({
                title: "Are you sure you want to delete project notes ?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                }).then((willDelete) => {
                if (willDelete) {
                    deleteProjectNote(projectNoteId);
                }
            });
		}
    });
    
	function deleteProjectNote(projectNoteId){
		var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
		jQuery.ajax({
	      	url  : getsiteurl() + '/project_note/delete',
	    	type : "DELETE",
	      	data: {_token: CSRF_TOKEN,projectNoteId:projectNoteId},
	      	success: function(response) {
                if(response.status == 'success'){
                    swal(response.message,"", "success");
                    window.location.reload();
                }else if(response.status == 'error'){
                    swal(response.message,"", "error");
                    if(response.code == 'userNotValid')
                    {
                        location.href = getsiteurl();
                    }
                }
	       	}
	    });
    }
    
    $(document).on("click", ".addProjectNoteBtn", function(){
		var projectId = $(this).attr('data-project-id');
		if(projectId != ''){
			jQuery.ajax({
		      	url  : getsiteurl() + '/add/addProjectNoteModal',
		    	type : "GET",
		      	data: {projectId:projectId},
		      	success: function(response) {
                    if(response.status == 'error'){
                        swal(response.message,"", "error");
                        if(response.code == 'userNotValid')
                        {
                            location.href = getsiteurl();
                        }
                    }
                    else
                    {
                        $('#addProjectNoteModal').empty();
                        $('#addProjectNoteModal').append(response);
                                    manageAddNotes();
                        $('#addProjectNoteModal').modal('show');
                    }
		       	}
		    });
		}
    });
    
    function manageAddNotes(){
        $("#addProjectNotes").validate({
            rules: {
                addNotes: "required",
                addNotesTitle: "required",
            },
            messages: {
                addNotes: "Please enter description",
                addNotesTitle: "Please enter notes title",
            },
            submitHandler: function (form) {
                var addNotes   = $('#addNotes').val();
                if(addNotes == ''){
                        $('#addNotes').css({'display':'block'});
                        $('#addNotes-error').text('Please enter description');
                        $('#addNotesTitle').css({'display':'block'});
                        $('#addNotesTitle-error').text('Please enter notes title');
                }else{
                form.submit();
                }
            }
        });
    }

    $(document).on("click", ".editProjectNote", function(){
		var projectNoteId = $(this).attr('data-note-id');
		if(projectNoteId != ''){
			jQuery.ajax({
		      	url  : getsiteurl() + '/edit/editProjectNoteModal',
		    	type : "GET",
		      	data: {projectNoteId:projectNoteId},
		      	success: function(response) {
                    if(response.status == 'error'){
                        swal(response.message,"", "error");
                        if(response.code == 'userNotValid')
                        {
                            location.href = getsiteurl();
                        }
                    }
                    else
                    {
                        $('#editProjectNoteModal').empty();
		      		    $('#editProjectNoteModal').append(response);
		      		    manageEditNote();
                        $('#editProjectNoteModal').modal('show');
                    }
		       	}
		    });
		}
	});

	function manageEditNote(){
        $('.select2').select2();
               
        $("#editProjectNotes").validate({
	        rules: {
                editNotes: "required",
                editNotesTitle: "required",
	        },
	        messages: {
                editNotes: "Please enter description",
	            editNotesTitle: "Please enter notes title",
	        },
	        submitHandler: function (form) {
	            var editNotes   = $('#editNotes').val();
                if(editNotes == ''){
                    $('#editNotes').css({'display':'block'});
                    $('#editNotes-error').text('Please enter notes description');
                    $('#editNotesTitle').css({'display':'block'});
                    $('#editNotesTitle-error').text('Please enter notes title');
                }else{
                    form.submit();
                }
	        }
	    });
    }
    
    $('#page_range_dropdown').change(function(){
        var limit = $(this).val();
        var link  = window.location.pathname;
        var query_string = window.location.search;

        var search_str = (query_string.indexOf('?') == 0 ? query_string.substring(1) : query_string);
        if(limit != '' && query_string.indexOf('search_submit') != -1){
            if(query_string.indexOf('page_range')==1){
                var page_range = query_string.slice(0,query_string.indexOf('='));
                var srch_str = query_string.slice(query_string.indexOf('&'));
                var redirectLink = link +page_range+'='+limit+srch_str;
            }else{
                var redirectLink = link +'?page_range='+limit + '&'+search_str;
            }
        }else{
            var redirectLink = link +'?page_range='+limit;
        }
        window.location = redirectLink;
    });	
});