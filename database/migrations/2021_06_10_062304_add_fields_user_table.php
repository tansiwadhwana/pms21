<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('employee_id')->after('id')->nullable();
            $table->integer('company_id')->after('employee_id')->nullable();
            $table->string('first_name')->after('company_id')->nullable();
            $table->string('last_name')->after('first_name')->nullable();
            $table->string('personal_email')->after('to_shift')->unique();
            $table->string('contact_number')->after('personal_email')->nullable();
            $table->date('birth_date')->after('contact_number')->nullable();
            $table->string('reporting_user')->after('birth_date')->nullable();
            $table->string('company_name')->after('reporting_user')->nullable();
            $table->string('designation_name')->after('company_name')->nullable();
            $table->date('confirmation_date')->after('designation_name')->nullable();
            $table->date('joining_date')->after('confirmation_date')->nullable();
            $table->integer('department')->after('joining_date')->nullable();
            $table->string('bank_name')->after('department')->nullable();
            $table->string('bank_account_no')->after('bank_name')->nullable();
            $table->string('pan_no')->after('bank_account_no')->nullable();
            $table->double('available_leaves')->after('pan_no')->nullable();
            $table->double('used_leaves')->after('available_leaves')->nullable();
            $table->string('code')->after('used_leaves')->nullable();
            $table->tinyInteger('status')->after('code')->nullable();
            $table->tinyInteger('employee_status')->after('status')->nullable();
            $table->tinyInteger('gratuity')->after('status')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
