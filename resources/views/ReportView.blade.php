@extends('layouts.master')
@section('content')
@php
    $pageRangArray = config('constant.page_range');
@endphp
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/report')}}">Report</a></li>
                </ol>
            </div>
            <div>
               <div class="card-body text-left pt-2 pb-2">
                          
                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Task Entry" 
                                                  class="btn btn-dark timeEntry" data-task-id="" data-id=''>
                      All Report Sync
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
@include('sweet::alert') 
    <section class="content">
    <div class="container-fluid">
    <div id="accordion">
            <div class="card">
                <div class="card-header bg-dark {{(!$request->has('search_submit'))?'collapsed':''}}" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false">
                    <h3 class="card-title">Filter Users</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-chevron-down"></i></button>
                    </div>
                </div>
                <div id="collapseOne" class="panel-collapse in {{(!$request->has('search_submit'))?'collapse':'collapse show'}}">
                    <form class="form-horizontal" method="get" action="{{url('/report/')}}" name="search_filter" id="search_filter">
                        <div class="card-body">
                            <div class="row">
                                <div class="form-group col-lg col-md-6 col-sm-6">
                                    <label class="">User Name</label>
                                    <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="search_by_user_name" id="search_by_user_name">
                                        <option value="">Select</option>
                                        @foreach($empName as $ename)
                                        <option value="{{$ename}}" {{(isset($request->search_by_user_name) && ($request->search_by_user_name == $ename))?'selected':''}}>{{$ename}}</option>
                                        @endforeach
                                    </select>  
                                </div>
                                <div class="form-group col-lg col-md-6 col-sm-6">
                                    <label class="">Employee Id</label>
                                    <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="search_by_employee_id" id="search_by_employee_id">
                                        <option value="">Select</option>
                                         @foreach($empId as $eid)
                                          <option value="{{$eid}}" {{(isset($request->search_by_employee_id) && ($request->search_by_employee_id == $eid))?'selected':''}}>{{'TRD'.$eid}}</option>
                                          @endforeach	
                                    </select>  
                                </div>
                               
                                
                            </div>

                            
                            
                        </div>
                        {{-- <input type="hidden" value="{{(isset($request->page_range))?$request->page_range:''}}" name="page_range" id="page_range"> --}}
                        <div class="card-footer">
                            <button name="search_submit" value="1" type="submit" class="btn btn-primary btn-dark">
                                Search
                            </button>
                            <button name="search_reset" type="reset" class="btn btn-info btn-secondary" onclick="location.href ='{{url('/report')}}'">
                                Reset
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
      <div id="right-sidebar" class="settings-panel">
        <i class="settings-close ti-close"></i>
     
      </div>
      <!-- partial -->
      <!-- partial:../../partials/_sidebar.html -->
      <!-- partial -->
     
          <div class="row">
            
            <div class="col-md-12">

              <div class="card">
               <div class="row update_checked_data_row">
                      <div class="col-md-1 "> 
                          <div class="form-group col">
                              <select class="select2 form-control custom-select page_range_dropdown" name="page_range_dropdown" id="page_range_dropdown">
                                  @if(is_array($pageRangArray))
                                      @foreach ($pageRangArray as $key => $node)
                                          <option value="{{$key}}" {{(isset($request->page_range) && ($request->page_range == $node))?'selected':''}}> {{ $node }}</option>
                                      @endforeach 
                                  @endif
                              </select>
                          </div>
                      </div>
                      <div class="col-md-7"></div>
                      <div class="col-md-4"> 
                          <div class="card-body show_total_no">
                              Showing {{ $report_data->firstItem() }} to {{ $report_data->lastItem() }} of total {{$report_data->total()}} entries
                          </div> 
                      </div>
                  </div>
                  <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                      <thead>
                          <h4 class="card-title">Reports</h4>
                          <tr>
                            <th>No</th>
                            @foreach($fielddata as $key=>$value)
                            <?php echo "<th>".ucwords($value).'</th>';?>
                            @endforeach
                          </tr>
                        </thead> 
                         <tbody>
                          @if($report_data->count()>0)
                          <?php $i=$report_data->firstItem();?>
                              @foreach($report_data as $user)
                              <?php $i++;?>
                              <tr>
                              <td>{{$i}}</td>
                               @foreach($fields as $key=>$value)
                              <?php $userdata=$value->field_name; echo "<td>".$user->$userdata.'</td>';?>
                              @endforeach
                              </tr>
                              @endforeach
                          @else
                              <tr>
                              <td colspan="12">
                              <center><h4 class="card-title">No User Found</h4></center>
                              </td>
                            </tr>
                          @endif
                      </tbody> 
                    </table>
                     <div class="d-flex justify-content-center">
                    {!! $report_data->links() !!}
                    </div>
                  </div>
               </div> 

                </div>

              </div>

             
            </div>
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        
        <!-- partial -->
     
  </div>
</section>
@include('modal.Report_Filter_modal')
@endsection
@section('javascript')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script type="text/javascript">
   $(document).on("click", ".timeEntry", function () {
     $('#taskTimeEntryModal').modal();
     $("#StartDate").datepicker({
                            todayBtn: 1,
                            autoclose: true,
                            format: 'dd-mm-yyyy',             
          });
     $("#EndDate").datepicker({
                            todayBtn: 1,
                            autoclose: true,
                            format: 'dd-mm-yyyy',                
          });
   });
   $('body').on('submit', '#reportform', function (e) {
    
     if(($('#ReportMonth').val() != '' && $('#ReportYear').val() != '')){
      $( "#reportform" ).submit();
     }else{
       alert("Please enter start date and end date or start month and start year");
      return false;
     }
   });
</script>
@endsection
                                                   
                           
                            
                                                     
                          
                         
                         
                           
                      
                         
                        
                         
                           
                     
                         
                            
                          
                         
                         
                          
                          
                         
                         
                   
           