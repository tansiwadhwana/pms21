<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\SalaryslipController;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Salarysheet_attechment;



class sendsalarymail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:salaryslip';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Email Salary Slip to employess';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
         $request = new Request();
       $request['month'] =  Carbon::now()->month; // 1
        $request['year']=  Carbon::now()->year;
    //    echo '<pre>';print_r($month);
  
       $salarysheet_attechment = Salarysheet_attechment::where('month', $request['month'])->where('year',$request['year'])->first();
        //  echo '<pre>';print_r($salarysheet_attechment);exit;
       if(!empty($salarysheet_attechment))
       {
            $salaryslip_controller = new SalaryslipController;
            // Use other controller's method in this controller's method
            $salaryslip_controller->SendSalarySlip($request,'Cron');
       }
       

     
    }
}
