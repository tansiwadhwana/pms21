<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Settings;
use Illuminate\Support\Facades\Auth;

class SettingsController extends Controller
{
     public function ViewSettings(Request $request)
     
     {
     	   $settings = '';
     $settings = Settings::first();
        return view('settings.viewsettings',compact('settings'));
     }

  public function  settingSync(Request $request)
  {
  	   $settings = '';
		$settings = Settings::first(); 
     	$url = 'https://pms.tridhyatech.com/api/general_settings/'.Auth::user()->company_id;
        $ch = curl_init();
        $payload = '{""}';
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT,true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $final_result=json_decode($result);
		//echo "fdsf<pre>";print_r($final_result);exit;
		if(!empty($final_result->data)){
			if(!empty($settings))
			{
				$settings = Settings::first();
			}else{
				$settings  = new Settings();
			}
			foreach($final_result->data as $index=>$value)
            {
				if($value->module == "professional_tax"){
					$settings->professional_tax = $value->professional_tax ;
				}
				if($value->module == "gratuity"){
					$settings->gratuity = $value->gratuity;
				}
				if($value->module == "basic_salary"){
					$settings->basic_salary = $value->basic_salary;

				}if($value->module == "other_allownces"){
					$settings->other_allownces = $value->other_allownces;

				}if($value->module == "house_rent"){
					$settings->house_rent = $value->house_rent;

				}if($value->module == "performance_allowance"){
					$settings->performance_allowance = $value->performance_allowance;
				}if($value->module == "bonus_applicable_percentage"){
					$settings->bonus_applicable_percentage = $value->constant_float_value;
				}	
			}
			$settings->save();
		}
        

     	

		 //alert()->success('Record Update Successfully!')->showConfirmButton('Ok', '#bd242e');
     	 return redirect('/settings');
  }
  public function settingsUpdate(Request $request)
  {
  		$settings = Settings::first();
  		$settings->mail_to_cc = $request->mail_to_cc;
  		$settings->save();
  		return redirect('/settings');
  }

}
