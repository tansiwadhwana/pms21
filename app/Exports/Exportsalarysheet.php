<?php

namespace App\Exports;

use App\Salarysheet;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;
use jeremykenedy\LaravelRoles\Models\Role;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Events\AfterSheet;

class Exportsalarysheet implements FromView,ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct($salraysheet,$genralsettingdata) {
        // echo '<pre>'; print_r($salraysheet);exit;
        $this->salraysheet = $salraysheet;
        $this->genralsettingdata = $genralsettingdata;

      
    }
    public function view(): View
    {
        return view('salary.salarysheet_excle_view', [
            'salraysheet' => $this->salraysheet,
            'genralsettingdata' => $this->genralsettingdata,
            
            
        ]);
    }

    
    
}
