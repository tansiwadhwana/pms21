@extends('layouts.master')
@section('content')
@php
    $pageRangArray = config('constant.page_range');
@endphp
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/salary')}}">Salary</a></li>
                </ol>
            </div>
            
        </div>
        <div class="row">
            
               <div class="card-body text-left pt-2 pb-2 col-sm-6" >
                  <a href="{{url('/salary/create')}}" title="Back" class="btn btn-dark">
                      Add Salary
                  </a>
              </div>
              <div class="card-body text-left pt-2 pb-2 col-sm-6" >
                <button id="upload_csv_button" class="btn btn-dark float-sm-right ml-2"> Upload CSV</button>
                <a href="{{url('/usersync')}}" title="Back" class="btn btn-dark float-sm-right ">
                      All user sync
                  </a>
              </div>
             
        </div>
    </div>
</section>
@include('sweet::alert') 
    <section class="content">
    <div class="container-fluid">
    <div id="accordion">
            <div class="card">
                <div class="card-header bg-dark {{(!$request->has('search_submit'))?'collapsed':''}}" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false">
                    <h3 class="card-title">Filter Users</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-chevron-down"></i></button>
                    </div>
                </div>
                <div id="collapseOne" class="panel-collapse in {{(!$request->has('search_submit'))?'collapse':'collapse show'}}">
                    <form class="form-horizontal" method="get" action="{{url('/salary/')}}" name="search_filter" id="search_filter">
                        <div class="card-body">
                            <div class="row">
                                <div class="form-group col-lg col-md-6 col-sm-6">
                                    <label class="">User Name</label>
                                    <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="search_by_user_name" id="search_by_user_name">
                                        <option value="">Select</option>
                                        @foreach($empName as $ename)
                                        <option value="{{$ename}}" {{(isset($request->search_by_user_name) && ($request->search_by_user_name == $ename))?'selected':''}}>{{$ename}}</option>
                                        @endforeach
                                    </select>  
                                </div>
                                <div class="form-group col-lg col-md-6 col-sm-6">
                                    <label class="">Employee Id</label>
                                    <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="search_by_employee_id" id="search_by_employee_id">
                                        <option value="">Select</option>
                                         @foreach($empId as $eid)
                                          <option value="{{$eid}}" {{(isset($request->search_by_employee_id) && ($request->search_by_employee_id == $eid))?'selected':''}}>{{'TRD'.$eid}}</option>
                                          @endforeach	
                                    </select>  
                                </div>
                               
                                
                            </div>

                            
                            
                        </div>
                        {{-- <input type="hidden" value="{{(isset($request->page_range))?$request->page_range:''}}" name="page_range" id="page_range"> --}}
                        <div class="card-footer">
                            <button name="search_submit" value="1" type="submit" class="btn btn-primary btn-dark">
                                Search
                            </button>
                            <button name="search_reset" type="reset" class="btn btn-info btn-secondary" onclick="location.href ='{{url('/salary')}}'">
                                Reset
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
      <div id="right-sidebar" class="settings-panel">
        <i class="settings-close ti-close"></i>
     
      </div>
      <!-- partial -->
      <!-- partial:../../partials/_sidebar.html -->
      <!-- partial -->
     
          <div class="row">
            
            <div class="col-md-12">

              <div class="card">
              <div class="row update_checked_data_row">
                      <div class="col-md-1 "> 
                          <div class="form-group col">
                              <select class="select2 form-control custom-select page_range_dropdown" name="page_range_dropdown" id="page_range_dropdown">
                                  @if(is_array($pageRangArray))
                                      @foreach ($pageRangArray as $key => $node)
                                          <option value="{{$key}}" {{(isset($request->page_range) && ($request->page_range == $node))?'selected':''}}> {{ $node }}</option>
                                      @endforeach 
                                  @endif
                              </select>
                          </div>
                      </div>
                      <div class="col-md-7"></div>
                      <div class="col-md-4"> 
                          <div class="card-body show_total_no">
                              Showing {{ $salaryData->firstItem() }} to {{ $salaryData->lastItem() }} of total {{$salaryData->total()}} entries
                          </div> 
                      </div>
                  </div>
                  <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                      <thead>
                          <h4 class="card-title">Users</h4>

                          <tr>
                            <th>No</th>
                            <th>Name</th>
                            <th>Employee Code</th>
                            <th>Confirmation Date</th>
                            <th>Joining Date</th>
                            <th>Salary</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          @if($salaryData->count()>0)
                          <?php $i=$salaryData->firstItem();?>
                              @foreach($salaryData as $s)
                              <?php $i++;?>
                              <tr>
                              <td>{{$i}}</td>
                               <td>{{ $s->name }}</td>
                               <td>{{'TRD'. $s->employee_id }}</td>
                               <td>{{ $s->confirmation_date }}</td>
                               <td>{{ $s->joining_date }}</td>
                               <td>{{ $s->salary_amount }}</td>
                              <td align="center">
                                <a href="{{url('/salary/edit/'.$s->id)}}" class="btn btn-dark">Edit
                                    <i class="icon-pencil"></i>
                                </a>                                                         </td>
                              </tr>
                              @endforeach
                          @else
                              <tr>
                              <td colspan="8">
                              <center><h4 class="card-title">No User Found</h4></center>
                              </td>
                            </tr>
                          @endif
                      </tbody> 
                    </table>
                    <div class="d-flex justify-content-center">
                    {!! $salaryData->links() !!}
                  </div>

                  
                  </div>
               
               </div> 

                </div>

              </div>

             
            </div>
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        
        <!-- partial -->
     
  </div>
</section>
<div class="modal fade myfet-popup" id="upload_csv_modal" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">

            <div class="modal-content">
                
                 <div class="card">
                    <div class='card-body download_text'><a href="{{asset('doc/user_salary_sample.xlsx')}}"><i class="fa fa-file-excel" aria-hidden="true"></i><b> please download sample file.</b></a></div>
                </div> 
                <div class="card">
                    <form class="form-horizontal" method="post" action="{{url('/salary/user_salary_file')}}" name="upload_csv" id="upload_csv" enctype="multipart/form-data">
                    @csrf 
                        <div class="card-body">
                            <div class="form-group row">
                                <label class="col-md-12">File Upload(Allow to upload .xlsx file)</label>
                                <div class="custom-file">
                                    <input type="file" name="csvfile" id="validatedCustomFile" class="custom-file-input" required/>
                                    <label class="custom-file-label" for="validatedCustomFile" id="uploadedFileName">Choose file...</label>
                                </div>
                            </div> 
                        </div>
                        <div class="border-top">
                            <div class="card-body">
                                <button name="submit" type="submit" class="btn btn-primary btn-dark">
                                    Upload
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
   
@endsection
@section('javascript')
<script type="text/javascript">
 $('#upload_csv_button').click(function(){
            $("#upload_csv_modal").modal();
        });
          $('input[type="file"]').change(function(e){
                var fileName = e.target.files[0].name;
                $('#uploadedFileName').text(fileName);
        });
</script>
@endsection                                                
                           
                            
                                                     
                          
                         
                         
                           
                      
                         
                        
                         
                           
                     
                         
                            
                          
                         
                         
                          
                          
                         
                         
                   
           