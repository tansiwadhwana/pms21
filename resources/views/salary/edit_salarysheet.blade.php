  <input type="hidden" name="id" id="dataid" value="{{$data->id}}">
   <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Employee Id<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6"> 
                                    <input type="text" name="emp_id" class="form-control" value="{{$data->employee_id}}" readonly="">                
                                     <label id="emp_id-error" class="error" for="emp_id"></label> 
                                    </div>                                                   </div>
                            </div>
                        </div>
                    </div>

                     <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Name<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6"> 
                                    <input type="text" name="emp_name" class="form-control" value="{{$data->name}}" readonly="">                
                                     <label id="emp_name-error" class="error" for="emp_name"></label> 
                                    </div>                                                   </div>
                            </div>
                        </div>
                    </div>
                      <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Designation<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6"> 
                                    <input type="text" name="designation" class="form-control" value="{{$data->designation}}" readonly="">                
                                     <label id="designation-error" class="error" for="designation"></label> 
                                    </div>                                                   </div>
                            </div>
                        </div>
                    </div>
                     <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Department<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6"> 
                                    <input type="text" name="department" class="form-control" value="{{isset($data->getuser->department)?$data->getuser->department:''}}" readonly="">                
                                     <label id="department-error" class="error" for="department"></label> 
                                    </div>                                                   </div>
                            </div>
                        </div>
                    </div>
                     <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Joining Date<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6"> 
                                    <input type="text" name="joining_date" class="form-control" value="{{$data->joining_date}}" readonly="">                
                                     <label id="joining_date-error" class="error" for="joining_date"></label> 
                                    </div>                                                   </div>
                            </div>
                        </div>
                    </div>
                     <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Pan Number<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6"> 
                                    <input type="text" name="pan_number" class="form-control" value="{{$data->pan_number}}" readonly="">                
                                     <label id="pan_number-error" class="error" for="pan_number"></label> 
                                    </div>                                                   </div>
                            </div>
                        </div>
                    </div>
                     <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Bank Name<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6"> 
                                    <input type="text" name="bank_name" class="form-control" value="{{$data->bank_name}}" readonly="">                
                                     <label id="bank_name-error" class="error" for="bank_name"></label> 
                                    </div>                                                   </div>
                            </div>
                        </div>
                    </div>
                      <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Account Number<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6"> 
                                    <input type="text" name="account_number" class="form-control" value="{{$data->account_number}}" readonly="">                
                                     <label id="account_number-error" class="error" for="account_number"></label> 
                                    </div>                                                   </div>
                            </div>
                        </div>
                    </div>
                       <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">IFSC Code<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6"> 
                                    <input type="text" name="ifsc_code" class="form-control" value="{{$data->ifsc_code}}" readonly="">                
                                     <label id="ifsc_code-error" class="error" for="ifsc_code"></label> 
                                    </div>                                                   </div>
                            </div>
                        </div>
                    </div>
                      <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">UAN Number<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6"> 
                                    <input type="text" name="UAN_number" class="form-control" value="{{$data->UAN_number}}" readonly="">                
                                     <label id="UAN_number-error" class="error" for="UAN_number"></label> 
                                    </div>                                                   </div>
                            </div>
                        </div>
                    </div>

                     

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Month Days<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6"> 
                                    <input type="text" name="month_days" class="form-control" value="{{$data->month_days}}" readonly="">                
                                     <label id="month_days-error" class="error" for="month_days"></label> 
                                    </div>                                                   </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Working Days<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6"> 
                                    <input type="text" name="working_days" class="form-control" value="{{$data->working_days}}" readonly="" id="working_days">                
                                     <label id="working_days-error" class="error" for="working_days"></label> 
                                    </div>                                                   </div>
                            </div>
                        </div>
                    </div>

                       
                         <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Present Days<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6"> 
                                    <input type="text" name="present_days" class="form-control" value="{{$data->salaryCalculation['present_days']}}" readonly="" id="present_days">                
                                     <label id="present_days-error" class="error" for="present_days"></label> 
                                    </div>                                                   </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Month working Days<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6"> 
                                    <input type="text" name="month_working_days" class="form-control" value="{{$data->salaryCalculation['month_working_days']}}" readonly="" id="month_working_days">                
                                     <label id="month_working_days-error" class="error" for="month_working_days"></label> 
                                    </div>                                                   </div>
                            </div>
                        </div>
                    </div>
                        <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Total Days<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6"> 
                                    <input type="text" name="total_days" class="form-control" value="{{$data->salaryCalculation['total_days']}}" readonly="" id="total_days">                
                                     <label id="total_days-error" class="error" for="total_days"></label> 
                                    </div>                                                   </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Credit Leave Cf<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6"> 
                                    <input type="text" name="credit_leave_cf" class="form-control" value="{{$data->credit_leave_cf}}" readonly="" id="credit_leave_cf">                
                                     <label id="credit_leave_cf-error" class="error" for="credit_leave_cf"></label> 
                                    </div>                                                   </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Leave Of Month<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6"> 
                                    <input type="text" name="leave_of_month" class="form-control" value="{{$data->leave_of_month}}" readonly="" id="leave_of_month">                
                                     <label id="leave_of_month-error" class="error" for="leave_of_month"></label> 
                                    </div>                                                   </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Balance Leave<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6"> 
                                    <input type="text" name="balance_leave" class="form-control" value="{{$data->balance_leave}}" readonly="">                
                                     <label id="balance_leave-error" class="error" for="balance_leave"></label> 
                                    </div>                                                   </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Comp Off<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6"> 
                                    <input type="text" name="extra_leave" class="form-control" value="{{$data->salaryCalculation['incase_comp_off']}}" readonly="">                
                                     <label id="extra_leave-error" class="error" for="extra_leave"></label> 
                                    </div>                                                   </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Balance Leave After Comp_Off<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6"> 
                                    <input type="text" name="balance_leave_after_companyoff" class="form-control" value="{{$data->balance_leave_after_companyoff}}" readonly="">                
                                     <label id="balance_leave_after_companyoff-error" class="error" for="balance_leave_after_companyoff"></label> 
                                    </div>                                                   </div>
                            </div>
                        </div>
                    </div>
                        <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Salary<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6"> 
                                    <input type="text" name="salary" class="form-control" value="{{isset($data->salaryCalculation['salary'])?round($data->salaryCalculation['salary']):'' }}" id="salary" readonly="">                
                                     <label id="salary-error" class="error" for="salary"></label> 
                                    </div>                                                   </div>
                            </div>
                        </div>
                    </div>
                      <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Add<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6"> 
                                    <input type="text" name="additional" class="form-control detailsonchange" value="{{round($data->salaryCalculation['add'])}}" id="additional">                
                                     <label id="additional-error" class="error" for="additional"></label> 
                                    </div>                                                   </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Salary Payable<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6"> 
                                    <input type="text" name="salary_payable" class="form-control" value="{{round($data->salary_payable)}}" id="salary_payable" readonly="">                
                                     <label id="salary_payable-error" class="error" for="salary_payable"></label> 
                                    </div>                                                   </div>
                            </div>
                        </div>
                    </div> -->
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Proffessional Tax</label>
                            </div>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6"> 
                                    <input type="text" name="proffessional_tax" class="form-control" value="{{round($data->salaryCalculation['p_tax'])}}" id="proffessional_tax" readonly="">                
                                     <label id="proffessional_tax-error" class="error" for="proffessional_tax"></label> 
                                    </div>                                                   </div>
                            </div>
                        </div>
                    </div>
                       <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Permenant Deduction<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6"> 
                                    <input type="text" name="permenant_deduction" class="form-control detailsonchange" value="{{round($data->salaryCalculation['permenant_deduction'])}}" id="permenant_deduction">                
                                     <label id="permenant_deduction-error" class="error" for="permenant_deduction"></label> 
                                    </div>                                                   </div>
                            </div>
                        </div>
                    </div>
                      <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Refund<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6"> 
                                    <input type="text" name="refund" class="form-control detailsonchange" value="{{round($data->salaryCalculation['refund'])}}" id="refund">                
                                     <label id="refund-error" class="error" for="refund"></label> 
                                    </div>                                                   </div>
                            </div>
                        </div>
                    </div>
                      <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">TDS<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6"> 
                                    <input type="text" name="tds" class="form-control detailsonchange" value="{{round($data->salaryCalculation['tds'])}}" id="tds">                
                                     <label id="tds-error" class="error" for="tds"></label> 
                                    </div>                                                   </div>
                            </div>
                        </div>
                    </div>
                        <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Gratuity<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6"> 
                                    <input type="text" name="gratuity" class="form-control detailsonchange" value="{{isset($data->salaryCalculation['gratuity'])?round($data->salaryCalculation['gratuity']):''}}" id="gratuity">                
                                     <label id="gratuity-error" class="error" for="gratuity"></label> 
                                    </div>                                                   </div>
                            </div>
                        </div>
                    </div>
                       <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">provi_Fund_18%<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6"> 
                                    <input type="text" name="provi_Fund_18_persent" class="form-control detailsonchange" value="{{isset($data->salaryCalculation['provi_fund'])?round($data->salaryCalculation['provi_fund']):''}}" id="provi_Fund_18_persent">                
                                     <label id="provi_Fund_18_persent-error" class="error" for="provi_Fund_18_persent"></label> 
                                    </div>                                                   </div>
                            </div>
                        </div>
                    </div>
                     <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Other Deduction<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6"> 
                                    <input type="text" name="other_deduction" class="form-control detailsonchange" value="{{round($data->salaryCalculation['other_deduction'])}}" id="other_deduction">                
                                     <label id="other_deduction-error" class="error" for="other_deduction"></label> 
                                    </div>                                                   </div>
                            </div>
                        </div>
                    </div>
                   <!--  <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Resignation Hold Release<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6"> 
                                    <input type="text" name="resignation_hold_release" class="form-control" value="{{$data->resignation_hold_release}}">                
                                     <label id="resignation_hold_release-error" class="error" for="resignation_hold_release"></label> 
                                    </div>                                                   </div>
                            </div>
                        </div>
                    </div> -->
                       <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Net Payable<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6"> 
                                    <input type="text" name="net_payable" class="form-control" value="{{round($data->salaryCalculation['net_payable'])}}" id="net_payable" readonly="">                
                                     <label id="net_payable-error" class="error" for="net_payable"></label> 
                                    </div>                                                   </div>
                            </div>
                        </div>
                    </div>
                      <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Mode<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6"> 
                                    <input type="text" name="mode" class="form-control" value="HDFC" readonly="">                
                                     <label id="mode" class="error" for="mode"></label> 
                                    </div>                                                   </div>
                            </div>
                        </div>
                    </div>
                     <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Notes</label>
                            </div>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6"> 
                                    <textarea name="note" class="form-control">{{$data->note}}</textarea>              
                            </div>
                            </div>
                        </div>
                    </div>
                    <script>
                    $(document).ready(function(){
                        $('.detailsonchange').change(function(){
                            var id = $('#dataid').val();
                            var salary = $('#salary').val();
                            if($(this).attr('id') == 'additional'){
                                var additional = $(this).val();
                            }else{
                                var additional = $('#additional').val();
                            }
                            
                            var salary_payable = $('#salary_payable').val();
                            if($(this).attr('id') == 'permenant_deduction'){
                                var permenant_deduction = $(this).val();
                            }else{
                                var permenant_deduction = $('#permenant_deduction').val();
                            }
                            if($(this).attr('id') == 'refund'){
                                var refund = $(this).val();
                            }else{
                                var refund = $('#refund').val();
                            }

                            if($(this).attr('id') == 'tds'){
                                var tds = $(this).val();
                            }else{
                                var tds = $('#tds').val();
                            }
                            if($(this).attr('id') == 'other_deduction'){
                                var other_deduction = $(this).val();
                            }else{
                                var other_deduction = $('#other_deduction').val();
                            }
                           
                            
                            var gratuity = $('#gratuity').val();
                            var working_days = $('#working_days').val();
                            var total_days = $('#total_days').val();
                            var provi_Fund_18_persent = $('#provi_Fund_18_persent').val();
                            var credit_leave_cf = $('#credit_leave_cf').val();
                            var leave_of_month = $('#leave_of_month').val();
                            var present_days = $('#present_days').val();
                            var month_working_days = $('#month_working_days').val();
                            var net_payable = $('#net_payable').val();
                            var proffessional_tax = $('#proffessional_tax').val();

                       // alert("here");
                            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                            jQuery.ajax({
                                url  : getsiteurl() + '/salary/getSalaryCalculation',
                                type : "POST",
                                data: {_token: CSRF_TOKEN,id:id,salary:salary,salary_payable:salary_payable,permenant_deduction:permenant_deduction,refund:refund,tds:tds,other_deduction:other_deduction,gratuity:gratuity,provi_Fund_18_persent:provi_Fund_18_persent,additional:additional,working_days:working_days,total_days:total_days,leave_of_month:leave_of_month,credit_leave_cf:credit_leave_cf,present_days:present_days,month_working_days:month_working_days,net_payable:net_payable,proffessional_tax:proffessional_tax},
                                success: function(response) {
                                    console.log(response);
                                  //  var salary_payable = $('#salary_payable').val();
                                   // alert(response.salary_payable);
                                   // alert(response.salary_payable);
                                     //   console.log();
                                       // $('#salary_payable').attr('value', '12345');
                                      ////  $('#salary_payable').val() = '12345';
                                        $("#salary_payable").val(response.salary_payable);
                                        $("#net_payable").val(response.net_payable);
                                }
                            });
                        });
                    });
                    </script>
                 <!--    <input type="text" name=""> -->