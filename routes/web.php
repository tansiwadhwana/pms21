<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::group(['middleware'=>['auth']],function(){

Route::get('/register', [App\Http\Controllers\Auth\LoginController::class, 'RedirectToLogin'])->middleware('guest')->name('register');

Route::get('logout', [App\Http\Controllers\HomeController::class, 'logout']);
Route::match(['get','post'],'/changepassword',[App\Http\Controllers\HomeController::class, 'changePassword']);
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/home/userfield', [App\Http\Controllers\HomeController::class, 'DisplayFiled'])->name('/home/userfield');
Route::post('/insertfields', [App\Http\Controllers\HomeController::class,'InsertFields'])->name('InsertFields');
Route::get('/users', [App\Http\Controllers\HomeController::class, 'users'])->name('users');
Route::get('/usersync', [App\Http\Controllers\HomeController::class, 'usersync'])->name('usersync');
Route::get('/edituser/{id}', [App\Http\Controllers\HomeController::class, 'edit'])->name('edituser');



Route::get('/report', [App\Http\Controllers\ReportController::class, 'Report'])->name('report');
Route::post('/reportinsert', [App\Http\Controllers\ReportController::class,'ReportStore'])->name('ReportStore');
Route::get('/home/reportfield', [App\Http\Controllers\HomeController::class, 'ReportDisplayFiled'])->name('/home/reportfield');
Route::post('/insertreportfields', [App\Http\Controllers\HomeController::class,'InsertReportFields'])->name('InsertReportFields');

Route::get('/salary', [App\Http\Controllers\SalaryController::class, 'salary'])->name('salary');
Route::post('salary/user_salary_file',[App\Http\Controllers\SalaryController::class, 'userSalaryUpload']);
Route::get('/salary/create', [App\Http\Controllers\SalaryController::class, 'create'])->name('create');
Route::post('/salary/get_user_details', [App\Http\Controllers\SalaryController::class, 'getUserDetails'])->name('get_user_details');
Route::post('/salary/store', [App\Http\Controllers\SalaryController::class, 'store'])->name('store');
Route::get('/salary/edit/{id}', [App\Http\Controllers\SalaryController::class, 'edit'])->name('edit');
Route::post('/salary/update', [App\Http\Controllers\SalaryController::class, 'update'])->name('update');
Route::post('/salary/getSalaryCalculation', [App\Http\Controllers\SalaryController::class, 'salaryCalculationAjax'])->name('getSalaryCalculation');

Route::any('/salarysheetDelete', [App\Http\Controllers\SalaryController::class, 'salarysheetDelete'])->name('salarysheetDelete');
Route::any('/salarysheetpopup', [App\Http\Controllers\SalaryController::class, 'salarySheetPopup'])->name('salarysheetpopup');
Route::any('/salarysheetpopup/update', [App\Http\Controllers\SalaryController::class, 'salarySheetPopupUpdate'])->name('salarySheetPopupUpdate');
Route::any('/salarysheet/{month?}/{year?}', [App\Http\Controllers\SalaryController::class, 'Salarysheet'])->name('salarysheet');
Route::get('/salraysheet/genrate', [App\Http\Controllers\SalaryController::class,'GenrateSalraysheet'])->name('genrate_salraysheet');
Route::get('/salarysheetedit/edit/{id}',[App\Http\Controllers\SalaryController::class,'EditSalarysheet'])->name('salarysheet_edit');
Route::post('/salarysheetupdate/update/',[App\Http\Controllers\SalaryController::class,'UpdateSalarysheet'])->name('salarysheet_update');

Route::any('/salaryslip/{month?}/{year?}', [App\Http\Controllers\SalaryslipController::class, 'SalarySlip'])->name('SalarySlip');
Route::any('/salaryslip/create', [App\Http\Controllers\SalaryslipController::class, 'SalarySlipCreate'])->name('SalarySlipCreate');
Route::any('/sendsalaryslip', [App\Http\Controllers\SalaryslipController::class, 'SendSalarySlip'])->name('SendSalarySlip');
Route::any('/importSalarySlipModal', [App\Http\Controllers\SalaryslipController::class, 'ImportSalarySlipModal'])->name('importSalarySlipModal');


Route::any('/importTds', [App\Http\Controllers\SalaryController::class, 'importTds'])->name('importTds');

Route::any('/settings', [App\Http\Controllers\SettingsController::class, 'ViewSettings'])->name('ViewSettings');
Route::post('/settingsUpdate', [App\Http\Controllers\SettingsController::class, 'settingsUpdate'])->name('settingsUpdate');
Route::post('/settings/storeorupdate', [App\Http\Controllers\SettingsController::class, 'SettingsStoreOrUpdate'])->name('store_or_update');
Route::get('/setting_sync', [App\Http\Controllers\SettingsController::class, 'settingSync'])->name('settingSync');
/*Route::get('/home','Admin\userController@index');*/
/*Route::post('dashboard','Admin\userController@index');*/
});